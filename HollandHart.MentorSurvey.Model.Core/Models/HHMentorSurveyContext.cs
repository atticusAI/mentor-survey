﻿using System;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class HHMentorSurveyContext : DbContext
    {
        public virtual DbSet<Admins> Admins { get; set; }
        public virtual DbSet<Answers> Answers { get; set; }
        public virtual DbSet<AppSettings> AppSettings { get; set; }
        public virtual DbSet<Choices> Choices { get; set; }
        public virtual DbSet<Mentees> Mentees { get; set; }
        public virtual DbSet<MentorAssignments> MentorAssignments { get; set; }
        public virtual DbSet<Mentors> Mentors { get; set; }
        public virtual DbSet<QuestionRules> QuestionRules { get; set; }
        public virtual DbSet<Questions> Questions { get; set; }
        public virtual DbSet<QuestionTypeRules> QuestionTypeRules { get; set; }
        public virtual DbSet<QuestionTypes> QuestionTypes { get; set; }
        public virtual DbSet<Rules> Rules { get; set; }
        public virtual DbSet<RuleTypes> RuleTypes { get; set; }
        public virtual DbSet<SurveyAudiences> SurveyAudiences { get; set; }
        public virtual DbSet<SurveyQuestions> SurveyQuestions { get; set; }
        public virtual DbSet<Surveys> Surveys { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        Configuration Configuration = new Configuration();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.ConnectionStrings["HHMentorSurvey"]);
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admins>(entity =>
            {
                entity.HasKey(e => e.AdminId);

                entity.Property(e => e.AdminId).ValueGeneratedNever();

                entity.HasOne(d => d.Admin)
                    .WithOne(p => p.Admins)
                    .HasForeignKey<Admins>(d => d.AdminId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Admins_Users");
            });

            modelBuilder.Entity<Answers>(entity =>
            {
                entity.HasKey(e => e.AnswerId);

                entity.HasIndex(e => e.QuestionId)
                    .HasName("IX_FK_QuestionAnswer");

                entity.HasIndex(e => e.SurveyId)
                    .HasName("IX_FK_SurveyAnswer");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_FK_UserAnswer");

                entity.Property(e => e.Value).HasMaxLength(250);

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.Answers)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionAnswer");

                entity.HasOne(d => d.Survey)
                    .WithMany(p => p.Answers)
                    .HasForeignKey(d => d.SurveyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SurveyAnswer");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Answers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserAnswer");
            });

            modelBuilder.Entity<AppSettings>(entity =>
            {
                entity.HasKey(e => e.SettingId);

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Text).HasColumnType("ntext");

                entity.Property(e => e.Value).HasMaxLength(50);
            });

            modelBuilder.Entity<Choices>(entity =>
            {
                entity.HasKey(e => e.ChoiceId);

                entity.HasIndex(e => e.QuestionId)
                    .HasName("IX_FK_QuestionChoice");

                entity.Property(e => e.Description).IsRequired();

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.Choices)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionChoice");
            });

            modelBuilder.Entity<Mentees>(entity =>
            {
                entity.HasKey(e => e.MenteeId);

                entity.Property(e => e.MenteeId).ValueGeneratedNever();

                entity.HasOne(d => d.Mentee)
                    .WithOne(p => p.Mentees)
                    .HasForeignKey<Mentees>(d => d.MenteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Mentees_Users");
            });

            modelBuilder.Entity<MentorAssignments>(entity =>
            {
                entity.HasKey(e => e.MentorAssignmentId);

                entity.HasOne(d => d.Mentee)
                    .WithMany(p => p.MentorAssignments)
                    .HasForeignKey(d => d.MenteeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MentorAssignments_Mentees");

                entity.HasOne(d => d.Mentor)
                    .WithMany(p => p.MentorAssignments)
                    .HasForeignKey(d => d.MentorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MentorAssignments_Mentors");
            });

            modelBuilder.Entity<Mentors>(entity =>
            {
                entity.HasKey(e => e.MentorId);

                entity.Property(e => e.MentorId).ValueGeneratedNever();

                entity.HasOne(d => d.Mentor)
                    .WithOne(p => p.Mentors)
                    .HasForeignKey<Mentors>(d => d.MentorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Mentors_Users");
            });

            modelBuilder.Entity<QuestionRules>(entity =>
            {
                entity.HasKey(e => e.QuestionRuleId);

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.QuestionRules)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionRules_Questions");

                entity.HasOne(d => d.Rule)
                    .WithMany(p => p.QuestionRules)
                    .HasForeignKey(d => d.RuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionRules_Rules");
            });

            modelBuilder.Entity<Questions>(entity =>
            {
                entity.HasKey(e => e.QuestionId);

                entity.HasIndex(e => e.QuestionTypeId)
                    .HasName("IX_FK_AnswerTypeQuestion");

                entity.Property(e => e.Text).IsRequired();

                entity.HasOne(d => d.QuestionType)
                    .WithMany(p => p.Questions)
                    .HasForeignKey(d => d.QuestionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Questions_QuestionTypes");
            });

            modelBuilder.Entity<QuestionTypeRules>(entity =>
            {
                entity.HasKey(e => e.QuestionTypeRuleId);

                entity.HasOne(d => d.QuestionType)
                    .WithMany(p => p.QuestionTypeRules)
                    .HasForeignKey(d => d.QuestionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionTypeRules_QuestionTypes");

                entity.HasOne(d => d.Rule)
                    .WithMany(p => p.QuestionTypeRules)
                    .HasForeignKey(d => d.RuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionTypeRules_Rules");
            });

            modelBuilder.Entity<QuestionTypes>(entity =>
            {
                entity.HasKey(e => e.QuestionTypeId);

                entity.Property(e => e.Description).HasColumnType("ntext");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Rules>(entity =>
            {
                entity.HasKey(e => e.RuleId);

                entity.Property(e => e.Definition).HasColumnType("ntext");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.RuleType)
                    .WithMany(p => p.Rules)
                    .HasForeignKey(d => d.RuleTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Rules_RuleType");
            });

            modelBuilder.Entity<RuleTypes>(entity =>
            {
                entity.HasKey(e => e.RuleTypeId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SurveyAudiences>(entity =>
            {
                entity.HasKey(e => e.SurveyAudienceId);

                entity.HasIndex(e => e.SurveyId)
                    .HasName("IX_FK_SurveySurveyAudience");

                entity.HasIndex(e => e.UserId)
                    .HasName("IX_FK_UserSurveyAudience");

                entity.HasOne(d => d.Survey)
                    .WithMany(p => p.SurveyAudiences)
                    .HasForeignKey(d => d.SurveyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SurveySurveyAudience");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SurveyAudiences)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserSurveyAudience");
            });

            modelBuilder.Entity<SurveyQuestions>(entity =>
            {
                entity.HasKey(e => e.SurveyQuestionId);

                entity.HasIndex(e => e.QuestionId)
                    .HasName("IX_FK_QuestionSurveyQuestion");

                entity.HasIndex(e => e.SurveyId)
                    .HasName("IX_FK_SurveySurveyQuestion");

                entity.HasOne(d => d.Question)
                    .WithMany(p => p.SurveyQuestions)
                    .HasForeignKey(d => d.QuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuestionSurveyQuestion");

                entity.HasOne(d => d.Survey)
                    .WithMany(p => p.SurveyQuestions)
                    .HasForeignKey(d => d.SurveyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SurveySurveyQuestion");
            });

            modelBuilder.Entity<Surveys>(entity =>
            {
                entity.HasKey(e => e.SurveyId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Title).HasMaxLength(256);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.Adlogin)
                    .HasColumnName("ADLogin")
                    .HasMaxLength(250);

                entity.Property(e => e.EmailAddress).HasMaxLength(250);

                entity.Property(e => e.EmployeeId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.PracticeGroup).HasMaxLength(250);
            });
        }
    }
}

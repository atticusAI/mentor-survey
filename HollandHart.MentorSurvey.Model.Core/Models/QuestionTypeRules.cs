﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class QuestionTypeRules
    {
        public int QuestionTypeRuleId { get; set; }
        public int QuestionTypeId { get; set; }
        public int RuleId { get; set; }

        public QuestionTypes QuestionType { get; set; }
        public Rules Rule { get; set; }
    }
}

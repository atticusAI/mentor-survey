﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Users
    {
        public Users()
        {
            Answers = new HashSet<Answers>();
            SurveyAudiences = new HashSet<SurveyAudiences>();
        }

        public int UserId { get; set; }
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public string Adlogin { get; set; }
        public string PracticeGroup { get; set; }
        public string EmailAddress { get; set; }

        public Admins Admins { get; set; }
        public Mentees Mentees { get; set; }
        public Mentors Mentors { get; set; }
        public ICollection<Answers> Answers { get; set; }
        public ICollection<SurveyAudiences> SurveyAudiences { get; set; }
    }
}

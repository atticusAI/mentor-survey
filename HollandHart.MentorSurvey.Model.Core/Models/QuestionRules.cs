﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class QuestionRules
    {
        public int QuestionRuleId { get; set; }
        public int QuestionId { get; set; }
        public int RuleId { get; set; }

        public Questions Question { get; set; }
        public Rules Rule { get; set; }
    }
}

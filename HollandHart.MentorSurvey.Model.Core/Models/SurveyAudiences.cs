﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class SurveyAudiences
    {
        public int SurveyAudienceId { get; set; }
        public int SurveyId { get; set; }
        public int UserId { get; set; }
        public bool Complete { get; set; }

        public Surveys Survey { get; set; }
        public Users User { get; set; }
    }
}

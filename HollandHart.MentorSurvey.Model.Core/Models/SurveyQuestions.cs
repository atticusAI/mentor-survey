﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class SurveyQuestions
    {
        public int SurveyQuestionId { get; set; }
        public int QuestionId { get; set; }
        public int SurveyId { get; set; }

        public Questions Question { get; set; }
        public Surveys Survey { get; set; }
    }
}

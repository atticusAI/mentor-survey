﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Choices
    {
        public int ChoiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? SortOrder { get; set; }
        public int QuestionId { get; set; }
        public bool? IsDefault { get; set; }

        public Questions Question { get; set; }
    }
}

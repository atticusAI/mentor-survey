﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Answers
    {
        public int AnswerId { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
        public int SurveyId { get; set; }
        public int UserId { get; set; }
        public int QuestionId { get; set; }

        public Questions Question { get; set; }
        public Surveys Survey { get; set; }
        public Users User { get; set; }
    }
}

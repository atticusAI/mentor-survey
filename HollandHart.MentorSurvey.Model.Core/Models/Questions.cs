﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Questions
    {
        public Questions()
        {
            Answers = new HashSet<Answers>();
            Choices = new HashSet<Choices>();
            QuestionRules = new HashSet<QuestionRules>();
            SurveyQuestions = new HashSet<SurveyQuestions>();
        }

        public int QuestionId { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }
        public int QuestionTypeId { get; set; }

        public QuestionTypes QuestionType { get; set; }
        public ICollection<Answers> Answers { get; set; }
        public ICollection<Choices> Choices { get; set; }
        public ICollection<QuestionRules> QuestionRules { get; set; }
        public ICollection<SurveyQuestions> SurveyQuestions { get; set; }
    }
}

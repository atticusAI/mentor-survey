﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class RuleTypes
    {
        public RuleTypes()
        {
            Rules = new HashSet<Rules>();
        }

        public int RuleTypeId { get; set; }
        public string Name { get; set; }

        public ICollection<Rules> Rules { get; set; }
    }
}

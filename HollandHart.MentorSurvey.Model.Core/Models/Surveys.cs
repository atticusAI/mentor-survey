﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Surveys
    {
        public Surveys()
        {
            Answers = new HashSet<Answers>();
            SurveyAudiences = new HashSet<SurveyAudiences>();
            SurveyQuestions = new HashSet<SurveyQuestions>();
        }

        public int SurveyId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public bool Closed { get; set; }
        public string Description { get; set; }

        public ICollection<Answers> Answers { get; set; }
        public ICollection<SurveyAudiences> SurveyAudiences { get; set; }
        public ICollection<SurveyQuestions> SurveyQuestions { get; set; }
    }
}

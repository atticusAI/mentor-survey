﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Mentors
    {
        public Mentors()
        {
            MentorAssignments = new HashSet<MentorAssignments>();
        }

        public int MentorId { get; set; }

        public Users Mentor { get; set; }
        public ICollection<MentorAssignments> MentorAssignments { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Mentees
    {
        public Mentees()
        {
            MentorAssignments = new HashSet<MentorAssignments>();
        }

        public int MenteeId { get; set; }

        public Users Mentee { get; set; }
        public ICollection<MentorAssignments> MentorAssignments { get; set; }
    }
}

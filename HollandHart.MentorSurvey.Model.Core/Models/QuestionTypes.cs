﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class QuestionTypes
    {
        public QuestionTypes()
        {
            QuestionTypeRules = new HashSet<QuestionTypeRules>();
            Questions = new HashSet<Questions>();
        }

        public int QuestionTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<QuestionTypeRules> QuestionTypeRules { get; set; }
        public ICollection<Questions> Questions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class MentorAssignments
    {
        public int MentorAssignmentId { get; set; }
        public int MentorId { get; set; }
        public int MenteeId { get; set; }

        public Mentees Mentee { get; set; }
        public Mentors Mentor { get; set; }
    }
}

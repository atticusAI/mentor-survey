﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Admins
    {
        public int AdminId { get; set; }

        public Users Admin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace HollandHart.MentorSurvey.Model.Core.Models
{
    public partial class Rules
    {
        public Rules()
        {
            QuestionRules = new HashSet<QuestionRules>();
            QuestionTypeRules = new HashSet<QuestionTypeRules>();
        }

        public int RuleId { get; set; }
        public string Name { get; set; }
        public string Definition { get; set; }
        public int RuleTypeId { get; set; }

        public RuleTypes RuleType { get; set; }
        public ICollection<QuestionRules> QuestionRules { get; set; }
        public ICollection<QuestionTypeRules> QuestionTypeRules { get; set; }
    }
}

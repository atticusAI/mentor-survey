﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HollandHart.MentorSurvey.Model.Core
{
    public class Configuration : IConfiguration
    {

        private IConfiguration _configuration = null;

        public Configuration()
        {
            var bldr = new ConfigurationBuilder();
            bldr.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            _configuration = bldr.Build();
        }

        public string this[string key] { get => _configuration[key]; set => _configuration[key] = value; }

        public IEnumerable<IConfigurationSection> GetChildren() => _configuration.GetChildren();

        public IChangeToken GetReloadToken() => _configuration.GetReloadToken();

        public IConfigurationSection GetSection(string key) => _configuration.GetSection(key);

        public IConfigurationSection ConnectionStrings => _configuration.GetSection("ConnectionStrings");
    }
}

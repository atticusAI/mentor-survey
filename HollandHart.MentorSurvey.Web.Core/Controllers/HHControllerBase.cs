﻿using HollandHart.MentorSurvey.Model.Core.Models;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace HollandHart.MentorSurvey.Web.Core.Controllers
{
    public class HHControllerBase : Controller
    {
        HHMentorSurveyContext db = new HHMentorSurveyContext();


        protected bool IAmAdmin
        {
            get
            {
                var qry = from a in db.Admins
                          join u in db.Users on a.AdminId equals u.UserId
                          where u.Adlogin == ADLoginName
                          select a;
                return (qry.Count() > 0);
            }
        }

        protected string ADLoginName
        {
            get
            {
                throw new NotImplementedException();
                // return RequestContext.Principal.Identity.Name.Split('\\')[1];
            }
        }


    }
}

﻿<%@ page title="" language="C#" masterpagefile="~/Site.Master" autoeventwireup="true" codebehind="Complete.aspx.cs" inherits="HollandHart.MentorSurvey.Web.Complete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrapper">
        <div id="mentor" runat="server" visible="false">
            <h2>Thank You!</h2>
            You’ve successfully submitted your Mentor Profile. Thank you for volunteering as a mentor. PD Team will be in touch with you in a few weeks.
        </div>

        <div id="mentee" runat="server" visible="false">
            <h2>Thank You!</h2>
            You’ve successfully submitted your Mentee Profile. Congratulations on signing up to participate in H&amp;H Mentoring Program. PD Team will be in touch with you in a few weeks. 
        </div>

        <div id="unknown" runat="server" visible="false">
            <h2>Thank You!</h2>
            You’ve successfully submitted your profile. Thank you for signing up to participate in H&amp;H Mentoring Program. PD Team will be in touch with you in a few weeks. 
        </div>
    </div>
</asp:Content>

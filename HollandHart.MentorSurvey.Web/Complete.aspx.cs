﻿using HollandHart.MentorSurvey.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web
{
    public partial class Complete : System.Web.UI.Page
    {

        private int GetIntParam(string paramName, int position)
        {
            var s = Request.QueryString[paramName];
            if (string.IsNullOrWhiteSpace(s))
            {
                var ps = Request.Path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (ps.Length >= position + 1)
                {
                    s = ps[position];
                }
            }
            int i = -1;
            if (!Int32.TryParse(s, out i))
            {
                i = -1;
            }
            return i;
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            int sid = GetIntParam("s", 1);

            using (SurveyController ctrl = new SurveyController())
            {
                var srv = ctrl.GetSurvey(sid);
                if (srv != null)
                {
                    if (srv.Name == "Mentor")
                    {
                        mentor.Visible = true;
                    } else if (srv.Name == "Mentee")
                    {
                        mentee.Visible = true;
                    }
                    else
                    {
                        unknown.Visible = true;
                    }
                }
                else
                {
                    unknown.Visible = true;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web.Controls
{
    public partial class UserDropDown : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lbl.Attributes["for"] = sel.ClientID;
        }

        public string Text
        {
            get { return lbl.InnerText; }
            set { lbl.InnerText = value; }
        }

        public SelectorType Type
        {
            get
            {
                SelectorType t = SelectorType.All;
                Enum.TryParse<SelectorType>(typ.Value, out t);
                return t;
            }

            set
            {
                typ.Value = value.ToString();
            }
        }

        public enum SelectorType
        {
            All,
            Mentor,
            Mentee,
            Admin
        }
    }
}
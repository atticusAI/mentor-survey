﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MySurveys.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.MySurveys" %>
<div id="mysurveys" runat="server">
    No Profiles here...
</div>
<input type="hidden" id="hidComplete" runat="server" value="false" />

<script type="text/javascript">
    var <%=ClientID%>_ctrl = new hh.MySurveys($('#<%=mysurveys.ClientID%>'), '<%=ClientID%>_view', $('#<%=hidComplete.ClientID%>'));
</script>

<script type="text/x-jsrender" id="<%=ClientID%>_view">
    <div>
        <% if (ShowComplete) { %>
        <a href="/Review/{{:SurveyId}}">{{:Name}}</a> {{:Description}}
        <% } else {  %>
        <a href="/Survey/{{:SurveyId}}">{{:Name}}</a> {{:Description}}
        <% } %>
    </div>
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserSelect.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.UserSelect" %>
<span id="spn" runat="server">Select a user</span>
<input type="text" id="txtUser" runat="server" />
<label id="lblUser" runat="server"></label>
<input type="button" class="btn btn-default" href="#" id="aClear" runat="server" style="display: none" value="Clear" />
<input type="hidden" id="hUser" runat="server" />
<input type="hidden" id="hType" runat="server" value="All" />
<script type="text/javascript">
    <%=ClientID%>_ctrl = new hh.UserSelector($('#<%=txtUser.ClientID%>'),
        $('#<%=lblUser.ClientID%>'),
        $('#<%=aClear.ClientID%>'),
        $('#<%=hUser.ClientID%>'),
        $('#<%=hType.ClientID%>'));
</script>

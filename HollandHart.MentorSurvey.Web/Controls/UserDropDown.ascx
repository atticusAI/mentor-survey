﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserDropDown.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.UserDropDown" %>
<label id="lbl" runat="server"></label>
<select id="sel" runat="server"></select>
<input type="hidden" runat="server" id="typ" value="All" />
<script type="text/javascript">
<%=ClientID%>_ctrl = new hh.UserDropDown($('#<%=sel.ClientID%>'), $('#<%=typ.ClientID%>'), '#<%=ClientID%>_tmp');
</script>
<script type="text/x-jsrender" id="<%=ClientID%>_tmp">
    <option value="{{:UserId}}">{{:Name}} ({{:EmployeeId}})</option>
</script>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web.Controls
{
    public partial class UserSelect : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public string Text
        {
            get { return spn.InnerText; }
            set { spn.InnerText = value; }
        }

        public SelectorType Type
        {
            get
            {
                SelectorType typ = SelectorType.All;
                Enum.TryParse<SelectorType>(hType.Value, out typ);
                return typ;
            }

            set
            {
                hType.Value = value.ToString();
            }
        }

        public enum SelectorType
        {
            All, 
            Mentor, 
            Mentee, 
            Admin
        }
    }
}
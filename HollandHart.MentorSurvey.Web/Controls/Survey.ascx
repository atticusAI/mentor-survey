﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Survey.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.Survey" %>
<div id="wrp" class="wrapper" runat="server">
    <div class="page-header">
        <h2><%=(Survey != null) ? Survey.Title : ""%></h2>
        <h3><%=(Survey != null) ? Survey.Description : ""%></h3>
        <h3><%=(Survey != null) ? Survey.Description2 : ""%></h3>
    </div>

    <div class="form-group" id="question" runat="server"></div>
    <div class="form-group">
        <input type="button" id="prev" runat="server" class="btn btn-default" value="Previous" disabled />
        <input type="button" id="next" runat="server" class="btn btn-default" value="Next" disabled />
        <input type="button" id="review" runat="server" class="btn btn-success" value="Review" style="display: none;" />
    </div>

    <script type="text/javascript">
        var <%=ClientID%>_ctrl = new hh.Survey({
            $sid: $('#<%=sid.ClientID%>'),
            $qid: $('#<%=qid.ClientID%>'),
            $wrp: $('#<%=wrp.ClientID%>'),
            $question: $('#<%=question.ClientID%>'),
            $prev: $('#<%=prev.ClientID%>'),
            $next: $('#<%=next.ClientID%>'),
            $review: $('#<%=review.ClientID%>')
        });
    </script>
</div>

<script type="text/x-jsrender" id="CommentView">
    <div class="question-text"><span class="number">{{:Number}}.</span>{{:Text}}</div>
    <textarea class="form-control {{joinspace:Rules}}" rows="16" title="{{:Title}}">{{:Answer.Text}}</textarea>
    <div class="alert alert-danger" style="display: none;" role="alert"></div>
</script>

<script type="text/x-jsrender" id="TextView">
    <div class="question-text"><span class="number">{{:Number}}.</span>{{:Text}}</div>
    <input type="text" value="{{:Answer.Text}}" title="{{:Title}}" class="{{joinspace:Rules}}" />
    <div class="alert alert-danger" style="display: none;" role="alert"></div>
</script>

<script type="text/x-jsrender" id="SingleChoiceView">
    <div class="question-text"><span class="number">{{:Number}}.</span>{{:Text}}</div>
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
        {{for Choices}}
            <label data-choice-id="{{:ChoiceId}}" data-toggle="button" class="btn btn-secondary choice">
                <input type="radio" name="sc_{{:~root.QuestionId}}" title="{{:Title}}" class="{{joinspace:Rules}}" value="{{:ChoiceId}}" />
                {{:Description}}
            </label>
        {{/for}}
    </div>
    <div class="alert alert-danger" style="display: none;" role="alert"></div>
</script>

<script type="text/x-jsrender" id="MultipleChoiceRankedView">
    <div class="question-text"><span class="number">{{:Number}}.</span>{{:Text}}</div>
    <div class="question-description">{{:Description}}</div>
    <ul class="list-group">
        {{for Choices}}
                <li data-choice-id="{{:ChoiceId}}" class="list-group-item" id="li_{{:ChoiceId}}">
                    <label class="index"></label>
                    <input type="checkbox" id="ch_{{:ChoiceId}}" data-choice-id="{{:ChoiceId}}" class="{{joinspace:~root.Rules}}" />
                    <label for="ch_{{:ChoiceId}}">{{:Description}}</label>
                </li>
        {{/for}}

        {{if Rules && Rules.indexOf("other") > -1}}
        <li data-choice-id="0" class="list-group-item" id="li_0">
            <label class="index"></label>
            <input type="checkbox" id="ch_0" data-choice-id="0" />
            <label for="ch_0">Other:</label>
            <input type="text" id="txt_0" class="{{joinspace:Rules}}" title="* Please provide a value for this selection." />
            <div class="alert alert-danger" style="display: none;" role="alert"></div>
        </li>
        {{/if}}
    </ul>
    <input type="hidden" value="{{:Rules}}" id="rules" />
</script>

<script type="text/x-jsrender" id="MultipleChoiceView">
    <div class="question-text"><span class="number">{{:Number}}.</span>{{:Text}}</div>
    <div class="question-description">{{:Description}}</div>
    <ul class="list-group {{joinspace:Rules}}" title="{{:Title}}">
        {{for Choices}}
        <li data-choice-id="{{:ChoiceId}}" class="multiple-choice-item" id="li_{{:ChoiceId}}">
            <input type="checkbox" id="ch_{{:ChoiceId}}" data-choice-id="{{:ChoiceId}}" />
            <label for="ch_{{:ChoiceId}}">{{:Description}}</label>
        </li>
        {{/for}}
        {{if Rules && Rules.indexOf("other") > -1}}
        <li data-choice-id="0" class="multiple-choice-item" id="li_0">
            <input type="checkbox" id="ch_0" data-choice-id="0" class="other"/>
            <label for="ch_0">Other</label>
            <input type="text" id="txt_0" title="* Please provide a value for this selection." class="{{joinspace:Rules}}" />
            <div class="alert alert-danger other" style="display: none;" role="alert"></div>
        </li>
        {{/if}}
    </ul>
    <div class="alert alert-danger err" style="display:none;" role="alert"></div>
    <input type="hidden" value="{{:Rules}}" id="rules" />
</script>

<script type="text/x-jsrender" id="RankedListView">
    <div class="question-text"><span class="number">{{:Number}}.</span>{{:Text}}</div>
    <div class="question-description">{{:Description}}</div>
    <ul class="list-group">
        {{for RelatedChoices}}
        <li data-choice-id="{{:ChoiceId}}" class="list-group-item" id="li_{{:ChoiceId}}">
            <a href="#" class="btn btn-default move up"><span class="mega-octicon octicon-arrow-up"></span></a>
            <a href="#" class="btn btn-default move down"><span class="mega-octicon octicon-arrow-down"></span></a>
            {{:Description}}
        </li>
        {{/for}}
        {{if RelatedRules && RelatedRules.indexOf('other') > -1}}
        <li data-choice-id="0" class="list-group-item" id="li_0">
            <a href="#" class="btn btn-default move up"><span class="mega-octicon octicon-arrow-up"></span></a>
            <a href="#" class="btn btn-default move down"><span class="mega-octicon octicon-arrow-down"></span></a>
            Other <input type="text" id="txt_0" disabled="disabled" />
        </li>
        {{/if}}
    </ul>
</script>

<script type="text/x-jsrender" id="DropDownListView">
    <div class="question-text"><span class="number">{{:Number}}.</span>{{:Text}}</div>
    <select class="btn btn-default">
        {{for Choices}}
            <option class="btn btn-default" data-choice-id="{{:ChoiceId}}" value="{{:ChoiceId}}">{{:Description}}</option>
        {{/for}}
    </select>
</script>

﻿<%@ control language="C#" autoeventwireup="true" codebehind="UserManager.ascx.cs" inherits="HollandHart.MentorSurvey.Web.Controls.UserManager" %>

<div class="form-group">
    <div>
        <label>Select a user group.</label>
    </div>
    <div class="btn-group btn-group-toggle" data-toggle="buttons" id="sel" runat="server">
        <label class="btn btn-secondary">
            <input type="radio" id="mentor" name="group" value="mentor" />Mentors
        </label>
        <label class="btn btn-secondary">
            <input type="radio" id="mentee" name="group" value="mentee" />Mentees
        </label>
        <label class="btn btn-secondary">
            <input type="radio" id="admin" name="group" value="admin" />Admins
        </label>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12 col-md-6" runat="server" id="view">
        <table>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <div>
            <input type="button" class="btn btn-default" value="Previous" disabled />
            <input type="button" class="btn btn-default" value="Next" disabled />
            <input type="button" class="btn btn-default" value="Export" id="export" runat="server" disabled />
        </div>
    </div>

    <div class="form-group col-sm-12 col-md-6">
        <label for="users" runat="server">Enter email addresses below, one per line.</label>
        <textarea id="imports" class="form-control" rows="15" runat="server"></textarea>
        <label for="submit" runat="server">Click submit to import the users.</label>
        <input type="button" class="btn btn-default" id="submit" value="Submit" runat="server" />
    </div>
</div>

<script type="text/javascript">
    <%=ClientID%>_ctrl = new hh.UserManager($('#<%=sel.ClientID%>'),
        $('#<%=imports.ClientID%>'),
        $('#<%=submit.ClientID%>'),
        $('#<%=export.ClientID%>'),
        $('#<%=view.ClientID%>'),
        '#tmpUsers');
</script>

<script type="text/x-jsrender" id="tmpUsers">
    <tr>
        <td><a href="#" data-id="{{:UserId}}" class="remove" title="delete"><span class="octicon octicon-x" style="color: red;"></span></a></td>
        <td>{{:Name}}</td>
        <td>{{:EmailAddress}}</td>
    </tr>
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Volunteer.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.Volunteer" %>
<input type="hidden" id="hidMentor" runat="server" value="false" />
<a href="#" id="aMentor" runat="server" style="display:none" class="nav-link">Sign-up as a Mentor</a>

<input type="hidden" id="hidMentee" runat="server" value="false" />
<a href="#" id="aMentee" runat="server" style="display:none" class="nav-link">Sign-up as a Mentee</a>

<script type="text/javascript">
    var <%=ClientID%>_ctrl = new hh.Volunteer($('#<%=hidMentor.ClientID%>'), $('#<%=hidMentee.ClientID%>'),
        $('#<%=aMentor.ClientID%>'), $('#<%=aMentee.ClientID%>'));
</script>
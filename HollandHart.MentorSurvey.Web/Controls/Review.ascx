﻿<%@ control language="C#" autoeventwireup="true" codebehind="Review.ascx.cs" inherits="HollandHart.MentorSurvey.Web.Controls.Review" %>

<div id="wrp" class="wrapper" runat="server">
    <div class="page-header">
        <h2><%=(Survey != null) ? Survey.Title : ""%></h2>
        <h3><%=(Survey != null) ? Survey.Description : ""%></h3>
    </div>
    <h1>Review your Survey Answers</h1>
    <div id="review" runat="server"></div>
    <div class="form-group">
        <input type="button" class="btn btn-default" id="bBack" runat="server" value="Edit" />
        <input type="button" class="btn btn-success" id="bComp" runat="server" value="Complete" />
    </div>

    <script type="text/javascript">
        <%=ClientID%>_ctrl = new hh.Review($('#<%=review.ClientID%>'), $('#<%=sid.ClientID%>'),
            $('#<%=bBack.ClientID%>'), $('#<%=bComp.ClientID%>')
        );
    </script>
</div>

<script type="text/x-jsrender" id="CommentView">
    <div>{{:QuestionText}}</div>
    <pre>{{:AnswerText}}</pre>
    <a class="edit" href="/Survey/{{:SurveyId}}/{{:QuestionId}}">Edit</a>
</script>

<script type="text/x-jsrender" id="TextView">
    <div>{{:QuestionText}}</div>
    <input type="text" disabled value="{{:AnswerText}}" title="{{:QuestionTitle}}" class="{{joinspace:Rules}}" />
    <a class="edit" href="/Survey/{{:SurveyId}}/{{:QuestionId}}">Edit</a>
</script>

<script type="text/x-jsrender" id="SingleChoiceView">
    <div>{{:QuestionText}}</div>
    <div class="btn-group btn-group-toggle" data-toggle="buttons">
        {{for Choices}}
                {{if ChoiceId == ~root.AnswerValue}}
                    <label for="ch_{{:ChoiceId}}" data-choice-id="{{:ChoiceId}}" class="btn btn-info disabled">
                        <input type="radio" name="sc_{{:~root.QuestionId}}" title="{{:Title}}" class="{{joinspace:Rules}}" disabled checked value="{{:ChoiceId}}" id="ch_{{:ChoiceId}}" />
                {{else}}
                    <label for="ch_{{:ChoiceId}}" data-choice-id="{{:ChoiceId}}" class="btn btn-secondary disabled">
                        <input type="radio" name="sc_{{:~root.QuestionId}}" title="{{:Title}}" class="{{joinspace:Rules}}" disabled value="{{:ChoiceId}}" id="ch_{{:ChoiceId}}" />
                {{/if}}
                {{:Description}}</label>
        {{/for}}
    </div>
    <a class="edit" href="/Survey/{{:SurveyId}}/{{:QuestionId}}">Edit</a>
</script>

<script type="text/x-jsrender" id="MultipleChoiceRankedView">
    <div>{{:QuestionText}}</div>
    <ul>
        {{for Choices}}
                <li data-choice-id="{{:ChoiceId}}" id="li_{{:ChoiceId}}" style="list-style-type: none">
                    <label class="index"></label>
                    <label>{{:Description}}</label>
                </li>
        {{/for}}

            {{if Rules && Rules.indexOf("other") > -1}}
                <li data-choice-id="0" id="li_0" style="list-style-type: none">
                    <label class="index"></label>
                    <label>Other:</label>
                    <input type="text" id="txt_0" class="{{joinspace:Rules}}" value="{{otheranswer:~root.AnswerText}}" disabled />
                </li>
        {{/if}}
    </ul>
    <input type="hidden" value="{{:Rules}}" id="rules" />
    <a class="edit" href="/Survey/{{:SurveyId}}/{{:QuestionId}}">Edit</a>
</script>

<script type="text/x-jsrender" id="MultipleChoiceView">
</script>

<script type="text/x-jsrender" id="RankedListView">
    <div>{{:RelatedQuestionText}}</div>
    <div>{{:QuestionText}}</div>
    <ul>
        {{for RelatedChoices}}
        <li data-choice-id="{{:ChoiceId}}">{{:Description}}
        </li>
        {{/for}}
        {{if RelatedRules && RelatedRules.indexOf('other') > -1}}
        <li data-choice-id="0">Other
            <input type="text" id="txt_0" disabled="disabled" />
        </li>
        {{/if}}
    </ul>
    <a class="edit" href="/Survey/{{:SurveyId}}/{{:RelatedQuestionId}}">Edit</a>
</script>


<script type="text/x-jsrender" id="DropDownListView">
    <div>{{:QuestionText}}</div>
    <div>
        {{for Choices}}
            {{if ChoiceId == ~root.AnswerValue}}
            {{:Description}}
            {{/if}}
            {{/for}}
    </div>
    <a class="edit" href="/Survey/{{:SurveyId}}/{{:QuestionId}}">Edit</a>
</script>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SurveyTitle.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.SurveyTitle" %>
<div id="title" runat="server"></div>
<script type="text/javascript">
    var <%=ClientID%>_ctrl = new hh.SurveyTitle({
        '$title': $('#<%=title.ClientID%>'),
        'surveyId': <%=SurveyID%>
        });
</script>
<script type="text/x-jsrender" id="titleTemplate">
    <h2>{{:Title}}</h2>
    <h3>{{:Description}}</h3>
    <h3>{{:Description2}}</h3>
</script>

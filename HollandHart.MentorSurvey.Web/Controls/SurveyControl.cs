﻿using HollandHart.MentorSurvey.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace HollandHart.MentorSurvey.Web.Controls
{
    public class SurveyControl : UserControl
    {

        public HtmlInputHidden sid = new HtmlInputHidden();
        public HtmlInputHidden qid = new HtmlInputHidden();

        protected void Page_Init(object sender, EventArgs e)
        {
            SurveyId = GetIntParam("s", 1);
            QuestionId = GetIntParam("q", 2);

            sid.ID = "sid";
            qid.ID = "qid";
            Controls.AddAt(0, sid);
            Controls.AddAt(1, qid);
        }

        private Model.Survey _survey = null;

        public Model.Survey Survey
        {
            get
            {
                if (_survey == null)
                {
                    if (SurveyId != -1)
                    {
                        using (var db = new HHMentorSurveyEntities())
                        {
                            var qry = from s in db.Surveys
                                      where s.SurveyId == SurveyId
                                      select s;
                            _survey = qry.FirstOrDefault();
                        }
                    }
                }
                return _survey;
            }
        }

        public int SurveyId
        {
            get
            {
                int i = -1;
                string s = sid.Value;
                Int32.TryParse(s, out i);
                return i;
            }

            set
            {
                sid.Value = Convert.ToString(value);
            }
        }

        public int QuestionId
        {
            get
            {
                int i = -1;
                string s = qid.Value;
                Int32.TryParse(s, out i);
                return i;
            }

            set
            {
                qid.Value = Convert.ToString(value);
            }
        }

        public int GetIntParam(string paramName, int position)
        {
            var s = Request.QueryString[paramName];
            if (string.IsNullOrWhiteSpace(s))
            {
                var ps = Request.Path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (ps.Length >= position + 1)
                {
                    s = ps[position];
                }
            }
            int i = -1;
            if (!Int32.TryParse(s, out i))
            {
                i = -1;
            }
            return i;
        }
    }
}
﻿<%@ control language="C#" autoeventwireup="true" codebehind="AdminMenu.ascx.cs" inherits="HollandHart.MentorSurvey.Web.Controls.AdminMenu" %>
<li class="nav-item dropdown">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        Admin<span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <a href="/Admin/UserProfiles" class="dropdown-item">User Profiles</a>
        <a href="/Admin/Users" class="dropdown-item">Users</a>
        <a href="/Admin/Matching" class="dropdown-item">Matching</a>
    </ul>
</li>

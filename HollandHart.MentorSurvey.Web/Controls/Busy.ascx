﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Busy.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.Busy" %>
<div id="busy" runat="server" style="display:none" class="busy alert alert-info">Loading...</div>
<script type="text/javascript">
    var <%=ClientID%>_ctrl = new hh.Busy($('#<%=busy.ClientID%>'));
</script>
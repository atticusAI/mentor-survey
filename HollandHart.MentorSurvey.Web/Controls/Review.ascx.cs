﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web.Controls
{
    public partial class Review :  SurveyControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Survey != null)
            {
                wrp.Attributes["style"] = Survey.SurveyStyle;
            }
        }
    }
}

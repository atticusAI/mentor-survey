﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web.Controls
{
    public partial class Volunteer : System.Web.UI.UserControl
    {
        public bool ShowMentor
        {
            get { return Convert.ToBoolean(hidMentor.Value); }
            set { hidMentor.Value = Convert.ToString(value).ToLower(); }
        }

        public bool ShowMentee
        {
            get { return Convert.ToBoolean(hidMentee.Value); }
            set { hidMentee.Value = Convert.ToString(value).ToLower(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}

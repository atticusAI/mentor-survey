﻿<%@ control language="C#" autoeventwireup="true" codebehind="MatchingSelector.ascx.cs" inherits="HollandHart.MentorSurvey.Web.Controls.MatchingSelector" %>

<div style="display: none">
    <div>
        <h3>Mentors</h3>
        <ul id="mentor" runat="server"></ul>
    </div>
    <div>
        <h3>Mentees</h3>
        <ul id="mentee" runat="server"></ul>
    </div>
</div>
<script type="text/x-jsrender" id="userTemplate">
    <li>
        <label>
            {{:Name}}
            <input type="checkbox" data-id="{{:UserId}}" />
        </label>
    </li>
</script>
<script type="text/javascript">
    <%=this.ClientID%>_ctrl = new hh.MatchingSelector($('#<%=mentor.ClientID%>'), $('#<%=mentee.ClientID%>'), '#userTemplate');
</script>



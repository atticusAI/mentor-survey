﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web.Controls
{
    public partial class MySurveys : System.Web.UI.UserControl
    {
        public bool ShowComplete
        {
            get { return Convert.ToBoolean(hidComplete.Value); }
            set { hidComplete.Value = (Convert.ToBoolean(value)).ToString().ToLower(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}

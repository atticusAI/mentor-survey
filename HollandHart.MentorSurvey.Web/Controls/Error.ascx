﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Error.ascx.cs" Inherits="HollandHart.MentorSurvey.Web.Controls.Error" %>
<div id="error" runat="server" style="display: none" class="alert alert-danger alert-dismissible">
    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <span class="message"></span>
</div>
<div id="success" runat="server" style="display: none" class="alert alert-success alert-dismissible">
    <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <span class="message"></span>
</div>
<script type="text/javascript">
    var <%=ClientID%>_ctrl = new hh.Error($('#<%=error.ClientID%>'), $('#<%=success.ClientID%>'));
</script>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web.Controls
{
    public partial class SurveyTitle : System.Web.UI.UserControl
    {
        private string _hiddenFieldID = "";

        public string HiddenFieldID
        {
            get { return _hiddenFieldID; }
            set { _hiddenFieldID = value; }
        }

        public string SurveyID
        {
            get
            {
                string id = "-1";
                HtmlInputHidden hid = Parent.FindControl(_hiddenFieldID) as HtmlInputHidden;
                if (hid != null)
                {
                    id = hid.Value;
                }
                return id;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
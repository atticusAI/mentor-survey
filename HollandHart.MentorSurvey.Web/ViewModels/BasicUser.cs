﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class BasicUser
    {
        public string ADLogin { get; set; }
        public string EmailAddress { get; set; }
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public string PracticeGroup { get; set; }
        public int UserId { get; set; }
    }
}
﻿using ExcelWriter;
using HollandHart.MentorSurvey.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class ExportProfile
    {
        [ExportCustom("Employee Name", 1)]
        public string EmployeeName { get; set; }

        [ExportCustom("Employee Email", 2)]
        public string EmployeeEmail { get; set; }

        [ExportCustom("Office", 3)]
        public string Office { get; set; }

        [ExportCustom("Practice Group", 4)]
        public string PracticeGroup { get; set; }

        [ExportCustom("Question", 5)]
        public string QuestionText { get; set; }

        public string QuestionType { get; set; }

        public int AnswerId { get; set; }

        [ExportCustom("Answer", 6)]
        public string Answer { get; set; }

        public int UserId { get; set; }
    }
}
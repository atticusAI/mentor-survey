﻿using ExcelWriter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class ExportSuggestedMatches
    {
        [ExportCustom("Mentor", 1)]
        public string MentorName { get; set; }

        [ExportCustom("Mentor Description", 2)]
        public string MentorDescription { get; set; }

        [ExportCustom("Score", 3)]
        public decimal Score { get; set; }

        [ExportCustom("Mentee", 4)]
        public string MenteeName { get; set; }

        [ExportCustom("Mentee Description", 5)]
        public string MenteeDescription { get; set; }
    }
}

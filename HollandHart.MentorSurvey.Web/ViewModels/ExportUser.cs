﻿using ExcelWriter;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class ExportUser
    {
        [ExportCustom("Employee ID", 1)]
        public string EmployeeID { get; set; }

        [ExportCustom("Employee Name", 2)]
        public string Name { get; set; }

        [ExportCustom("Practice Group", 3)]
        public string PracticeGroup { get; set; } 

        [ExportCustom("Email Address", 4)]
        public string EmailAddress { get; set; }
    }
}
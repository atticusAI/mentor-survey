﻿using ExcelWriter;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class ExportMenteeProfile2018
    {
        public const string QUESTION_1_TEXT = "Select your Level:";
        public const string QUESTION_2_TEXT = "Would you prefer a mentor in your same\r\n" +
            " office or a different office?";
        public const string QUESTION_3_TEXT = "Would you prefer a mentor in the same\r\n" +
            " practice group or a different practice\r\n" +
            " group?";
        public const string QUESTION_4_TEXT = "Would you prefer a mentor of\r\n" +
            " your same gender?";
        public const string QUESTION_5_TEXT = "How many mentors would you like?\r\n" +
            " You may be matched with up to 2 mentors.";
        public const string QUESTION_6_TEXT = "Please select up to 5 areas in which you\r\n" +
            " would like to be mentored.";
        public const string QUESTION_7_TEXT = "In a few sentences please describe what you are looking\r\n" +
            " for in a mentor. Please also use this space to provide\r\n" +
            " any additional information including whether you would\r\n" +
            " prefer a diverse mentor or if you already have a\r\n" +
            " mentor in mind who you would like to be paired with.";

        [ExportCustom("Employee Name", 1)]
        public string EmployeeName { get; set; }

        [ExportCustom("Employee Email", 2)]
        public string EmployeeEmail { get; set; }

        [ExportCustom("Office", 3)]
        public string Office { get; set; }

        [ExportCustom("Practice Group", 4)]
        public string PracticeGroup { get; set; }

        public string Question1 { get; set; }
        [ExportCustom(QUESTION_1_TEXT, 5)]
        public string Answer1 { get; set; }

        public string Question2 { get; set; }
        [ExportCustom(QUESTION_2_TEXT, 6)]
        public string Answer2 { get; set; }

        public string Question3 { get; set; }
        [ExportCustom(QUESTION_3_TEXT, 7)]
        public string Answer3 { get; set; }

        public string Question4 { get; set; }
        [ExportCustom(QUESTION_4_TEXT, 8)]
        public string Answer4 { get; set; }

        public string Question5 { get; set; }
        [ExportCustom(QUESTION_5_TEXT, 9)]
        public string Answer5 { get; set; }

        public string Question6_1 { get; set; }
        [ExportCustom("1. " + QUESTION_6_TEXT, 10)]
        public string Answer6_1 { get; set; }

        public string Question6_2 { get; set; }
        [ExportCustom("2. " + QUESTION_6_TEXT, 11)]
        public string Answer6_2 { get; set; }

        public string Question6_3 { get; set; }
        [ExportCustom("3. " + QUESTION_6_TEXT, 12)]
        public string Answer6_3 { get; set; }

        public string Question6_4 { get; set; }
        [ExportCustom("4. " + QUESTION_6_TEXT, 13)]
        public string Answer6_4 { get; set; }

        public string Question6_5 { get; set; }
        [ExportCustom("5. " + QUESTION_6_TEXT, 14)]
        public string Answer6_5 { get; set; }

        public string Question7 { get; set; }
        [ExportCustom(QUESTION_7_TEXT, 15)]
        public string Answer7 { get; set; }

        public int UserId { get; set; }
    }
}
﻿using ExcelWriter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class ExportSuggestedMatches2018
    {
        public const string MENTOR_QUESTION_1_TEXT = "Mentor Level:";
        public const string MENTOR_QUESTION_2_TEXT = "Would you prefer a mentee in your same\r\n" +
            " office or a different office?";
        public const string MENTOR_QUESTION_3_TEXT = "Would you prefer a mentee in your same\r\n" +
            " practice group or a different practice group?";
        public const string MENTOR_QUESTION_4_TEXT = "Would you prefer a mentee of your\r\n" +
            " same gender?";
        public const string MENTOR_QUESTION_5_TEXT = "How many mentees are\r\n" +
            " you willing and able to mentor?";
        public const string MENTOR_QUESTION_6_TEXT = "Please select up to 5 areas in which you\r\n" +
            " would like to provide mentorship.";
        public const string MENTOR_QUESTION_7_TEXT = "In a few sentences please describe what makes you a unique\r\n" +
            " mentor and what you want to share with your mentee. Please\r\n" +
            " also use this space to provide any additional information,\r\n" +
            " including whether you would prefer a diverse mentee, or if\r\n" +
            " you already have a mentee in mind that you would like to be\r\n" +
            " paired with.";

        [ExportCustom("Mentor Name", 1)]
        public string MentorName { get; set; }

        [ExportCustom("Mentor Email", 2)]
        public string MentorEmail { get; set; }

        [ExportCustom("Mentor Office", 3)]
        public string MentorOffice { get; set; }

        [ExportCustom("Mentor Practice Group", 4)]
        public string MentorPracticeGroup { get; set; }

        public string MentorQuestion1 { get; set; }
        [ExportCustom(MENTOR_QUESTION_1_TEXT, 5)]
        public string MentorAnswer1 { get; set; }

        public string MentorQuestion2 { get; set; }
        [ExportCustom(MENTOR_QUESTION_2_TEXT, 6)]
        public string MentorAnswer2 { get; set; }

        public string MentorQuestion3 { get; set; }
        [ExportCustom(MENTOR_QUESTION_3_TEXT, 7)]
        public string MentorAnswer3 { get; set; }

        public string MentorQuestion4 { get; set; }
        [ExportCustom(MENTOR_QUESTION_4_TEXT, 8)]
        public string MentorAnswer4 { get; set; }

        public string MentorQuestion5 { get; set; }
        [ExportCustom(MENTOR_QUESTION_5_TEXT, 9)]
        public string MentorAnswer5 { get; set; }

        public string MentorQuestion6_1 { get; set; }
        [ExportCustom("1. " + MENTOR_QUESTION_6_TEXT, 10)]
        public string MentorAnswer6_1 { get; set; }

        public string MentorQuestion6_2 { get; set; }
        [ExportCustom("2. " + MENTOR_QUESTION_6_TEXT, 11)]
        public string MentorAnswer6_2 { get; set; }

        public string MentorQuestion6_3 { get; set; }
        [ExportCustom("3. " + MENTOR_QUESTION_6_TEXT, 12)]
        public string MentorAnswer6_3 { get; set; }

        public string MentorQuestion6_4 { get; set; }
        [ExportCustom("4. " + MENTOR_QUESTION_6_TEXT, 13)]
        public string MentorAnswer6_4 { get; set; }

        public string MentorQuestion6_5 { get; set; }
        [ExportCustom("5. " + MENTOR_QUESTION_6_TEXT, 14)]
        public string MentorAnswer6_5 { get; set; }

        public string MentorQuestion7 { get; set; }
        [ExportCustom(MENTOR_QUESTION_7_TEXT, 15)]
        public string MentorAnswer7 { get; set; }

        [ExportCustom("Score", 16, HighlightColor = "Yellow")]
        public decimal Score { get; set; }


        public const string MENTEE_QUESTION_1_TEXT = "Mentee Level:";
        public const string MENTEE_QUESTION_2_TEXT = "Would you prefer a mentor in your same\r\n" +
            " office or a different office?";
        public const string MENTEE_QUESTION_3_TEXT = "Would you prefer a mentor in the same\r\n" +
            " practice group or a different practice group?";
        public const string MENTEE_QUESTION_4_TEXT = "Would you prefer a mentor of\r\n" +
            " your same gender?";
        public const string MENTEE_QUESTION_5_TEXT = "How many mentors would you like?\r\n" +
            " You may be matched with up to 2 mentors.";
        public const string MENTEE_QUESTION_6_TEXT = "Please select up to 5 areas in which you\r\n" +
            " would like to be mentored.";
        public const string MENTEE_QUESTION_7_TEXT = "In a few sentences please describe what you are looking\r\n" +
            " for in a mentor. Please also use this space to provide\r\n" +
            " any additional information including whether you would\r\n" +
            " prefer a diverse mentor or if you already have a\r\n" +
            " mentor in mind who you would like to be paired with.";

        [ExportCustom("Mentee Name", 17)]
        public string MenteeName { get; set; }

        [ExportCustom("Mentee Email", 18)]
        public string MenteeEmail { get; set; }

        [ExportCustom("Mentee Office", 19)]
        public string MenteeOffice { get; set; }

        [ExportCustom("Mentee Practice Group", 20)]
        public string MenteePracticeGroup { get; set; }

        public string MenteeQuestion1 { get; set; }
        [ExportCustom(MENTEE_QUESTION_1_TEXT, 21)]
        public string MenteeAnswer1 { get; set; }

        public string MenteeQuestion2 { get; set; }
        [ExportCustom(MENTEE_QUESTION_2_TEXT, 22)]
        public string MenteeAnswer2 { get; set; }

        public string MenteeQuestion3 { get; set; }
        [ExportCustom(MENTEE_QUESTION_3_TEXT, 23)]
        public string MenteeAnswer3 { get; set; }

        public string MenteeQuestion4 { get; set; }
        [ExportCustom(MENTEE_QUESTION_4_TEXT, 24)]
        public string MenteeAnswer4 { get; set; }

        public string MenteeQuestion5 { get; set; }
        [ExportCustom(MENTEE_QUESTION_5_TEXT, 25)]
        public string MenteeAnswer5 { get; set; }

        public string MenteeQuestion6_1 { get; set; }
        [ExportCustom("1. " + MENTEE_QUESTION_6_TEXT, 26)]
        public string MenteeAnswer6_1 { get; set; }

        public string MenteeQuestion6_2 { get; set; }
        [ExportCustom("2. " + MENTEE_QUESTION_6_TEXT, 27)]
        public string MenteeAnswer6_2 { get; set; }

        public string MenteeQuestion6_3 { get; set; }
        [ExportCustom("3. " + MENTEE_QUESTION_6_TEXT, 28)]
        public string MenteeAnswer6_3 { get; set; }

        public string MenteeQuestion6_4 { get; set; }
        [ExportCustom("4. " + MENTEE_QUESTION_6_TEXT, 29)]
        public string MenteeAnswer6_4 { get; set; }

        public string MenteeQuestion6_5 { get; set; }
        [ExportCustom("5. " + MENTEE_QUESTION_6_TEXT, 30)]
        public string MenteeAnswer6_5 { get; set; }

        public string MenteeQuestion7 { get; set; }
        [ExportCustom(MENTEE_QUESTION_7_TEXT, 31)]
        public string MenteeAnswer7 { get; set; }

        public int MentorId { get; set; }

        public int MenteeId { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class ImportGroup
    {
        [JsonProperty(PropertyName = "group")]
        public string Group { get; set; }

        [JsonProperty(PropertyName = "users")]
        public string[] Users { get; set; }
    }
}
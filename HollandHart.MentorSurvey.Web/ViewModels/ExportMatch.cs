﻿using ExcelWriter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HollandHart.MentorSurvey.Web.ViewModels
{
    public class ExportMatch
    {
        [ExportCustom("Mentor Name", 1)]
        public string MentorName { get; set; }

        [ExportCustom("Mentor Email", 2)]
        public string MentorEmail { get; set; }

        [ExportCustom("Mentee Name", 3)]
        public string MenteeName { get; set; }

        [ExportCustom("Mentee Email", 4)]
        public string MenteeEmail { get; set; }


    }
}
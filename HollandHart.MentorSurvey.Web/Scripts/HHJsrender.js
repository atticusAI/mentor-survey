﻿(function (hh) {
    'use  strict';

    // jsrender converters
    $.views.converters('joinspace', function (val) {
        return (val || []).join(' ');
    });

    $.views.converters('otheranswer', function (val) {
        var r = '';
        try {
            r = JSON.parse(val).other;
        } catch (x) {
            r = '';
        }
        return r;
    });

    $.views.converters('orderanswer', function (val) {
        var r = '';
        try {
            r = JSON.parse(val).order;
        } catch (x) {
            r = '';
        }
        return r;
    });

})(window.hh = window.hh || {});

﻿(function (hh) {
    'use  strict';

    // post render on the review screen
    hh.postrenderMultipleChoiceRankedReview = function (answer, $div) {
        var buffer = $('<ul />');
        var $ul = $div.find('ul');
        var order = JSON.parse(answer.AnswerText).order;
        $.each(order, function (idx, val) {
            if (idx > 4) return false;
            var $li = $div.find('li[data-choice-id="' + val + '"]');
            var $idx = $li.find('label.index');
            $idx.text((idx + 1) + '. ');
            buffer.append($li);
        });
        $ul.html(buffer.html());

        return $div;
    };

    hh.postrenderRankedListReview = function (answer, $div) {
        var buffer = $('<ul/>');
        var $ul = $div.find('ul');
        var ans = JSON.parse(answer.RelatedAnswers[0].Text);
        $.each(ans.selected, function (idx, val) {
            var $li = $ul.find('li[data-choice-id="' + val + '"]');
            if ($li.length) buffer.append($li);
        });
        $ul.html(buffer);
        var $txt = $ul.find('input:text');
        if ($txt.length) {
            $txt.val(ans.other);
        }
        return $div;
    };

    hh.postrenderMultipleChoiceReview = function (answer, $div) {
        $div.hide();
    };

    //controller classes
    hh.SurveyTitle = function (options) {
        var api = new hh.WebApiController('/api/Survey/');
        var o = {
            $title: null,
            surveyId: null,
            template: '#titleTemplate'
        };

        var init = function () {
            hh.merge(o, options);

            api.getAction('Title', [o.surveyId]).then(
                function (res) {
                    var tmp = $.templates(o.template);
                    o.$title.html(tmp.render(res));
                }, hh.throwError);
        };

        $(document).ready(init);
    };

    hh.Review = function ($div, $sid, $bBack, $bComp) {
        var me = this;
        me.$div = $div;
        me.$sid = $sid;
        me.$bBack = $bBack;
        me.$bComp = $bComp;
        me.api = new hh.WebApiController('/api/Survey/');

        me.buttonState = function (hide) {
            if (hide) {
                me.$bBack.hide();
                me.$bComp.hide();
            } else {
                me.$bBack.show();
                me.$bComp.show();
            }
        };

        me.renderAnswers = function (answers) {
            var i;
            var tmp;
            var $d;
            me.$div.html('');
            for (i = 0; i < answers.length; i++) {
                tmp = $.templates('#' + answers[i].QuestionTypeName + 'View');
                $d = $('<div/>').attr('class', 'alert alert-light')
                    .html(tmp.render(answers[i]));
                if (hh['postrender' + answers[i].QuestionTypeName + 'Review']) {
                    d = hh['postrender' + answers[i].QuestionTypeName + 'Review'](answers[i], $d);
                }
                me.$div.append($d);
            }
            if (answers[0].Complete) {
                $('a.edit').hide();
            }
        };

        var init = function () {
            me.api.getAction('Answers', me.$sid.val()).then(
                function (res) {
                    me.renderAnswers(res);
                    me.buttonState(res[0].Complete);
                }, hh.throwError);

            me.$bBack.click(function () {
                window.location.href = '/Survey/' + me.$sid.val();
            });

            me.$bComp.click(function () {
                me.api.putAction('Complete', me.$sid.val()).then(
                    function (res) {
                        // hh.throwSuccess('Your survey is complete.  Thank you.  Click <a href="/">here</a> to return to the home page.');
                        window.location.href = '/Complete/' + me.$sid.val();
                    }, hh.throwError);
            });
        };

        $(document).ready(init);
    };

    hh.UserManager = function ($sel, $imports, $submit, $export, $view, tmp, length) {
        var me = this;
        me.$sel = $sel;
        me.$imports = $imports;
        me.$submit = $submit;
        me.$export = $export;
        me.$view = $view;
        me.$next = null;
        me.$prev = null;
        me.tmp = tmp;
        me.api = new hh.WebApiController('/api/UserAdmin/');
        me.page = 0;
        me.count = 0;
        me.length = length || 15;

        me.import = function () {
            var $u = me.$sel.find('input:radio:checked').first();
            if ($u.length) {
                me.api.postAction('Import', { group: $u.val(), users: me.$imports.val().split('\n') }).then(
                    function (res) {
                        me.$imports.val('');
                        me.loadPage();
                    }, hh.throwError);
            }
        };

        me.remove = function (evt, data) {
            var $u = me.$sel.find('input:radio:checked').first();
            evt.preventDefault();

            if ($u.length) {
                me.api.deleteAction($u.val(), $(this).attr('data-id')).then(
                    function (res) {
                        me.loadPage(me.page)
                    }, hh.throwError);
            }
        };

        me.selChange = function () {
            me.loadPage(0);
        };

        me.loadPage = function (page) {
            var grp;
            var $u = me.$sel.find('input:radio:checked').first();
            if ($u.length) {
                grp = $u.val();
                me.api.getAction(grp, ['Count']).then(
                    function (res) {
                        me.count = res;
                        if (res && page <= Math.ceil(res / me.length)) {
                            me.getPage(page);
                        } else {
                            me.page = 0;
                            me.getPage(0);
                        }
                    },
                    hh.throwError);
            }
        };

        me.enableNav = function () {
            if (me.page <= 0) {
                me.$prev.prop('disabled', true);
            } else {
                me.$prev.prop('disabled', false);
            }

            if (me.page >= Math.floor(me.count / me.length)) {
                me.$next.prop('disabled', true);
            } else {
                me.$next.prop('disabled', false);
            }

            if (me.length > 0) {
                me.$export.prop('disabled', false);
            } else {
                me.$export.prop('disabled', true);
            }
        };

        me.getPage = function (page) {
            var $u = me.$sel.find('input:radio:checked').first();
            if ($u.length) {
                me.api.getAction('UserGroup', ['Category', $u.val(), 'Page', page, 'Length', me.length]).then(
                    function (res) {
                        var tmp = $.templates(me.tmp);
                        me.$view.show();
                        me.$view.find('tbody').html(tmp.render(res));
                        me.$view.find('a.remove').click(me.remove);
                        me.page = page;
                        me.enableNav();
                    }, hh.throwError);
            } else {
                me.$view.find('tbody').html(tmp.render([]));
            }
        };


        me.prev = function () {
            me.loadPage(me.page - 1);
        };

        me.next = function () {
            me.loadPage(me.page + 1);
        };

        me.export = function () {
            var $grp = me.$sel.find('input:radio:checked').first();
            window.open('/api/export/users/type/' + $grp.val(), '_blank');
        };

        me.init = function () {
            me.$submit.click(me.import);
            me.$sel.change(me.selChange);
            me.$prev = me.$view.find('input[type="button"][value="Previous"]').first();
            me.$prev.click(me.prev);
            me.$next = me.$view.find('input[type="button"][value="Next"]').first();
            me.$next.click(me.next);
            me.$export.click(me.export);
        };

        $(document).ready(me.init);
    };

    hh.Volunteer = function ($mentor, $mentee, $amentor, $amentee) {
        var me = this;
        me.$mentor = $mentor;
        me.$mentee = $mentee;
        me.$amentor = $amentor;
        me.$amentee = $amentee;
        me.api = new hh.WebApiController('/api/User/');

        me.init = function () {
            me.$amentor.click(function () {
                me.api.postAction('VolunteerMentor/0', {}).then(
                    function (res) {
                        me.$amentor.hide();
                        hh.throwSuccess('Thank you for volunteering as a mentor. Please take the <a href="/Survey?s=' + res + '">Mentor Survey</a>');
                    }, hh.throwError);
                return false;
            });

            me.$amentee.click(function () {
                me.api.postAction('VolunteerMentee/0', {}).then(
                    function (res) {
                        me.$amentee.hide();
                        hh.throwSuccess('Thank you for volunteering as a mentee. Please take the <a href="/Survey?s=' + res + '">Mentee Survey</a>');
                    }, hh.throwError);
                return false;
            });

            if (me.$mentor.val() === 'true') {
                me.api.getAction('IsMentor', [0]).then(
                    function (res) {
                        if (res === false) me.$amentor.show();
                    }, hh.throwError);
            }

            if (me.$mentee.val() === 'true') {
                me.api.getAction('IsMentee', [0]).then(
                    function (res) {
                        if (res === false) me.$amentee.show();
                    }, hh.throwError);
            }
        };

        $(document).ready(me.init);
    };

    hh.MySurveys = function ($div, vw, $comp) {
        var me = this;
        me.$div = $div;
        me.vw = vw;
        me.$comp = $comp;
        me.api = new hh.WebApiController('/api/User/');

        me.init = function () {
            me.api.getAction('Surveys', [me.$comp.val()]).then(
                function (res) {
                    var tmp = $.templates('#' + me.vw);
                    if (res && res.length)
                        me.$div.html(tmp.render(res));
                }, hh.throwError);
        };

        $(document).ready(me.init);
    };

    hh.TextAnswer = function ($div, question, api) {
        var me = this;
        me.$div = $div;
        me.$txt = me.$div.find('input.text').first();
        me.question = question;
        me.api = api;

        me.val = function () {
            return me.$div.find('input:text').first().val();
        };

        me.save = function () {
            return new Promise(function (resolve, reject) {
                if (hh.validate(me.$div.find('input:text').first(), me.$div.find('div.alert').first())) {
                    me.question.Answer.Text = me.val();
                    me.question.Answer.Value = '';
                    me.api.putAction('Answer', me.question.Answer.AnswerId, [], me.question.Answer).then(resolve, reject);
                } else {
                    reject(new Error("Input is invalid."));
                }
            });
        };

        me.dispose = function () {

        };

        me.init = function () {
            me.$txt.on('blur', function () {
                me.save.then(
                    function (res) {

                    }, hh.throwError);
            });
        };

        me.init();
    };

    hh.CommentAnswer = function ($div, question, api, autosaveTime) {
        var me = this;
        me.$div = $div;
        me.question = question;
        me.api = api;
        me.autosaveTime = autosaveTime || 5000;
        me.timer = null;
        me.saved = true;

        me.val = function () {
            return me.$div.find('textarea').val();
        };

        me.save = function () {
            return new Promise(function (resolve, reject) {
                if (hh.validate(me.$div.find('textarea').first(), me.$div.find('div.alert').first())) {
                    if (!me.saved) {
                        me.question.Answer.Text = me.val();
                        me.question.Answer.Value = '';
                        me.api.putAction('Answer', me.question.Answer.AnswerId, [], me.question.Answer).then(
                            function (res) {
                                me.saved = true;
                                resolve(true);
                            }, reject);
                    } else {
                        resolve(true);
                    }
                } else {
                    reject(new Error('Input is invalid.'));
                }
            });
        };

        me.autosave = function () {
            me.save().then(me.StartTimer, hh.throwError);
        };

        me.startTimer = function () {
            me.timer = window.setTimeout(me.autosave, me.autosaveTime);
        };

        me.dispose = function () {
            window.clearTimeout(me.timer);
        };

        me.init = function () {
            me.startTimer();
            me.$div.find('textarea').on('keyup', function () {
                me.saved = false;
            });

            me.$div.find('textarea').on('blur', function () {
                me.save().then(
                    function (res) {
                    }, hh.throwError);
            });
        };

        me.init();
    };

    hh.SingleChoiceAnswer = function ($div, question, api) {
        var me = this;
        me.$div = $div;
        me.question = question;
        me.api = api;

        me.val = function (v) {
            var $r;
            if (v) {
                me.$div.find('label').removeClass('active');
                me.$div.find('label[data-choice-id="' + v + '"]').addClass('active');
            }

            $r = me.$div.find('label.active');
            return $r.length ? $r.attr('data-choice-id') : -1;
        };

        me.save = function () {
            me.question.Answer.Text = '';
            me.question.Answer.Value = me.val();
            return me.api.putAction('Answer', me.question.Answer.AnswerId, [], me.question.Answer);
        };

        me.dispose = function () {

        };

        me.init = function () {
            me.val(me.question.Answer.Value);
            me.$div.find('label.btn.choice').on('click', function () {
                var $btn = $(this);
                me.val($btn.val());
                me.save().then(
                    function (res) {
                        $btn.addClass('btn-success')
                            .siblings().removeClass('btn-success');
                    }, hh.throwError);
            });
        };

        me.init();
    };

    hh.MultipleChoiceRankedAnswer = function ($div, question, api) {
        var me = this;
        me.$div = $div;
        me.question = question;
        me.api = api;
        me.srt = null;
        me.selectCount = 5;

        me.val = function () {
            var r = {};
            r.order = $(me.srt).sortable('toArray', { attribute: 'data-choice-id' });
            r.other = me.$div.find("input:text").val();
            return r;
        };

        me.save = function () {
            return new Promise(function (resolve, reject) {
                var valid = true;
                var $inp = me.$div.find('input:text');
                var $err = me.$div.find('div[role="alert"]');
                var $chk;
                if ($inp.length) {
                    $chk = me.$div.find('input:checkbox[data-choice-id="0"]:checked');
                    if ($chk.length) {
                        valid = hh.validate($inp, $err);
                    }
                }

                if (valid) {
                    me.question.Answer.Text = JSON.stringify(me.val());
                    me.question.Answer.Value = '';
                    me.api.putAction('Answer', me.question.Answer.AnswerId, [], me.question.Answer).then(resolve, reject);
                } else {
                    reject(new Error("Input is invalid."));
                }
            });
        };

        me.move = function (chk) {
            var $li = $(chk).parent();
            var dci = $(chk).attr('data-choice-id');
            var $lc = me.srt.find('input:checkbox:checked[data-choice-id!="' + dci + '"]').last().parent();

            $li.toggle('drop');
            if ($(chk).is(':checked')) {
                if ($lc.length === 0) {
                    $lc = me.srt.find('li').first();
                    $li.insertBefore($lc);
                } else {
                    if ($lc.index() === me.selectCount - 1) {
                        $li.insertBefore($lc);
                    } else {
                        $li.insertAfter($lc);
                    }
                }
            } else {
                $li.insertAfter(me.srt.find('li').last());
            }
            $li.toggle('drop', function () { me.update(true); });
        };

        me.update = function (save) {
            $.each(me.srt.find('li'), function (idx, val) {
                if (idx >= me.selectCount) {
                    $(val).find('input:checkbox').prop('checked', false);
                } else {
                    $(val).find('input:checkbox').prop('checked', true);
                }

                if (idx < me.selectCount) {
                    $(val).find('label.index').text(idx + 1);
                } else {
                    $(val).find('label.index').text('');
                }
            });
            if (save) {
                me.save().then(function (res) {

                }, hh.throwError);
            }
        };

        me.reorder = function (order) {
            var buffer = $('<ul />');
            $.each(order, function (idx, val) {
                buffer.append(me.srt.find('li[data-choice-id="' + val + '"]'));
            });
            me.srt.html(buffer.html());
            me.update();
        };

        me.dispose = function () {
        };

        me.init = function () {
            var $rules = me.$div.find('#rules');
            var opts;
            var a;
            if ($rules.length) {
                opts = hh.parseOptions($rules.val());
                if (opts && opts.selected) {
                    me.selectCount = opts.selected;
                }
            }

            me.srt = me.$div.find('ul').sortable({ update: me.update });

            if (me.question.Answer.Text) {
                a = JSON.parse(me.question.Answer.Text)
                me.reorder(a.order);
                if (a.other) {
                    me.$div.find('input:text').val(a.other);
                }
            } else {
                me.update();
            }

            me.srt.find('input:checkbox').on('change', function () {
                me.move(this);
            });
        };

        me.init();
    };

    hh.RankedListAnswer = function ($div, question, api) {
        var me = this;
        var srt = null;
        me.$div = $div;
        me.question = question;
        me.api = api;

        me.val = function () {
            var r = {
                selected: [],
                other: ''
            };
            srt.find('li').each(function (idx, val) {
                r.selected.push($(val).attr('data-choice-id'))
            });
            r.other = srt.find("input:text").val();
            return r;
        };

        me.save = function () {
            return new Promise(function (resolve, reject) {
                me.question.Answer.Text = JSON.stringify(me.val());
                me.question.Answer.Value = '';
                me.question.Answer.AnswerId = me.question.RelatedAnswers[0].AnswerId;
                me.question.Answer.QuestionId = me.question.RelatedAnswers[0].QuestionId;
                me.api.putAction('Answer', me.question.RelatedAnswers[0].AnswerId, [], me.question.Answer).then(resolve, reject);
            });
        };

        me.dispose = function () {

        };

        var update = function () {
            console.log('foo');
        };

        var up = function (evt, data) {
            evt.preventDefault();
            move($(this).parent(), 1);
        };

        var down = function (evt, data) {
            evt.preventDefault();
            move($(this).parent(), -1);
        };

        var move = function ($li, direction) {
            $li.hide('fade', function () {
                if (direction > 0) {
                    $li.prev().before($li);
                } else if (direction < 0) {
                    $li.next().after($li);
                }
                $li.show('fade');
            });
        };

        var reorder = function (order) {
            var buffer = $('<ul />');
            $.each(order, function (idx, val) {
                buffer.append(srt.find('li[data-choice-id="' + val + '"]'));
            });
            srt.html(buffer.html());
        };

        var update = function () {

        };

        var init = function () {
            var v = JSON.parse(me.question.RelatedAnswers[0].Text);
            $.each(me.$div.find('li'), function (idx, val) {
                var $li = $(val);
                var id = $li.attr('data-choice-id');
                if (v.selected.indexOf(id) === -1) {
                    $li.remove();
                }
            });
            srt = me.$div.find('ul').sortable({ update: update });
            reorder(v.selected);
            if (v.other) {
                srt.find('input:text').val(v.other);
            }

            me.$div.find('a.move.up').click(up);
            me.$div.find('a.move.down').click(down);
        };

        init();
    };

    hh.MultipleChoiceAnswer = function ($div, question, api) {
        var me = this;
        var srt = null;
        me.$div = $div;
        me.question = question;
        me.api = api;
        me.opts = {
            selected: 5,
            requirescheck: true,
            other: false
        }

        me.val = function (v) {
            if (v) {
                for (var i = 0; i < v.selected.length; i++) {
                    $div.find('input:checkbox[data-choice-id=' + v.selected[i] + ']').attr('checked', true);
                }
                $div.find('input.other').val(v.other);
                reorder(v.selected);
            }

            var r = {
                selected: [],
                other: ''
            };
            $div.find('input:checkbox:checked').each(function (idx, val) {
                r.selected.push($(val).attr('data-choice-id'))
            });
            r.other = me.$div.find("input:text").val();
            return r;
        };

        me.dispose = function () {

        };

        me.save = function () {
            return new Promise(function (resolve, reject) {
                var valid = true;
                if (srt.find('input:checkbox.other:checked').length) {
                    srt.find('input:text.other').addClass('required');
                    valid = hh.validate(me.$div.find('input:text'), me.$div.find('div.other[role="alert"]'))
                }

                valid = valid & hh.validate(me.$div.find('ul'), me.$div.find('div.err[role="alert"]'));

                if (valid) {
                    me.question.Answer.Text = JSON.stringify(me.val());
                    me.question.Answer.Value = '';
                    me.api.putAction('Answer', me.question.Answer.AnswerId, [], me.question.Answer).then(resolve, reject);
                } else {
                    reject(new Error("Input is invalid."));
                }
            });
        };

        var check = function (evt, data) {
            var c = $div.find('input:checkbox:checked').length;
            if (c > me.opts.selected) {
                $(this).prop('checked', false);
            } else {
                me.save();
            }
        };

        var text = function (evt, data) {
            if ($(this).val()) {
                me.save();
            }
        }

        var reorder = function (order) {
            var buffer = $('<ul />');
            var buffer2 = $('<ul />');

            $.each(order, function (idx, val) {
                buffer.append(srt.find('li[data-choice-id="' + val + '"]'));
            });

            $.each(srt.find('li'), function (idx, val) {
                if (order.indexOf($(val).attr('data-choice-id')) === -1) {
                    buffer2.append(val);
                }
            });

            srt.html(buffer)
                .append(buffer2);
        };

        var init = function () {
            hh.merge(me.opts, hh.parseOptions(me.$div.find('#rules').val()));
            srt = me.$div.find('ul');
            srt.find('input:checkbox').on('change', check);
            srt.find('input:text').on('change', text);
            if (me.question.Answer.Text) {
                me.val(JSON.parse(me.question.Answer.Text));
            }
        };

        init();
    };

    hh.DropDownListAnswer = function ($div, question, api) {
        var me = this;
        me.$div = $div;
        me.question = question;
        me.api = api;
        me.$sel = me.$div.find('select').first();

        me.val = function (v) {
            if (v) {
                me.$sel.val(v);
            }
            return me.$sel.val();
        };

        me.save = function () {
            return new Promise(function (resolve, reject) {
                me.question.Answer.Text = '';
                me.question.Answer.Value = me.val();
                me.api.putAction('Answer', me.question.Answer.AnswerId, [], me.question.Answer).then(resolve, reject);
            });
        };

        me.dispose = function () {

        };

        var init = function () {
            me.val(me.question.Answer.Value);
            me.$sel.on('change', function () {
                me.save().then(function (res) {
                    me.$sel.addClass('btn-success');
                }, hh.throwError);
            });
        };

        init();
    };

    hh.Survey = function (options) {
        var me = this;
        var o = {
            $sid: null,
            $qid: null,
            $wrp: null,
            $question: null,
            $prev: null,
            $next: null,
            $review: null,
            $question: null,
            count: 0,
            current: 0,
            answer: null,
            uid: null
        };

        me.api = new hh.WebApiController('/api/Survey/');

        me.load = function (sid, qid, uid) {
            o.uid = uid;
            if (sid && sid > 0) {
                return getCount(sid).then(
                    function (res) {
                        o.count = res;
                        if (qid && qid > 0) {
                            return getIndex(sid, qid).then(
                                function (res) {
                                    o.current = res;
                                    return getQuestion(sid, res).then(draw)
                                });
                        } else {
                            return getQuestion(sid, 0).then(draw);
                        }
                    });
            }
        };

        me.hide = function () {
            o.$wrp.hide();
        };

        me.show = function () {
            o.$wrp.show();
        };

        var userParam = function () {
            if (o.uid) {
                return ['User', o.uid];
            } else {
                return [];
            }
        };

        var dispose = function () {
            if (o.answer) {
                o.answer.dispose();
            }
        };

        var getCount = function (sid) {
            return me.api.getAction('Count', [sid]);
        };

        var getIndex = function (sid, qid) {
            return me.api.getAction('QuestionIndex', [sid, 'Question', qid].concat(userParam()));
        };

        var getQuestion = function (sid, idx) {
            return me.api.getAction('Question', [sid, 'Index', idx].concat(userParam())).then(
                function (res) {
                    var num = { "Number": idx + 1 };
                    hh.merge(res, num);
                    o.current = idx;
                    o.question = res;
                    o.$sid.val(res.Answer.SurveyId);
                    o.$qid.val(res.QuestionId);
                });
        };

        var navState = function () {
            if (o.current <= 0) {
                o.$prev.prop('disabled', true);
            } else {
                o.$prev.prop('disabled', false);
            }

            if (o.current + 1 >= o.count) {
                o.$next.prop('disabled', true).hide();
                o.$review.show();
            } else {
                o.$next.prop('disabled', false).show();
                o.$review.hide();
            }

            if (o.uid) {
                o.$review.hide();
            }
        };

        var answerState = function () {
            o.answer = new hh[o.question.QuestionTypeName + 'Answer'](o.$question, o.question, me.api);
        };

        var questionState = function () {
            var tmp = $.templates('#' + o.question.QuestionTypeName + 'View');
            o.$question.html(tmp.render(o.question));
        };

        var draw = function () {
            navState();
            questionState();
            answerState();
        };

        var navigate = function (sid, np) {
            return o.answer.save().then(
                function (res) {
                    o.answer.dispose();
                    delete o.answer;
                    return getQuestion(sid, o.current + (1 * np));
                });
        };

        var next = function (evt, data) {
            navigate(o.$sid.val(), +1).then(draw, hh.throwError);
        };

        var prev = function (evt, data) {
            navigate(o.$sid.val(), -1).then(draw, hh.throwError);
        };

        var review = function (evt, data) {
            if (o.answer) {
                o.answer.save().then(
                    function (res) {
                        window.location.href = '/Review/' + o.question.Answer.SurveyId;
                    }, hh.throwError);
            }
        };

        var init = function () {
            hh.merge(o, options);
            me.load(o.$sid.val(), o.$qid.val());
            o.$next.click(next);
            o.$prev.click(prev);
            o.$review.click(review);
        };

        $(document).ready(init);
    };

    hh.Error = function ($error, $success) {
        var me = this;
        me.$error = $error;
        me.$success = $success;

        var init = function () {
            $(document).on('error', function (evt, data) {
                var $spn = me.$error.find('span.message');
                $spn.html(data);
                me.$error.show();
            });

            me.$error.find('button.close').on('click', function () {
                me.$error.hide();
            });

            $(document).on('success', function (evt, data) {
                var $spn = me.$success.find('span.message');
                $spn.html(data);
                me.$success.show();
            });

            me.$success.find('button.close').on('click', function () {
                me.$success.hide();
            });

            $(document).on('hideerror', function () {
                me.$error.hide();
            });

            $(document).on('hidesuccess', function () {
                me.$success.hide();
            });
        };

        $(document).ready(init);
    };

    hh.Busy = function ($div) {
        var me = this;
        me.$div = $div;

        var init = function () {
            $(document).ajaxStart(function () {
                me.$div.show();
            });

            $(document).ajaxStop(function () {
                me.$div.hide();
            });
        };

        $(document).ready(init);
    };

    hh.UserSelector = function ($txt, $lbl, $btn, $hid, $typ) {
        var me = this;
        me.$txt = $txt;
        me.$lbl = $lbl;
        me.$btn = $btn;
        me.$hid = $hid;
        me.$typ = $typ;

        me.selected = null;
        me.cleared = null;

        var api = new hh.WebApiController('/api/User/');

        var getNames = function (request, response) {
            api.getAction('Search', ['Term', request.term, 'Type', me.$typ.val()]).then(
                function (res) {
                    response(res);
                },
                function (rej) {
                    hh.throwError(rej);
                    response(null);
                });
        }

        var renderItem = function (ul, item) {
            return $('<li/>').attr('data-value', item.UserId)
                .append(item.Name + ' (' + item.EmployeeId + ') ')
                .appendTo(ul);
        };

        var select = function (evt, ui) {
            setUser(ui.item);
        };

        var clear = function (evt, data) {
            evt.stopPropagation();
            me.$lbl.text('').hide();
            me.$btn.hide();
            me.$hid.val(null);
            me.$txt.show();

            if (typeof me.cleared === 'function') {
                me.cleared(me);
            }
        };

        var init = function () {
            var ac = me.$txt.autocomplete({
                source: getNames,
                select: select
            }).data('ui-autocomplete')._renderItem = renderItem;

            me.$btn.on('click', clear);
        };

        var setUser = function (user) {
            me.$lbl.text(user.Name + ' (' + user.EmployeeId + ') ')
                .show();
            me.$btn.show();
            me.$hid.val(user.UserId);
            me.$txt.hide();
            if (typeof me.selected === 'function') {
                me.selected(me, user);
            }
        };

        me.val = function (v) {
            if (v) {
                me.$hid.val(v);
                me.api.getAction('User', [v]).then(setUser, hh.thowError);
            }
            return me.$hid.val();
        };

        $(document).ready(init);
    };

    hh.UserDropDown = function ($sel, $typ, tmp) {
        var me = this;
        me.$sel = $sel;
        me.$typ = $typ;
        me.tmp = tmp;

        me.selected = null;

        me.val = function () {
            return me.$sel.val();
        };

        var api = new hh.WebApiController('/api/User/');

        var change = function (evt, data) {
            if (typeof me.selected === 'function') {
                me.selected(me, me.$sel.val());
            }
        };

        var init = function () {
            api.getAction('Users', ['Type', me.$typ.val()]).then(function (res) {
                me.$sel.html($('<option/>').val(-1).text('Select...'));
                me.$sel.append($.templates(tmp).render(res));
            }, hh.throwError);

            me.$sel.change(change);
        };

        $(document).ready(init);
    };

    hh.UserSurveys = function (user, survey, $ul, $div) {
        var me = this;
        me.user = user;
        me.survey = survey;
        me.$ul = $ul;
        me.$div = $div;

        var api = new hh.WebApiController('/api/SurveyAdmin/');

        var toggleLock = function (evt, data) {
            var $btn = $(this);
            if ($btn.attr('data-locked') === 'true') {
                api.putAction('Unlock', $btn.attr('data-survey-id'), ['User', me.user.val()]).then(
                    function (res) {
                        $btn.attr('data-locked', 'false')
                            .val('Lock');
                    }, hh.throwError);
            } else {
                api.putAction('Lock', $btn.attr('data-survey-id'), ['User', me.user.val()]).then(
                    function (res) {
                        $btn.attr('data-locked', 'true')
                            .val('Unlock');
                    }, hh.throwError);
            }
        };

        var selectSurvey = function (evt, data) {
            var $btn = $(this);
            me.$ul.find('input:button.select').removeClass('btn-info');
            $btn.addClass('btn-info');

            me.survey.load($btn.attr('data-survey-id'), 0, me.user.val());
            me.survey.api = api;
            me.survey.show();
        };

        var userSelected = function (sel, userId) {
            api.getAction('Surveys', [userId]).then(
                function (res) {
                    var tmp = $.templates('#LockSurveyView');
                    me.$ul.html(tmp.render(res));
                    me.$ul.find('input:button.lock').click(toggleLock);
                    me.$ul.find('input:button.select').click(selectSurvey);
                }, hh.throwError);
        };

        var userCleared = function (sel) {
            me.$ul.html('');
            me.survey.hide();
        };

        var init = function () {
            me.user.selected = userSelected;
            me.user.cleared = userCleared;
            me.survey.hide();
            me.survey.api = api;
        };

        $(document).ready(init);
    };

    hh.Matching = function (options) {
        var me = this;
        var o = {
            mentor: null,
            mentee: null,
            $match: null,
            $mentors: null,
            $mentees: null,
            tmp: '#usersTemplate'
        };

        var api = new hh.WebApiController('/api/MatchingAdmin/');

        var match = function (evt, data) {
            var mentorId = o.mentor.val();
            var menteeId = o.mentee.val();
            if (mentorId && menteeId) {
                addMatch(mentorId, menteeId).then(
                    function (res) {
                        loadMentors();
                        loadMentees();
                    }, hh.throwError);
            }
        };

        var addMatch = function (mentorId, menteeId) {
            return api.postAction('Match', { MentorId: mentorId, MenteeId: menteeId });
        };

        var mentorSelected = function (sel, user) {
            loadMentees();
        };

        var loadMentees = function () {
            var id = o.mentor.val();
            if (id) {
                getMentees(id).then(
                    function (res) {
                        var tmp = $.templates(o.tmp);
                        o.$mentees.html(tmp.render(res));
                        o.$mentees.find('a.remove').click(removeAssignment);
                    }, hh.throwError);
            }
        };

        var menteeSelected = function (sel, user) {
            loadMentors();
        };

        var loadMentors = function () {
            var id = o.mentee.val();
            if (id) {
                getMentors(id).then(
                    function (res) {
                        var tmp = $.templates(o.tmp);
                        o.$mentors.html(tmp.render(res));
                        o.$mentors.find('a.remove').click(removeAssignment);
                    }, hh.throwError);
            }
        };

        var removeAssignment = function (evt, data) {
            var $a = $(this);
            var id = $a.attr('data-id');
            api.deleteAction('Assignment', [id]).then(
                function (res) {
                    loadMentors();
                    loadMentees();
                }, hh.throwError);
        };

        var mentorCleared = function (sel) {
            o.$mentees.html(null);
        };

        var menteeCleared = function (sel) {
            o.$mentors.html(null);
        };

        var getMentees = function (id) {
            return api.getAction('Mentees', [id]);
        };

        var getMentors = function (id) {
            return api.getAction('Mentors', [id]);
        };

        var init = function () {
            hh.merge(o, options);
            o.$match.click(match);
            o.mentor.selected = mentorSelected;
            o.mentee.selected = menteeSelected;
            o.mentor.cleared = mentorCleared;
            o.mentee.cleared = menteeCleared;
        };

        $(document).ready(init);
    };

    hh.MatchingSelector = function ($mentorList, $menteeList, templateId) {
        var me = this;
        me.$mentorList = $mentorList;
        me.$menteeList = $menteeList;
        me.templateId = templateId;
        var api = new hh.WebApiController('/api/MatchingAdmin/');

        var init = function () {
        };

        $(document).ready(init);
    };

    hh.WebApiController = function (baseUrl) {
        var me = this;
        me.baseUrl = baseUrl;

        me.get = function (id) {
            var url = me.baseUrl + (id ? id : '');
            return new Promise(function (resolve, reject) {
                $.ajax({
                    method: 'GET',
                    accept: 'application/json',
                    url: url,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };

        me.getAction = function (action, params) {
            var url = me.baseUrl + action;
            var i = 0;
            if (params) {
                for (i = 0; i < params.length; i++) {
                    url = url + '/' + params[i];
                }
            }
            return new Promise(function (resolve, reject) {
                $.ajax({
                    method: 'GET',
                    accept: 'application/json',
                    url: url,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };

        me.put = function (id, obj) {
            return new Promise(function (resolve, reject) {
                var url = me.baseUrl + id;
                $.ajax({
                    method: 'PUT',
                    dataType: 'json',
                    accept: 'application/json',
                    data: obj,
                    url: url,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };

        me.putAction = function (action, id, params, obj) {
            var url = me.baseUrl + action + '/' + id;
            if (params) {
                for (i = 0; i < params.length; i++) {
                    url = url + '/' + params[i];
                }
            }
            return new Promise(function (resolve, reject) {
                $.ajax({
                    method: 'PUT',
                    dataType: 'json',
                    accept: 'application/json',
                    data: obj,
                    url: url,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };

        me.post = function (obj) {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    method: 'POST',
                    dataType: 'json',
                    accept: 'application/json',
                    data: obj,
                    url: me.baseUrl,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };

        me.postAction = function (action, obj) {
            var url = me.baseUrl + action;
            return new Promise(function (resolve, reject) {
                $.ajax({
                    method: 'POST',
                    dataType: 'json',
                    accept: 'application/json',
                    data: obj,
                    url: url,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };

        me.delete = function (id) {
            return new Promise(function (resolve, reject) {
                var url = me.baseUrl + id;
                $.ajax({
                    method: 'DELETE',
                    accept: 'application/json',
                    url: url,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };

        me.deleteAction = function (action, id) {
            return new Promise(function (resolve, reject) {
                var url = me.baseUrl + action + '/' + id;
                $.ajax({
                    method: 'DELETE',
                    accept: 'application/json',
                    url: url,
                    success: function (data, textStatus, jqXHR) {
                        resolve(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reject(new Error(textStatus + ': ' + errorThrown));
                    }
                });
            });
        };
    };


})(window.hh = window.hh || {});

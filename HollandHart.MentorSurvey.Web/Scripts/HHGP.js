﻿(function (hh) {
    'use  strict';

    // general purpose stuff
    hh.merge = function (o1, o2) {
        $.extend(o1, o2);
    };

    hh.throwError = function (err) {
        $(document).trigger('error', err);
    };

    hh.throwHideError = function () {
        $(document).trigger('hideerror');
    };

    hh.throwSuccess = function (msg) {
        $(document).trigger('success', msg);
    };

    hh.parseOptions = function (opts) {
        var o = {};
        $.each(opts.split(','), function (idx, val) {
            var v;
            if (val.indexOf('_') > -1) {
                v = val.split('_');
                if (v.length == 2) {
                    o[v[0]] = v[1];
                } else if (v.length > 2) {
                    o[v[0]] = v.splice(1);
                }
            } else {
                o[val] = true;
            }
        });
        return o;
    };
})(window.hh = window.hh || {});

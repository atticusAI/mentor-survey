﻿(function (hh) {
    'use  strict';

    // validators
    hh.checked = function ($ul, args) {
        var r = true;
        if ($ul && $ul.length && $ul[0].nodeName === 'UL') {
            r = $ul.find('input:checked').length > 0;
        }
        return r;
    };

    hh.required = function ($inp, args) {
        var r = false;
        var v = $inp.val();
        if (v && v.length > 0 && v.trim().length > 0) {
            r = true;
        }
        return r;
    };

    hh.isInteger = function (v) {
        return /^(\+|-)?\d+$/.test(v);
    };

    hh.integer = function ($inp, args) {
        return hh.isInteger($inp.val());
    };

    hh.atLeast = function ($inp, args) {
        var v = $inp.val();
        return v >= args[0];
    };

    hh.atMost = function ($inp, args) {
        var v = $inp.val();
        return v <= args[0];
    };

    hh.minLength = function ($inp, args) {
        var r = false;
        var v = $inp.val();
        if (v && v.length >= args[0]) {
            r = true;
        }
        return r;
    };

    hh.maxLength = function ($inp, args) {
        var r = false;
        var v = $inp.val();
        if (!v || v && v.length <= args[0]) {
            r = true;
        }
        return r;
    };

    hh.validators = {
        'required': hh.required,
        'minlength': hh.minLength,
        'maxlength': hh.maxLength,
        'integer': hh.integer,
        'atleast': hh.atLeast,
        'atmost': hh.atMost,
        'checked': hh.checked,
    };

    hh.validate = function ($inp, $err) {
        var r = true;
        if ($inp.length && $err.length) {
            $inp.removeClass('error');
            $.each($inp.attr('class').split(' '), function (idx, val) {
                var args = val.split('_');
                if (hh.validators[args[0]]) {
                    if (!hh.validators[args[0]]($inp, args.slice(1))) {
                        $err.text($inp.attr('title'))
                            .show();
                        $inp.addClass('error');
                        r = false;
                    }
                }
            });
            if (r) {
                $err.hide()
                hh.throwHideError();
            };
        }
        return r;
    };

})(window.hh = window.hh || {});

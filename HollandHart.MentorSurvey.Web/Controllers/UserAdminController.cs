﻿using System;
using System.Data;
using System.Linq;
using System.Web.Http;
using HollandHart.MentorSurvey.Model;
using HollandHart.MentorSurvey.Web.HRDataService;
using HollandHart.MentorSurvey.Web.ViewModels;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class UserAdminController : HHApiController
    {
        [HttpGet]
        private IQueryable<BasicUser> GetGroup(string grp)
        {
            var qry = from u in db.Users
                      where (grp == "admin" && u.Admin != null) ||
                      (grp == "mentor" && u.Mentor != null) ||
                      (grp == "mentee" && u.Mentee != null) ||
                      (grp == "all")
                      orderby u.Name
                      select new BasicUser
                      {
                          ADLogin = u.ADLogin,
                          EmailAddress = u.EmailAddress,
                          EmployeeId = u.EmployeeId,
                          Name = u.Name,
                          PracticeGroup = u.PracticeGroup,
                          UserId = u.UserId
                      };
            return qry;
        }

        [HttpGet]
        [ActionName("UserGroup")]
        public IHttpActionResult GetUserGroup(string category)
        {
            return Ok(GetGroup(category));
        }

        [HttpGet]
        [ActionName("UserGroup")]
        public IHttpActionResult GetUserGroup(string category, int page, int length)
        {
            var pg = GetGroup(category).Skip(page * length).Take(length);
            return Ok(pg);
        }

        [HttpPost]
        [ActionName("Import")]
        public IHttpActionResult ImportUsers(ImportGroup import)
        {
            GuardAgainstNonAdmin("ImportUsers can only be called by an admin.");

            foreach (string u in import.Users)
            {
                EmployeeLookupContractClient cli = new EmployeeLookupContractClient();
                try
                {
                    if (!string.IsNullOrWhiteSpace(u))
                    {
                        var emp = cli.GetEmployeeFromEmailAlias(u);
                        if (emp != null)
                        {
                            switch (import.Group)
                            {
                                case "admin":
                                    EnsureAdmin(emp);
                                    break;
                                case "mentee":
                                    EnsureMentee(emp);
                                    break;
                                case "mentor":
                                    EnsureMentor(emp);
                                    break;
                            }
                            cli.Close();
                        }
                    }
                }
                catch
                {
                    cli.Abort();
                    throw;
                }
            }

            return Ok(true);
        }

        [HttpGet]
        [ActionName("Admin")]
        public IHttpActionResult GetAdminCount()
        {
            var qry = from a in db.Admins
                      select a;
            return Ok(qry.Count());
        }

        [HttpGet]
        [ActionName("Mentor")]
        public IHttpActionResult GetMentorCount()
        {
            var qry = from m in db.Mentors
                      select m;
            return Ok(qry.Count());
        }

        [HttpGet]
        [ActionName("Mentee")]
        public IHttpActionResult GetMenteeCount()
        {
            var qry = from e in db.Mentees
                      select e;
            return Ok(qry.Count());
        }

        [HttpDelete]
        [ActionName("Mentor")]
        public IHttpActionResult DeleteMentor(int id)
        {
            GuardAgainstNonAdmin("DeleteMentor can only be called by an admin.");

            var qry = from m in db.Mentors
                      where m.MentorId == id
                      select m;
            Mentor mnt = qry.FirstOrDefault();
            if (mnt != null)
            {
                db.Mentors.Remove(mnt);
                db.SaveChanges();
            }
            return Ok(true);
        }

        [HttpDelete]
        [ActionName("Mentee")]
        public IHttpActionResult DeleteMentee(int id)
        {
            GuardAgainstNonAdmin("DeleteMentee can only be called by an admin.");

            var qry = from m in db.Mentees
                      where m.MenteeId == id
                      select m;
            Mentee mte = qry.FirstOrDefault();
            if (mte != null)
            {
                db.Mentees.Remove(mte);
                db.SaveChanges();
            }

            return Ok(true);
        }

        [HttpDelete]
        [ActionName("Admin")]
        public IHttpActionResult DeleteAdmin(int id)
        {
            GuardAgainstNonAdmin("DeleteAdmin can only be called by an admin.");
            var qry = from m in db.Admins
                      where m.AdminId == id
                      select m;
            Model.Admin adm = qry.FirstOrDefault();
            if (adm != null)
            {
                db.Admins.Remove(adm);
                db.SaveChanges();
            }
            return Ok(true);
        }

        private void EnsureMentor(Employee emp)
        {
            User usr = EnsureUser(emp);
            if (usr != null && usr.Mentor == null)
            {
                var mto = new Model.Mentor()
                {
                    MentorId = usr.UserId
                };

                db.Mentors.Add(mto);
                db.SaveChanges();
                MentorSignup(usr.UserId);
            }
        }

        private void EnsureMentee(Employee emp)
        {
            User usr = EnsureUser(emp);
            if (usr != null && usr.Mentee == null)
            {
                var mnt = new Model.Mentee()
                {
                    MenteeId = usr.UserId
                };

                db.Mentees.Add(mnt);
                db.SaveChanges();
                MenteeSignup(usr.UserId);
            }
        }

        private void EnsureAdmin(Employee emp)
        {
            User usr = EnsureUser(emp);
            if (usr != null && usr.Admin == null)
            {
                var adm = new Model.Admin()
                {
                    AdminId = usr.UserId
                };

                db.Admins.Add(adm);
                db.SaveChanges();
            }
        }

        private User EnsureUser(Employee emp)
        {
            string adl = emp.ADLogin;
            var qry = from u in db.Users
                      where u.ADLogin == adl
                      select u;
            User usr = qry.FirstOrDefault();
            if (usr == null)
            {
                usr = new Model.User()
                {
                    ADLogin = emp.ADLogin,
                    EmailAddress = emp.CompanyEmailAddress,
                    EmployeeId = emp.PayrollID,
                    Name = emp.FullName,
                    PracticeGroup = emp.SectionDescription, 
                    Office = emp.LocationDescription
                };
                db.Users.Add(usr);
                db.SaveChanges();
            }

            return usr;
        }

        [HttpPost]
        [ActionName("UpdateAllOffices")]
        public IHttpActionResult UpdateAllOffices()
        {
            GuardAgainstNonAdmin("UpdateAllOffices can only be called by an admin.");

            var qry = from u in db.Users
                      select u;

            foreach (var u in qry)
            {
                EmployeeLookupContractClient cli = new EmployeeLookupContractClient();
                try
                {
                    var emp = cli.GetEmployeeFromEmailAlias(u.EmailAddress);
                    if (emp != null)
                    {
                        u.Office = emp.LocationDescription;
                    }
                    cli.Close();
                }
                catch(Exception x)
                {
                    cli.Abort();
                }
            }

            db.SaveChanges();
            return Ok();
        }
    }
}

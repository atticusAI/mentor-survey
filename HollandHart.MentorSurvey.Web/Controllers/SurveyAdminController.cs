﻿using HollandHart.MentorSurvey.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class SurveyAdminController : HHApiController
    {

        private IEnumerable<Model.Question> GetQuestion(int surveyId, int index)
        {
            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      where sq.SurveyId == surveyId
                      orderby q.DisplayOrder
                      select q;

            return qry.Skip(index).Take(1);
        }

        private bool AnswerExists(int id)
        {
            return db.Answers.Count(e => e.AnswerId == id) > 0;
        }

        [HttpGet]
        [ActionName("Question")]
        public IHttpActionResult GetSurveyQuestion(int id, int index, int userId)
        {
            GuardAgainstNonAdmin("GetSurveyQuestion can only be called by admin");

            var qst = GetQuestion(id, index).FirstOrDefault();

            var ans = from a in db.Answers
                      join q in GetQuestion(id, index) on a.QuestionId equals q.QuestionId
                      join sq in db.SurveyQuestions on a.SurveyId equals sq.SurveyId
                      where a.SurveyId == id
                      && a.UserId == userId
                      select a;
            if (ans.Count() <= 0)
            {
                db.Answers.Add(new Model.Answer
                {
                    QuestionId = qst.QuestionId,
                    SurveyId = id,
                    Text = "",
                    Value = "",
                    UserId = userId

                });
                db.SaveChanges();
            }

            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      join qt in db.QuestionTypes on q.QuestionTypeId equals qt.QuestionTypeId
                      join a in db.Answers on q.QuestionId equals a.QuestionId
                      join r in db.RelatedQuestions on q.QuestionId equals r.QuestionOneId into rs
                      from r in rs.DefaultIfEmpty()
                      join rq in db.Questions on r.QuestionTwoId equals rq.QuestionId into rqs
                      from rq in rqs.DefaultIfEmpty()
                      where q.QuestionId == qst.QuestionId &&
                          sq.SurveyId == id &&
                          a.SurveyId == id &&
                          a.UserId == userId
                      orderby q.DisplayOrder
                      select new
                      {
                          q.QuestionId,
                          q.Text,
                          q.Title,
                          q.DisplayOrder,
                          q.Description,
                          q.QuestionTypeId,
                          QuestionTypeName = qt.Name,
                          Rules = q.QuestionRules.Select(r => r.Rule.Definition),
                          Choices = q.Choices.OrderBy(c => c.ChoiceId),
                          RelatedChoices = rq.Choices,
                          RelatedAnswers = rq.Answers.Where(a => a.UserId == userId),
                          RelatedRules = rq.QuestionRules.Select(r => r.Rule.Definition),
                          Answer = a
                      };

            var question = qry.FirstOrDefault();
            if (question == null)
            {
                return NotFound();
            }

            return Ok(question);
        }

        [HttpGet]
        [ActionName("Questions")]
        public IQueryable<Question> GetSurveyQuestions(int id)
        {
            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      where sq.SurveyId == id
                      select q;

            return qry;
        }

        [HttpGet]
        [ActionName("QuestionIndex")]
        public IHttpActionResult GetIndex(int id, int qid)
        {
            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      where sq.SurveyId == id
                      orderby q.DisplayOrder
                      select new QIDRow() { Index = 0, QuestionId = q.QuestionId };
            var qs = qry.ToArray().Select((q, i) => { q.Index = i; return q; });
            var qi = qs.Where(q => q.QuestionId == qid).FirstOrDefault();
            return Ok(qi == null ? -1 : qi.Index);
        }

        internal class QIDRow
        {
            public int Index { get; set; }
            public int QuestionId { get; set; }
        }

        [HttpGet]
        [ActionName("Count")]
        public int GetCount(int id)
        {
            return GetSurveyQuestions(id).Count();
        }

        [HttpGet]
        [ActionName("Answer")]
        public IHttpActionResult GetAnswer(int id, int index, int userId)
        {
            GuardAgainstNonAdmin("GetAnswer can only be called by admin");

            var qry = from sq in db.SurveyQuestions
                      join a in db.Answers on sq.QuestionId equals a.QuestionId
                      join u in db.Users on a.UserId equals u.UserId
                      where sq.SurveyId == id &&
                      sq.QuestionId == index &&
                      u.UserId == userId
                      select new
                      {
                          a.AnswerId,
                          a.Value,
                          a.Text,
                          a.SurveyId,
                          a.UserId,
                          a.QuestionId
                      };

            var answer = qry.FirstOrDefault();

            if (answer == null)
            {
                return NotFound();
            }

            return Ok(answer);
        }

        [HttpGet]
        [ActionName("Answers")]
        public IHttpActionResult GetAnswers(int id, int userId)
        {
            GuardAgainstNonAdmin("GetAnswers can only be called by admin");

            var qry = from sq in db.SurveyQuestions
                      join a in db.Answers on sq.QuestionId equals a.QuestionId
                      join u in db.Users on a.UserId equals u.UserId
                      join q in db.Questions on sq.QuestionId equals q.QuestionId
                      join qt in db.QuestionTypes on q.QuestionTypeId equals qt.QuestionTypeId
                      join sa in db.SurveyAudiences on new { sq.SurveyId, u.UserId } equals new { sa.SurveyId, sa.UserId }
                      join r in db.RelatedQuestions on q.QuestionId equals r.QuestionOneId into rs
                      from r in rs.DefaultIfEmpty()
                      join rq in db.Questions on r.QuestionTwoId equals rq.QuestionId into rqs
                      from rq in rqs.DefaultIfEmpty()
                      where sq.SurveyId == id &&
                      u.UserId == userId
                      orderby q.DisplayOrder
                      select new
                      {
                          a.AnswerId,
                          AnswerValue = a.Value,
                          AnswerText = a.Text,
                          a.SurveyId,
                          a.UserId,
                          a.QuestionId,
                          qt.QuestionTypeId,
                          QuestionTypeName = qt.Name,
                          QuestionText = q.Text,
                          QuestionTitle = q.Title,
                          Rules = q.QuestionRules.Select(r => r.Rule.Definition),
                          q.Choices,
                          sa.Complete,
                          RelatedChoices = rq.Choices,
                          RelatedAnswers = rq.Answers.Where(a => a.UserId == u.UserId),
                          RelatedRules = rq.QuestionRules.Select(r => r.Rule.Definition),
                          RelatedQuestionText = rq.Text,
                          RelatedQuestionId = (rq == null) ? null : (int?)rq.QuestionId
                      };

            return Ok(qry.ToArray());
        }

        [HttpPut]
        [ActionName("Answer")]
        public IHttpActionResult PutAnswer(int id, [FromBody] Model.Answer answer)
        {
            GuardAgainstNonAdmin("PutAnswer can only be called by admin");

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != answer.AnswerId)
            {
                return BadRequest();
            }

            db.Entry(answer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnswerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPut]
        [ActionName("Lock")]
        public IHttpActionResult Lock(int id, int userId)
        {
            GuardAgainstNonAdmin("Lock can only be called by admin");

            var qry = from s in db.Surveys
                      join sa in db.SurveyAudiences on s.SurveyId equals sa.SurveyId
                      where s.SurveyId == id && sa.UserId == userId
                      select sa;

            var srv = qry.FirstOrDefault();
            if (srv != null)
            {
                srv.Complete = true;
                db.SaveChanges();
            }

            return Ok(true);
        }


        [HttpPut]
        [ActionName("Unlock")]
        public IHttpActionResult Unlock(int id, int userId)
        {
            GuardAgainstNonAdmin("Unlock can only be called by admin");

            var qry = from s in db.Surveys
                      join sa in db.SurveyAudiences on s.SurveyId equals sa.SurveyId
                      where s.SurveyId == id && sa.UserId == userId
                      select sa;

            var srv = qry.FirstOrDefault();
            if (srv != null)
            {
                srv.Complete = false;
                db.SaveChanges();
            }

            return Ok(true);
        }

        public class UserId
        {
            public int userId { get; set; }
        }



        [HttpGet]
        [ActionName("Surveys")]
        public IHttpActionResult GetSurveys(int id)
        {
            GuardAgainstNonAdmin("GetSurveys can only be called by admin");

            var qry = from s in db.Surveys
                      join sa in db.SurveyAudiences on s.SurveyId equals sa.SurveyId
                      where sa.UserId == id
                      orderby s.Name
                      select new
                      {
                          s.SurveyId,
                          s.Name,
                          sa.Complete,
                          s.Closed,
                          s.Description,
                          s.Title
                      };

            return Ok(qry);
        }

        [HttpPut]
        [ActionName("Complete")]
        public IHttpActionResult PutComplete(int id, int userId)
        {
            GuardAgainstNonAdmin("Complete can only be called by admin");

            var qry = from sa in db.SurveyAudiences
                      where sa.SurveyId == id
                      && sa.UserId == userId
                      select sa;

            var srvA = qry.FirstOrDefault();
            if (srvA != null)
            {
                srvA.Complete = true;
                db.SaveChanges();
            }

            return Ok(true);
        }

        [HttpGet]
        [ActionName("Title")]
        public IHttpActionResult GetTitle(int id)
        {
            var qry = from s in db.Surveys
                      where s.SurveyId == id
                      select new
                      {
                          s.Title,
                          s.Description
                      };

            return Ok(qry.FirstOrDefault());
        }

        [HttpGet]
        [ActionName("ExportMentors")]
        public IHttpActionResult ExportMentors()
        {
            GuardAgainstNonAdmin("ExportMentors can only be called by admin");

            return Ok();
        }
    }
}

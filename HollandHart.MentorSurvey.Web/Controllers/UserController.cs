﻿using System;
using System.Data;
using System.Linq;
using System.Web.Http;
using HollandHart.MentorSurvey.Model;
using HollandHart.MentorSurvey.Web.HRDataService;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class UserController : HHApiController
    {
        [HttpGet]
        [ActionName("Surveys")]
        public IHttpActionResult GetSurveys(bool id = false)
        {
            // bool b = Convert.ToBoolean(id);

            var qry = from u in db.Users
                      join sa in db.SurveyAudiences on u.UserId equals sa.UserId
                      join s in db.Surveys on sa.SurveyId equals s.SurveyId
                      where u.ADLogin == ADLoginName &&
                      (id ? (sa.Complete == id || s.Closed == id)
                      : (sa.Complete == false && s.Closed == false))
                      select s;

            return Ok(qry);
        }

        [HttpPost]
        [ActionName("VolunteerMentor")]
        public IHttpActionResult VolunteerMentor()
        {
            var qry = from u in db.Users
                      where u.ADLogin == ADLoginName
                      select u;
            var user = qry.FirstOrDefault();
            if (user.Mentor == null)
            {
                db.Mentors.Add(new Mentor
                {
                    MentorId = user.UserId
                });
                db.SaveChanges();
            }

            int surveyId = MentorSignup(user.UserId);
            SendAdminEmail("Mentor Signup", user.Name + " signed up as a mentor.");
            return Ok(surveyId);
        }

        [HttpGet]
        [ActionName("IsMentor")]
        public IHttpActionResult IsMentor(int id = 0)
        {
            var qry = from m in db.Mentors
                      join u in db.Users on m.MentorId equals u.UserId
                      where u.ADLogin == ADLoginName
                      select m;
            return Ok(qry.Count() > 0);
        }

        [HttpPost]
        [ActionName("VolunteerMentee")]
        public IHttpActionResult VolunteerMentee()
        {
            var qry = from u in db.Users
                      where u.ADLogin == ADLoginName
                      select u;
            var user = qry.FirstOrDefault();
            if (user.Mentee == null)
            {
                db.Mentees.Add(new Mentee
                {
                    MenteeId = user.UserId
                });
                db.SaveChanges();
            }

            int surveyId = MenteeSignup(user.UserId);
            SendAdminEmail("Mentee Signup", user.Name + " signed up as a mentee.");
            return Ok(surveyId);
        }

        [HttpGet]
        [ActionName("IsMentee")]
        public IHttpActionResult IsMentee(int id = 0)
        {
            var qry = from m in db.Mentees
                      join u in db.Users on m.MenteeId equals u.UserId
                      where u.ADLogin == ADLoginName
                      select m;
            return Ok(qry.Count() > 0);
        }

        [HttpGet]
        [ActionName("Users")]
        public IHttpActionResult GetUsers(string type = "All")
        {
            var qry = from u in db.Users
                      where (type == "Mentor" && u.Mentor != null)
                      || (type == "Mentee" && u.Mentee != null)
                      || (type == "Admin" && u.Admin != null)
                      || (type == "All")
                      orderby u.Name
                      select new
                      {
                          u.ADLogin,
                          u.EmailAddress,
                          u.EmployeeId,
                          u.Name,
                          u.PracticeGroup,
                          u.UserId
                      };
            return Ok(qry);
        }

        [HttpGet]
        [ActionName("Search")]
        public IHttpActionResult SearchUsers(string term, string type = "All")
        {
            var qry = from u in db.Users
                      where (u.ADLogin.Contains(term)
                      || u.EmailAddress.Contains(term)
                      || u.EmployeeId.Contains(term)
                      || u.Name.Contains(term))
                      && (
                      (type == "Mentor" && u.Mentor != null)
                      || (type == "Mentee" && u.Mentee != null)
                      || (type == "Admin" && u.Admin != null)
                      || (type == "All")
                      )
                      orderby u.Name
                      select new
                      {
                          u.ADLogin,
                          u.EmailAddress,
                          u.EmployeeId,
                          u.Name,
                          u.PracticeGroup,
                          u.UserId
                      };

            return Ok(qry);
        }

        [HttpGet]
        [ActionName("User")]
        public new IHttpActionResult GetUser(int id)
        {
            return Ok(base.GetUser(id));
        }


        [HttpPost]
        [ActionName("EnsureUser")]
        public IHttpActionResult EnsureUser()
        {
            var qry = from u in db.Users
                      where u.ADLogin == ADLoginName
                      select u;
            var usr = qry.FirstOrDefault();

            if (usr == null)
            {
                EmployeeLookupContractClient cli = new EmployeeLookupContractClient();
                try
                {
                    var emp = cli.GetEmployeeFromLogin(ADLoginName);
                    if (emp != null)
                    {
                        db.Users.Add(new Model.User
                        {
                            ADLogin = emp.ADLogin,
                            EmailAddress = emp.CompanyEmailAddress,
                            EmployeeId = emp.PayrollID,
                            Name = emp.FullName,
                            PracticeGroup = emp.SectionDescription, 
                            Office = emp.LocationDescription
                        });
                        db.SaveChanges();
                    }
                    cli.Close();
                }
                catch (Exception x)
                {
                    cli.Abort();
                    return InternalServerError(x);
                }
            }

            return Ok(true);
        }
    }
}

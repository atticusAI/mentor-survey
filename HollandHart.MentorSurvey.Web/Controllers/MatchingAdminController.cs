﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class MatchingAdminController : HHApiController
    {
        [HttpGet]
        [ActionName("Mentors")]
        public IHttpActionResult  GetMentors(int id)
        {
            var qry = from ma in db.MentorAssignments
                      join u in db.Users on ma.MentorId equals u.UserId
                      where ma.MenteeId == id
                      orderby u.Name
                      select new
                      {
                          u.ADLogin,
                          u.EmailAddress,
                          u.EmployeeId,
                          u.Name,
                          u.PracticeGroup,
                          u.UserId, 
                          ma.MentorAssignmentId, 
                          ma.MentorId, 
                          ma.MenteeId
                      };

            return Ok(qry);
        }

        [HttpGet]
        [ActionName("Mentees")]
        public IHttpActionResult GetMentees(int id)
        {
            var qry = from ma in db.MentorAssignments
                      join u in db.Users on ma.MenteeId equals u.UserId
                      where ma.MentorId == id
                      orderby u.Name
                      select new
                      {
                          u.ADLogin,
                          u.EmailAddress,
                          u.EmployeeId,
                          u.Name,
                          u.PracticeGroup,
                          u.UserId,
                          ma.MentorAssignmentId, 
                          ma.MentorId, 
                          ma.MenteeId
                      };

            return Ok(qry);
        }

        [HttpPost]
        [ActionName("Match")]
        public IHttpActionResult AssignMatch([FromBody] Match match)
        {
            GuardAgainstNonAdmin("AssignMatch can only be called by admin");

            var qry = from ma in db.MentorAssignments
                      where ma.MentorId == match.MentorId &&
                      ma.MenteeId == match.MenteeId
                      select ma;

            if (qry.Count() <= 0)
            {
                var m = new Model.MentorAssignment
                {
                    MentorId = match.MentorId,
                    MenteeId = match.MenteeId
                };
                db.MentorAssignments.Add(m);
                db.SaveChanges();
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        public class Match
        {
            public int MentorId { get; set; }
            public int MenteeId { get; set; }
        }

        [HttpDelete]
        [ActionName("Assignment")]
        public IHttpActionResult DeleteAssignment(int id)
        {
            var qry = from ma in db.MentorAssignments
                      where ma.MentorAssignmentId == id
                      select ma;
            var mnt = qry.FirstOrDefault();
            if (mnt != null)
            {
                db.MentorAssignments.Remove(mnt);
                db.SaveChanges();
            }

            return Ok(true);
        }
    }
}

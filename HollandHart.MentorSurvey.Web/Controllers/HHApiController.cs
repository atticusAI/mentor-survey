﻿using HollandHart.MentorSurvey.Model;
using HollandHart.MentorSurvey.Web.EmailService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class HHApiController : ApiController
    {
        protected HHMentorSurveyEntities db = new HHMentorSurveyEntities();

        protected bool IAmAdmin
        {
            get
            {
                var qry = from a in db.Admins
                          join u in db.Users on a.AdminId equals u.UserId
                          where u.ADLogin == ADLoginName
                          select a;
                return (qry.Count() > 0);
            }
        }

        protected void GuardAgainstNonAdmin(string message)
        {
            if (!IAmAdmin)
            {
                throw new UnauthorizedAccessException(message);
            }
        }

        protected string ADLoginName
        {
            get
            {
                return RequestContext.Principal.Identity.Name.Split('\\')[1];
            }
        }

        protected User GetCurrentUser()
        {
            var qry = from u in db.Users
                      where u.ADLogin == ADLoginName
                      select u;

            return qry.FirstOrDefault();
        }

        protected User GetUser(int userId)
        {
            var qry = from u in db.Users
                      where u.UserId == userId
                      select u;

            return qry.FirstOrDefault();

        }

        protected AppSetting GetAppSetting(string name)
        {
            var qry = from a in db.AppSettings
                      where a.Name == name
                      select a;

            return qry.FirstOrDefault();
        }

        protected int MenteeSignup(int userId)
        {
            int surveyId = -1;

            AppSetting stg = GetAppSetting("MenteeSurvey");
            if (stg != null)
            {
                if (Int32.TryParse(stg.Value, out surveyId))
                {
                    SurveySignup(userId, surveyId);
                }
            }

            return surveyId;
        }

        protected int MentorSignup(int userId)
        {
            int surveyId = -1;

            AppSetting stg = GetAppSetting("MentorSurvey");
            if (stg != null)
            {
                if (Int32.TryParse(stg.Value, out surveyId))
                {
                    SurveySignup(userId, surveyId);
                }
            }

            return surveyId;
        }

        protected string GetSettingText(string name)
        {
            string txt = "";
            var setting = GetAppSetting(name);
            if (setting != null)
            {
                txt = setting.Text;
            }
            return txt;
        }

        protected string GetEmailSubject(string emailName)
        {
            return GetSettingText(emailName + "_EmailSubject");
        }
        protected string GetEmailBody(string emailName)
        {
            return GetSettingText(emailName + "_EmailBody");
        }


        protected string[] GetSettingArray(string name, char separator = ';')
        {
            List<string> arr = new List<string>();

            string setting = GetSettingText(name);
            if (!string.IsNullOrWhiteSpace(setting))
            {
                arr.AddRange(setting.Split(separator));
            }

            return arr.ToArray();
        }
        protected string[] AdminToAddresses
        {
            get { return GetSettingArray("AdminToAddresses"); }
        }

        protected string[] AdminBccAddresses
        {
            get { return GetSettingArray("AdminBccAddresses"); }
        }

        protected string[] AdminCcAddresses
        {
            get { return GetSettingArray("AdminCcAddresses"); }
        }

        protected string EmailSenderAddress
        {
            get { return GetSettingText("EmailSenderAddress"); }
        }

        protected string EmailSenderDisplayName
        {
            get { return GetSettingText("EmailSenderDisplayName"); }
        }

        protected void SurveySignup(int userId, int surveyId)
        {
            var qry = from sa in db.SurveyAudiences
                      where sa.SurveyId == surveyId &&
                      sa.UserId == userId
                      select sa;
            var assignment = qry.FirstOrDefault();
            if (assignment  == null)
            {
                db.SurveyAudiences.Add(new SurveyAudience
                {
                    UserId = userId,
                    SurveyId = surveyId
                });
                db.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // only notify administrators
        protected void SendAdminEmail(string subjectLine, string body)
        {
            SendEmail(EmailSenderAddress, EmailSenderDisplayName, AdminToAddresses, null, null, subjectLine, body);
        }

        protected void SendAdminEmail(string emailName)
        {
            SendEmail(EmailSenderAddress, EmailSenderDisplayName, AdminToAddresses, null, null, GetSettingText(emailName + "_EmailSubject"), GetSettingText(emailName + "_EmailBody"));
        }

        // notify the user
        // possibly bcc admins
        protected void SendUserEmail(string userEmailAddress, string emailName)
        {
            SendEmail(EmailSenderAddress, EmailSenderDisplayName, new string[] { userEmailAddress }, AdminCcAddresses, AdminBccAddresses, GetSettingText(emailName + "_EmailSubject"), GetSettingText(emailName + "_EmailBody"));
        }

        public void SendEmail(string senderAddress, string displayName, string[] recipientAddress, string[] ccAddress, string[] bccAddress, string subjectLine, string body, bool isBodyHtml = true, bool isHighPriority = false)
        {
            EmailClient clnt = new EmailClient();
            try
            {
                clnt.SendMail(senderAddress, displayName, recipientAddress, ccAddress, bccAddress, subjectLine, body, isBodyHtml, isHighPriority);
            }
            catch
            {
                clnt.Abort();
            }
        }
    }
}

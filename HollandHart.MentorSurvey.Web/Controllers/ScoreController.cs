﻿using HollandHart.MentorSurvey.Web.ViewModels;
using HollandHart.SolverApi;
using HollandHart.SolverApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class ScoreController : HHApiController
    {
        [HttpGet]
        [ActionName("Scores")]
        public IHttpActionResult GetScores()
        {
            var qry = from s in db.VW_Scores
                      select s;
            return Ok(qry.ToArray());
        }

        [HttpGet]
        [ActionName("SuggestedMatches")]
        public IHttpActionResult GetMatches()
        {
            MentorMatchingResponse mmr = null;
            try
            {
                var slots = from a in db.VW_AssignmentCounts
                            select new MentorMatchingSlot()
                            {
                                Requested = a.Requested ?? 0,
                                User = a.User
                            };

                var scores = from s in db.VW_Scores
                             select new MentorMatchingScore()
                             {
                                 Mentee = s.Mentee,
                                 Mentor = s.Mentor,
                                 Score = s.Score ?? 0.00M
                             };

                MentorMatchingRequest body = new MentorMatchingRequest(scores, slots);
                SolverRequest sr = new SolverRequest(Properties.Settings.Default.SolverBaseUrl);
                mmr = sr.PostRequest<MentorMatchingResponse>("mm-interface", body);
            }
            catch (Exception x)
            {
                return InternalServerError(x);
            }

            return Ok(mmr);
        }

        [HttpGet]
        [ActionName("Assignments")]
        public IHttpActionResult GetAssignments()
        {
            var qry = from a in db.VW_AssignmentCounts
                      select a;
            return Ok(qry.ToArray());
        }

    }
}
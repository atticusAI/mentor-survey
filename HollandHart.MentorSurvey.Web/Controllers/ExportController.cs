﻿using ExcelWriter;
using HollandHart.MentorSurvey.Web.ViewModels;
using HollandHart.SolverApi;
using HollandHart.SolverApi.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class ExportController : HHApiController
    {
        private Dictionary<string, string> fileNames = new Dictionary<string, string>()
        {
            {"mentor", "Mentors.xlsx"},
            {"mentee", "Mentees.xlsx"},
            {"admin", "Admins.xlsx"},
            {"mentorprofile", "MentorProfiles.xlsx" },
            {"menteeprofile", "MenteeProfiles.xlsx" }
        };

        [HttpGet]
        [ActionName("SuggestedMatches")]
        public IHttpActionResult ExportSuggestedMatches(string type = "all")
        {
            GuardAgainstNonAdmin("ExportSuggestedMatches can only be called by admin.");

            IExport<ExportSuggestedMatches2018> exp = new ExcelWriter<ExportSuggestedMatches2018>();
            var mmr = GetSuggestedMatches(type);
            List<ExportSuggestedMatches2018> sugs = new List<ExportSuggestedMatches2018>();
            byte[] excel;

            try
            {
                foreach (var assign in mmr.Assignments)
                {
                    foreach (var mentor in assign.Keys)
                    {
                        var mProfs = GetExportProfiles("mentor", Convert.ToInt32(mentor.Replace("M", "")));
                        var mProf = GetMentorProfile2018(GetQAGroup(mProfs.ToList())).FirstOrDefault();
                        foreach (var mentee in assign[mentor])
                        {
                            var score = (from s in db.VW_Scores
                                         where s.Mentee == mentee && s.Mentor == mentor
                                         select s.PercentageScore).FirstOrDefault();

                            var eProfs = GetExportProfiles("mentee", Convert.ToInt32(mentee.Replace("E", "")));
                            var eProf = GetMenteeProfile2018(GetQAGroup(eProfs.ToList())).FirstOrDefault();
                            sugs.Add(new ExportSuggestedMatches2018 {
                                MenteeAnswer1 = eProf.Answer1 ?? "",
                                MenteeAnswer2 = eProf.Answer2 ?? "",
                                MenteeAnswer3 = eProf.Answer3 ?? "",
                                MenteeAnswer4 = eProf.Answer4 ?? "",
                                MenteeAnswer5 = eProf.Answer5 ?? "",
                                MenteeAnswer6_1 = eProf.Answer6_1 ?? "",
                                MenteeAnswer6_2 = eProf.Answer6_2 ?? "",
                                MenteeAnswer6_3 = eProf.Answer6_3 ?? "",
                                MenteeAnswer6_4 = eProf.Answer6_4 ?? "",
                                MenteeAnswer6_5 = eProf.Answer6_5 ?? "",
                                MenteeAnswer7 = eProf.Answer7 ?? "",
                                MenteeEmail = eProf.EmployeeEmail ?? "",
                                MenteeId = eProf.UserId,
                                MenteeName = eProf.EmployeeName ?? "",
                                MenteeOffice = eProf.Office ?? "",
                                MenteePracticeGroup = eProf.PracticeGroup ?? "",
                                MenteeQuestion1 = eProf.Question1 ?? "",
                                MenteeQuestion2 = eProf.Question2 ?? "",
                                MenteeQuestion3 = eProf.Question3 ?? "",
                                MenteeQuestion4 = eProf.Question4 ?? "",
                                MenteeQuestion5 = eProf.Question5 ?? "",
                                MenteeQuestion6_1 = eProf.Question6_1 ?? "",
                                MenteeQuestion6_2 = eProf.Question6_2 ?? "",
                                MenteeQuestion6_3 = eProf.Question6_3 ?? "",
                                MenteeQuestion6_4 = eProf.Question6_4 ?? "",
                                MenteeQuestion6_5 = eProf.Question6_5 ?? "",
                                MenteeQuestion7 = eProf.Question7 ?? "",
                                MentorAnswer1 = mProf.Answer1 ?? "",
                                MentorAnswer2 = mProf.Answer2 ?? "",
                                MentorAnswer3 = mProf.Answer3 ?? "",
                                MentorAnswer4 = mProf.Answer4 ?? "",
                                MentorAnswer5 = mProf.Answer5 ?? "",
                                MentorAnswer6_1 = mProf.Answer6_1 ?? "",
                                MentorAnswer6_2 = mProf.Answer6_2 ?? "",
                                MentorAnswer6_3 = mProf.Answer6_3 ?? "",
                                MentorAnswer6_4 = mProf.Answer6_4 ?? "",
                                MentorAnswer6_5 = mProf.Answer6_5 ?? "",
                                MentorAnswer7 = mProf.Answer7 ?? "",
                                MentorEmail = mProf.EmployeeEmail ?? "",
                                MentorId = mProf.UserId,
                                MentorName = mProf.EmployeeName ?? "",
                                MentorOffice = mProf.Office ?? "",
                                MentorPracticeGroup = mProf.PracticeGroup ?? "",
                                MentorQuestion1 = mProf.Question1 ?? "",
                                MentorQuestion2 = mProf.Question2 ?? "",
                                MentorQuestion3 = mProf.Question3 ?? "",
                                MentorQuestion4 = mProf.Question4 ?? "",
                                MentorQuestion5 = mProf.Question5 ?? "",
                                MentorQuestion6_1 = mProf.Question6_1 ?? "",
                                MentorQuestion6_2 = mProf.Question6_2 ?? "",
                                MentorQuestion6_3 = mProf.Question6_3 ?? "",
                                MentorQuestion6_4 = mProf.Question6_4 ?? "",
                                MentorQuestion6_5 = mProf.Question6_5 ?? "",
                                MentorQuestion7 = mProf.Question7 ?? "",
                                Score = Math.Round(score ?? 0.00M)
                            });
                        }
                    }
                }

                // get all unmatched mentors
                var umxProfs = GetExportProfiles("mentor");
                var umProfs = GetMentorProfile2018(GetQAGroup(umxProfs.ToList()));

                var unmatchedMentors = from u in umProfs
                                       join s in sugs on u.UserId equals s.MentorId into sl
                                       from s in sl.DefaultIfEmpty()
                                       where s == null
                                       select new ExportSuggestedMatches2018()
                                       {
                                           MentorAnswer1 = u.Answer1,
                                           MentorAnswer2 = u.Answer1,
                                           MentorAnswer3 = u.Answer3,
                                           MentorAnswer4 = u.Answer4,
                                           MentorAnswer5 = u.Answer5,
                                           MentorAnswer6_1 = u.Answer6_1,
                                           MentorAnswer6_2 = u.Answer6_1,
                                           MentorAnswer6_3 = u.Answer6_3,
                                           MentorAnswer6_4 = u.Answer6_4,
                                           MentorAnswer6_5 = u.Answer6_5,
                                           MentorAnswer7 = u.Answer7,
                                           MentorOffice = u.Office,
                                           MentorEmail = u.EmployeeEmail,
                                           MentorId = u.UserId,
                                           MentorName = u.EmployeeName,
                                           MentorPracticeGroup = u.PracticeGroup,
                                           MentorQuestion1 = u.Question1,
                                           MentorQuestion2 = u.Question2,
                                           MentorQuestion3 = u.Question3,
                                           MentorQuestion4 = u.Question4,
                                           MentorQuestion5 = u.Question5,
                                           MentorQuestion6_1 = u.Question6_1,
                                           MentorQuestion6_2 = u.Question6_2,
                                           MentorQuestion6_3 = u.Question6_3,
                                           MentorQuestion6_4 = u.Question6_4,
                                           MentorQuestion6_5 = u.Question6_5,
                                           MentorQuestion7 = u.Question7,
                                       };

                // get all unmatched mentees
                var uexProfs = GetExportProfiles("mentee");
                var ueProfs = GetMenteeProfile2018(GetQAGroup(uexProfs.ToList()));
                var unmatchedMentees = from u in ueProfs
                                       join s in sugs on u.UserId equals s.MenteeId into sl
                                       from s in sl.DefaultIfEmpty()
                                       where s == null
                                       select new ExportSuggestedMatches2018()
                                       {
                                           MenteeAnswer1 = u.Answer1,
                                           MenteeAnswer2 = u.Answer1,
                                           MenteeAnswer3 = u.Answer3,
                                           MenteeAnswer4 = u.Answer4,
                                           MenteeAnswer5 = u.Answer5,
                                           MenteeAnswer6_1 = u.Answer6_1,
                                           MenteeAnswer6_2 = u.Answer6_1,
                                           MenteeAnswer6_3 = u.Answer6_3,
                                           MenteeAnswer6_4 = u.Answer6_4,
                                           MenteeAnswer6_5 = u.Answer6_5,
                                           MenteeAnswer7 = u.Answer7,
                                           MenteeOffice = u.Office,
                                           MenteeEmail = u.EmployeeEmail,
                                           MenteeId = u.UserId,
                                           MenteeName = u.EmployeeName,
                                           MenteePracticeGroup = u.PracticeGroup,
                                           MenteeQuestion1 = u.Question1,
                                           MenteeQuestion2 = u.Question2,
                                           MenteeQuestion3 = u.Question3,
                                           MenteeQuestion4 = u.Question4,
                                           MenteeQuestion5 = u.Question5,
                                           MenteeQuestion6_1 = u.Question6_1,
                                           MenteeQuestion6_2 = u.Question6_2,
                                           MenteeQuestion6_3 = u.Question6_3,
                                           MenteeQuestion6_4 = u.Question6_4,
                                           MenteeQuestion6_5 = u.Question6_5,
                                           MenteeQuestion7 = u.Question7,
                                       };

                excel = exp.Export(sugs.OrderBy(s => s.MentorName).ThenBy(s => s.MenteeName)
                    .Concat(unmatchedMentors.OrderBy(m => m.MentorName))
                    .Concat(unmatchedMentees.OrderBy(e => e.MenteeName)));
            }
            catch (Exception x)
            {
                return InternalServerError(x);
            }

            return ExcelResponse(excel, "SuggestedMatches.xlsx");
        }

        private MentorMatchingResponse GetSuggestedMatches(string type = "all")
        {
            MentorMatchingResponse mmr = null;

            var slots = from a in db.VW_AssignmentCounts
                        where type.ToLower() == "all" ||
                               (type.ToLower() == "unmatched" && a.Assigned < a.Requested)
                        select new MentorMatchingSlot()
                        {
                            Requested = a.Requested ?? 0,
                            User = a.User
                        };

            var scores = from s in db.VW_Scores
                         join m in slots on "M" + s.MentorId.ToString() equals m.User
                         join e in slots on "E" + s.MenteeId.ToString() equals e.User
                         select new MentorMatchingScore()
                         {
                             Mentee = s.Mentee,
                             Mentor = s.Mentor,
                             Score = s.Score ?? 0.00M
                         };

            MentorMatchingRequest body = new MentorMatchingRequest(scores, slots);
            SolverRequest sr = new SolverRequest(Properties.Settings.Default.SolverBaseUrl);
            mmr = sr.PostRequest<MentorMatchingResponse>("mm-interface", body);
            return mmr;
        }

        [HttpGet]
        [ActionName("Users")]
        public IHttpActionResult ExportUsers(string type)
        {
            GuardAgainstNonAdmin("ExportMentors can only be called by admin.");
            IExport<ExportUser> exp = new ExcelWriter<ExportUser>();

            var qry = from u in db.Users
                      where type == "mentor" && u.Mentor != null ||
                        type == "mentee" && u.Mentee != null ||
                        type == "admin" && u.Admin != null
                      orderby u.Name
                      select new ExportUser()
                      {
                          EmailAddress = u.EmailAddress,
                          EmployeeID = u.EmployeeId,
                          Name = u.Name,
                          PracticeGroup = u.PracticeGroup
                      };

            byte[] excel = exp.Export(qry);
            return ExcelResponse(excel, fileNames[type.ToLower()]);
        }

        [HttpGet]
        [ActionName("Matches")]
        public IHttpActionResult ExportMatches()
        {
            GuardAgainstNonAdmin("ExportMatches can only be called by admin.");
            IExport<ExportMatch> exp = new ExcelWriter<ExportMatch>();

            var qry = from ma in db.MentorAssignments
                      join u in db.Users on ma.MentorId equals u.UserId
                      join um in db.Users on ma.MenteeId equals um.UserId
                      orderby u.Name
                      select new ExportMatch()
                      {
                          MentorName = u.Name,
                          MentorEmail = u.EmailAddress,
                          MenteeName = um.Name,
                          MenteeEmail = um.EmailAddress
                      };

            byte[] excel = exp.Export(qry);
            return ExcelResponse(excel, "Matches.xlsx");
        }

        [HttpGet]
        [ActionName("Profiles")]
        public IHttpActionResult ExportProfiles(string type)
        {
            GuardAgainstNonAdmin("ExportProfiles can only be called by admin.");

            IEnumerable<ExportProfile> profs = GetExportProfiles(type);
            IEnumerable<QuestionAnswerGroup> grp = GetQAGroup(profs.ToList());

            if (type.ToLower() == "mentor")
            {
                IEnumerable<ExportMentorProfile2018> xl = GetMentorProfile2018(grp);
                IExport<ExportMentorProfile2018> exp = new ExcelWriter<ExportMentorProfile2018>();
                byte[] excel = exp.Export(xl);
                return ExcelResponse(excel, fileNames["mentorprofile"]);
            }
            else
            {
                IEnumerable<ExportMenteeProfile2018> xl = GetMenteeProfile2018(grp);
                IExport<ExportMenteeProfile2018> exp = new ExcelWriter<ExportMenteeProfile2018>();
                byte[] excel = exp.Export(xl);
                return ExcelResponse(excel, fileNames["menteeprofile"]);
            }
        }

        private IEnumerable<QuestionAnswerGroup> GetQAGroup(List<ExportProfile> profiles)
        {
            var grp = from q in profiles
                      group q by new { q.UserId, q.EmployeeEmail, q.EmployeeName, q.Office, q.PracticeGroup, q.QuestionType, q.QuestionText }
                      into g
                      select new QuestionAnswerGroup
                      {
                          UserId = g.Key.UserId,
                          EmployeeEmail = g.Key.EmployeeEmail,
                          EmployeeName = g.Key.EmployeeName,
                          Office = g.Key.Office,
                          PracticeGroup = g.Key.PracticeGroup,
                          QuestionText = g.Key.QuestionText,
                          QuestionType = g.Key.QuestionType,
                          Answers = g.Select(d => d.Answer)
                      };
            return grp;
        }

        private IEnumerable<ExportProfile> GetExportProfiles(string type, int? userId = null)
        {
            var qry = from sq in db.SurveyQuestions
                      join s in db.Surveys on sq.SurveyId equals s.SurveyId
                      join ap in db.AppSettings on sq.SurveyId.ToString() equals ap.Value
                      join a in db.Answers on sq.QuestionId equals a.QuestionId
                      join u in db.Users on a.UserId equals u.UserId
                      join q in db.Questions on sq.QuestionId equals q.QuestionId
                      join qt in db.QuestionTypes on q.QuestionTypeId equals qt.QuestionTypeId
                      join sa in db.SurveyAudiences on new { sq.SurveyId, u.UserId } equals new { sa.SurveyId, sa.UserId }
                      join c in db.Choices on a.Value equals c.ChoiceId.ToString() into qc
                      from c in qc.DefaultIfEmpty()
                      join cx in db.VW_MultipleChoiceAnswers on a.AnswerId equals cx.AnswerId into qcx
                      from cx in qcx.DefaultIfEmpty()
                      orderby new { u.Name, q.DisplayOrder }
                      where (userId ?? u.UserId) == u.UserId &&
                      ((type == "mentor" && u.Mentor != null && ap.Name == "MentorSurvey") ||
                      (type == "mentee" && u.Mentee != null && ap.Name == "MenteeSurvey")) &&
                      qt.Name != "RankedList" &&
                      qt.Name != "MultipleChoiceRanked"
                      select new ExportProfile
                      {
                          UserId = u.UserId,
                          EmployeeName = u.Name,
                          EmployeeEmail = u.EmailAddress,
                          PracticeGroup = u.PracticeGroup,
                          Office = u.Office,
                          QuestionText = q.Text,
                          QuestionType = qt.Name,
                          AnswerId = a.AnswerId,
                          Answer = (
                            qt.Name == "Comment" ? a.Text :
                            qt.Name == "Text" ? a.Text :
                            qt.Name == "SingleChoice" ? c.Description :
                            qt.Name == "DropDownList" ? c.Description :
                            qt.Name == "MultipleChoice" ? cx.ChoiceDescription :
                            ""
                          )
                      };

            return qry;
        }

        private IEnumerable<ExportMenteeProfile2018> GetMenteeProfile2018(IEnumerable<QuestionAnswerGroup> grp)
        {
            var xl = from q in grp
                     group q by new { q.UserId, q.EmployeeEmail, q.EmployeeName, q.Office, q.PracticeGroup }
                         into g
                     select new ExportMenteeProfile2018
                     {
                         Answer1 = GetAnswer(0, g),
                         Answer2 = GetAnswer(1, g),
                         Answer3 = GetAnswer(2, g),
                         Answer4 = GetAnswer(3, g),
                         Answer5 = GetAnswer(4, g),
                         Answer6_1 = GetAnswer(5, g, 0),
                         Answer6_2 = GetAnswer(5, g, 1),
                         Answer6_3 = GetAnswer(5, g, 2),
                         Answer6_4 = GetAnswer(5, g, 3),
                         Answer6_5 = GetAnswer(5, g, 4),
                         Answer7 = GetAnswer(6, g),
                         UserId = g.Key.UserId,
                         EmployeeEmail = g.Key.EmployeeEmail,
                         EmployeeName = g.Key.EmployeeName,
                         Office = g.Key.Office,
                         PracticeGroup = g.Key.PracticeGroup,
                         Question1 = GetQuestion(0, g),
                         Question2 = GetQuestion(1, g),
                         Question3 = GetQuestion(2, g),
                         Question4 = GetQuestion(3, g),
                         Question5 = GetQuestion(4, g),
                         Question6_1 = GetQuestion(5, g),
                         Question6_2 = GetQuestion(5, g),
                         Question6_3 = GetQuestion(5, g),
                         Question6_4 = GetQuestion(5, g),
                         Question6_5 = GetQuestion(5, g),
                         Question7 = GetQuestion(6, g),
                     };
            return xl;
        }

        private IEnumerable<ExportMentorProfile2018> GetMentorProfile2018(IEnumerable<QuestionAnswerGroup> grp)
        {
            var xl = from q in grp
                     group q by new { q.UserId, q.EmployeeEmail, q.EmployeeName, q.Office, q.PracticeGroup }
                         into g
                     select new ExportMentorProfile2018
                     {
                         Answer1 = GetAnswer(0, g),
                         Answer2 = GetAnswer(1, g),
                         Answer3 = GetAnswer(2, g),
                         Answer4 = GetAnswer(3, g),
                         Answer5 = GetAnswer(4, g),
                         Answer6_1 = GetAnswer(5, g, 0),
                         Answer6_2 = GetAnswer(5, g, 1),
                         Answer6_3 = GetAnswer(5, g, 2),
                         Answer6_4 = GetAnswer(5, g, 3),
                         Answer6_5 = GetAnswer(5, g, 4),
                         Answer7 = GetAnswer(6, g),
                         UserId = g.Key.UserId,
                         EmployeeEmail = g.Key.EmployeeEmail,
                         EmployeeName = g.Key.EmployeeName,
                         Office = g.Key.Office,
                         PracticeGroup = g.Key.PracticeGroup,
                         Question1 = GetQuestion(0, g),
                         Question2 = GetQuestion(1, g),
                         Question3 = GetQuestion(2, g),
                         Question4 = GetQuestion(3, g),
                         Question5 = GetQuestion(4, g),
                         Question6_1 = GetQuestion(5, g),
                         Question6_2 = GetQuestion(5, g),
                         Question6_3 = GetQuestion(5, g),
                         Question6_4 = GetQuestion(5, g),
                         Question6_5 = GetQuestion(5, g),
                         Question7 = GetQuestion(6, g),
                     };
            return xl;
        }


        private string GetQuestion(int index, IGrouping<object, QuestionAnswerGroup> qaGroup)
        {
            string q = "";
            try
            {
                var elm = qaGroup.ElementAt(index);
                if (elm != null)
                {
                    q = elm.QuestionText;
                }
            }
            catch
            {
                q = "";
            }
            return q;
        }

        private string GetAnswer(int index, IGrouping<object, QuestionAnswerGroup> qaGroup, int answerIndex = 0)
        {
            string a = "";
            try
            {
                var elm = qaGroup.ElementAt(index);
                if (elm != null && elm.Answers != null && elm.Answers.Count() > answerIndex)
                {
                    a = elm.Answers.ElementAt(answerIndex);
                }
            }
            catch
            {
                a = "";
            }
            return a;
        }

        internal class QuestionAnswerGroup
        {
            public int UserId { get; set; }
            public string EmployeeEmail { get; set; }
            public string EmployeeName { get; set; }
            public string Office { get; set; }
            public string PracticeGroup { get; set; }
            public string QuestionText { get; set; }
            public string QuestionType { get; set; }
            public IEnumerable<string> Answers { get; set; }
        }

        private ResponseMessageResult ExcelResponse(byte[] excel, string name)
        {
            var rslt = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(excel)
            };
            rslt.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = name
            };
            rslt.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/ms-excel");
            return ResponseMessage(rslt);
        }
    }
}
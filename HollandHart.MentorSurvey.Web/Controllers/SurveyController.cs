﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using HollandHart.MentorSurvey.Model;

namespace HollandHart.MentorSurvey.Web.Controllers
{
    public class SurveyController : HHApiController
    {

        private IEnumerable<Model.Question> GetQuestion(int surveyId, int index)
        {
            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      where sq.SurveyId == surveyId
                      orderby q.DisplayOrder
                      select q;

            return qry.Skip(index).Take(1);
        }

        private bool AnswerExists(int id)
        {
            return db.Answers.Count(e => e.AnswerId == id) > 0;
        }

        [HttpGet]
        public Model.Survey GetSurvey(int id)
        {
            var qry = from s in db.Surveys
                      where s.SurveyId == id
                      select s;

            return qry.FirstOrDefault();
        }


        /// <summary>
        /// GET: api/Survey/Question/1/5 
        /// zero based
        /// </summary>
        /// <param name="index"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Question")]
        public IHttpActionResult GetSurveyQuestion(int id, int index)
        {
            var usr = GetCurrentUser();
            var qst = GetQuestion(id, index).FirstOrDefault();

            var ans = from a in db.Answers
                      join q in GetQuestion(id, index) on a.QuestionId equals q.QuestionId
                      join sq in db.SurveyQuestions on a.SurveyId equals sq.SurveyId
                      where a.SurveyId == id
                      && a.UserId == usr.UserId
                      select a;
            if (ans.Count() <= 0)
            {
                db.Answers.Add(new Model.Answer
                {
                    QuestionId = qst.QuestionId,
                    SurveyId = id,
                    Text = "",
                    Value = "",
                    UserId = usr.UserId

                });
                db.SaveChanges();
            }

            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      join qt in db.QuestionTypes on q.QuestionTypeId equals qt.QuestionTypeId
                      join a in db.Answers on q.QuestionId equals a.QuestionId
                      join r in db.RelatedQuestions on q.QuestionId equals r.QuestionOneId into rs
                      from r in rs.DefaultIfEmpty()
                      join rq in db.Questions on r.QuestionTwoId equals rq.QuestionId into rqs
                      from rq in rqs.DefaultIfEmpty()
                      where q.QuestionId == qst.QuestionId &&
                          sq.SurveyId == id &&
                          a.SurveyId == id &&
                          a.UserId == usr.UserId
                      orderby q.DisplayOrder
                      select new
                      {
                          q.QuestionId,
                          q.Text,
                          q.Title,
                          q.DisplayOrder,
                          q.Description,
                          q.QuestionTypeId,
                          QuestionTypeName = qt.Name,
                          Rules = q.QuestionRules.Select(r => r.Rule.Definition),
                          Choices = q.Choices.OrderBy(c => c.ChoiceId),
                          RelatedChoices = rq.Choices,
                          RelatedAnswers = rq.Answers.Where(a => a.UserId == usr.UserId),
                          RelatedRules = rq.QuestionRules.Select(r => r.Rule.Definition),
                          Answer = a
                      };

            var question = qry.FirstOrDefault();
            if (question == null)
            {
                return NotFound();
            }

            return Ok(question);
        }

        [HttpGet]
        [ActionName("Questions")]
        public IQueryable<Question> GetSurveyQuestions(int id)
        {
            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      where sq.SurveyId == id
                      select q;

            return qry;
        }

        [HttpGet]
        [ActionName("QuestionIndex")]
        public IHttpActionResult GetIndex(int id, int qid)
        {
            var qry = from q in db.Questions
                      join sq in db.SurveyQuestions on q.QuestionId equals sq.QuestionId
                      where sq.SurveyId == id
                      orderby q.DisplayOrder
                      select new QIDRow() { Index = 0, QuestionId = q.QuestionId };
            var qs = qry.ToArray().Select((q, i) => { q.Index = i; return q; });
            var qi = qs.Where(q => q.QuestionId == qid).FirstOrDefault();
            return Ok(qi == null ? -1 : qi.Index);
        }

        internal class QIDRow
        {
            public int Index { get; set; }
            public int QuestionId { get; set; }
        }


        [HttpGet]
        [ActionName("Count")]
        public int GetCount(int id)
        {
            return GetSurveyQuestions(id).Count();
        }

        [HttpGet]
        [ActionName("Answer")]
        public IHttpActionResult GetAnswer(int id, int index)
        {
            var qry = from sq in db.SurveyQuestions
                      join a in db.Answers on sq.QuestionId equals a.QuestionId
                      join u in db.Users on a.UserId equals u.UserId
                      where sq.SurveyId == id &&
                      sq.QuestionId == index &&
                      u.ADLogin == ADLoginName
                      select new
                      {
                          a.AnswerId,
                          a.Value,
                          a.Text,
                          a.SurveyId,
                          a.UserId,
                          a.QuestionId
                      };

            var answer = qry.FirstOrDefault();

            if (answer == null)
            {
                return NotFound();
            }

            return Ok(answer);
        }

        [HttpGet]
        [ActionName("Answers")]
        public IHttpActionResult GetAnswers(int id)
        {
            var qry = from sq in db.SurveyQuestions
                      join a in db.Answers on sq.QuestionId equals a.QuestionId
                      join u in db.Users on a.UserId equals u.UserId
                      join q in db.Questions on sq.QuestionId equals q.QuestionId
                      join qt in db.QuestionTypes on q.QuestionTypeId equals qt.QuestionTypeId
                      join sa in db.SurveyAudiences on new { sq.SurveyId, u.UserId } equals new { sa.SurveyId, sa.UserId }
                      join r in db.RelatedQuestions on q.QuestionId equals r.QuestionOneId into rs
                      from r in rs.DefaultIfEmpty()
                      join rq in db.Questions on r.QuestionTwoId equals rq.QuestionId into rqs
                      from rq in rqs.DefaultIfEmpty()
                      where sq.SurveyId == id &&
                      u.ADLogin == ADLoginName
                      orderby q.DisplayOrder
                      select new
                      {
                          a.AnswerId,
                          AnswerValue = a.Value,
                          AnswerText = a.Text,
                          a.SurveyId,
                          a.UserId,
                          a.QuestionId,
                          qt.QuestionTypeId,
                          QuestionTypeName = qt.Name,
                          QuestionText = q.Text,
                          QuestionTitle = q.Title,
                          Rules = q.QuestionRules.Select(r => r.Rule.Definition),
                          q.Choices,
                          sa.Complete,
                          RelatedChoices = rq.Choices,
                          RelatedAnswers = rq.Answers.Where(a => a.UserId == u.UserId),
                          RelatedRules = rq.QuestionRules.Select(r => r.Rule.Definition),
                          RelatedQuestionText = rq.Text,
                          RelatedQuestionId = (rq == null) ? null : (int?)rq.QuestionId
                      };

            return Ok(qry.ToArray());
        }

        [HttpPut]
        [ActionName("Answer")]
        public IHttpActionResult PutAnswer(int id, Model.Answer answer)
        {
            User user = GetCurrentUser();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != answer.AnswerId)
            {
                return BadRequest();
            }

            if (answer.UserId != user.UserId)
            {
                return BadRequest();
            }

            db.Entry(answer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnswerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPut]
        [ActionName("Complete")]
        public IHttpActionResult PutComplete(int id)
        {
            User usr = GetCurrentUser();

            var qry = from sa in db.SurveyAudiences
                      where sa.SurveyId == id
                      && sa.UserId == usr.UserId
                      select sa;

            var srvA = qry.FirstOrDefault();
            if (srvA != null)
            {
                srvA.Complete = true;
                db.SaveChanges();
                var srv = GetSurvey(srvA.SurveyId);
                if (srv != null)
                {
                    SendAdminEmail("Profile Complete", usr.Name + " completed a " + srv.Name + " Profile.");
                    SendUserEmail(usr.EmailAddress, srv.Name + "Profile");
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpGet]
        [ActionName("Title")]
        public IHttpActionResult GetTitle(int id)
        {
            var qry = from s in db.Surveys
                      where s.SurveyId == id
                      select new
                      {
                          s.Title,
                          s.Description,
                          s.Description2
                      };

            return Ok(qry.FirstOrDefault());
        }

    }
}

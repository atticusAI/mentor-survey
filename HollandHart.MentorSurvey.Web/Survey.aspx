﻿<%@ page title="Profile" language="C#" masterpagefile="~/Site.Master" autoeventwireup="true" codebehind="Survey.aspx.cs" inherits="HollandHart.MentorSurvey.Web.Survey" %>

<%@ register src="~/Controls/Survey.ascx" tagprefix="hh" tagname="Survey" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <hh:Survey runat="server" ID="srv" />
</asp:Content>

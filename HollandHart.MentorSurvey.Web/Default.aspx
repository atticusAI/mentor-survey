﻿<%@ page title="Home Page" language="C#" masterpagefile="~/Site.Master" autoeventwireup="true" codebehind="Default.aspx.cs" inherits="HollandHart.MentorSurvey.Web._Default" %>

<%@ register src="~/Controls/MySurveys.ascx" tagprefix="hh" tagname="MySurveys" %>
<%@ register src="~/Controls/Volunteer.ascx" tagprefix="hh" tagname="Volunteer" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrapper">
        <div>
            <p>Welcome to the new Holland & Hart Attorney Mentoring Program! So we can provide you with 
                the best possible match for your interests in being mentored or providing mentorship, 
                please complete a profile by clicking on the appropriate link below. There are eight 
                simple questions that will take five minutes or less to complete.</p>
            <p>Once you complete your profile, you will receive a confirmation email. Your profile will 
                be reviewed by the Mentoring Program coordinator and you will notified by email of your 
                “match.”</p>
            <p>If you encounter any technical difficulties completing the profile, please click the blue 
                “Provide feedback” button on the right-hand side of your screen.</p>
        </div>
        <h2>My Profiles</h2>
        <hh:MySurveys runat="server" ID="MySurveys" ShowComplete="false" />

        <h2>My Complete Profiles</h2>
        <hh:MySurveys runat="server" ID="MySurveys2" ShowComplete="true" />
    </div>
</asp:Content>

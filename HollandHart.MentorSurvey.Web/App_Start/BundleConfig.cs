using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace HollandHart.MentorSurvey.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit https://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/frameworks")
                .Include("~/Scripts/jquery-3.3.1.js")
                .Include("~/Scripts/umd/popper.js")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/promise-7.0.4.js")
                .Include("~/Scripts/jsrender.js")
                .Include("~/Scripts/jquery-ui-1.12.1.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/HH")
                .Include("~/Scripts/HH*"));

            bundles.Add(new StyleBundle("~/bundles/css")
                .Include("~/Content/jquery-ui.css")
                .Include("~/Content/Site.css")
                );
        }
    }
}

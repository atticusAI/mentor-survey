﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HollandHart.MentorSurvey.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            config.Routes.MapHttpRoute(
                name: "Action",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Search",
                routeTemplate: "api/{controller}/{action}/Term/{term}",
                defaults: new { term = "" }
            );

            config.Routes.MapHttpRoute(
                name: "SearchType",
                routeTemplate: "api/{controller}/{action}/Term/{term}/Type/{type}",
                defaults: new { term = "", type = "All" }
            );

            config.Routes.MapHttpRoute(
                name: "Type",
                routeTemplate: "api/{controller}/{action}/Type/{type}",
                defaults: new { term = "", type = "All" }
            );


            config.Routes.MapHttpRoute(
                name: "Category",
                routeTemplate: "api/{controller}/{action}/Category/{category}",
                defaults: new { category = "all" }
            );

            config.Routes.MapHttpRoute(
                name: "Count",
                routeTemplate: "api/{controller}/{action}/Count"
            );

            config.Routes.MapHttpRoute(
                name: "Page",
                routeTemplate: "api/{controller}/{action}/Page/{page}/Length/{length}"
            );

            config.Routes.MapHttpRoute(
                name: "CategoryPage",
                routeTemplate: "api/{controller}/{action}/Category/{category}/Page/{page}/Length/{length}"
            );

            config.Routes.MapHttpRoute(
                name: "Index",
                routeTemplate: "api/{controller}/{action}/{id}/Index/{index}"
            );

            config.Routes.MapHttpRoute(
                name: "User",
                routeTemplate: "api/{controller}/{action}/{id}/User/{userId}"
            );

            config.Routes.MapHttpRoute(
                name: "IndexUser",
                routeTemplate: "api/{controller}/{action}/{id}/Index/{index}/User/{userId}"
            );


            config.Routes.MapHttpRoute(
                name: "Question",
                routeTemplate: "api/{controller}/{action}/{id}/Question/{qid}"
            );

        }
    }
}

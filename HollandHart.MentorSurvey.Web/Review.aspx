﻿<%@ page title="Review Profile" language="C#" masterpagefile="~/Site.Master" autoeventwireup="true" codebehind="Review.aspx.cs" inherits="HollandHart.MentorSurvey.Web.Review" %>

<%@ register src="~/Controls/Review.ascx" tagprefix="hh" tagname="Review" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <hh:Review runat="server" ID="rvw" />
</asp:Content>

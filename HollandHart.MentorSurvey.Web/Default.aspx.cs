﻿using HollandHart.MentorSurvey.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HollandHart.MentorSurvey.Web
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            using(var ctrl = new UserController())
            {
                ctrl.EnsureUser();
            }

        }
    }
}

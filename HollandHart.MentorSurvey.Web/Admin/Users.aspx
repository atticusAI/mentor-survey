﻿<%@ page title="Manage Users" language="C#" masterpagefile="~/Site.Master" autoeventwireup="true" codebehind="Users.aspx.cs" inherits="HollandHart.MentorSurvey.Web.Admin.Users" %>

<%@ register src="~/Controls/UserManager.ascx" tagprefix="hh" tagname="UserManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrapper">
        <hh:UserManager runat="server" ID="UserManager" />
    </div>
</asp:Content>

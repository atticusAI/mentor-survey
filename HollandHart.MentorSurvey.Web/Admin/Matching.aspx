﻿<%@ page title="Manual Matching" language="C#" masterpagefile="~/Site.Master" autoeventwireup="true" codebehind="Matching.aspx.cs" inherits="HollandHart.MentorSurvey.Web.Admin.Matching" %>

<%@ register src="~/Controls/UserSelect.ascx" tagprefix="hh" tagname="UserSelect" %>
<%@ register src="~/Controls/UserDropDown.ascx" tagprefix="hh" tagname="UserDropDown" %>
<%@ register src="~/Controls/MatchingSelector.ascx" tagprefix="hh" tagname="MatchingSelector" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrapper">
        <div class="row">
            <div class="col-xs-4">
                <input type="button" class="btn btn-default" id="export" runat="server" onclick="window.open('/api/Export/Matches', '_blank')" value="Export" />
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" id="drpSuggest" aria-haspopup="true" aria-expanded="false">
                    Suggest Matches
                </button>
                <div class="dropdown-menu" aria-labelledby="drpSuggest">
                    <a class="dropdown-item" id="allsugs" runat="server" onclick="window.open('/api/Export/SuggestedMatches', '_blank')">All</a>
                    <a class="dropdown-item" id="unsugs" runat="server" href="#" onclick="window.open('/api/Export/SuggestedMatches/Type/Unmatched', '_blank')">Unmatched</a>
                    <a class="dropdown-item" id="selectsugs" runat="server" href="#">Select...</a>
                </div>
            </div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-xs-4">
                <hh:UserDropDown runat="server" ID="mentor" Text="Select a Mentor:" Type="Mentor" />
                <%--<hh:UserSelect runat="server" ID="mentor" Text="Select a mentor:" Type="Mentor" />--%>
                <div>
                    <ul id="mentees" runat="server"></ul>
                </div>
            </div>
            <div class="col-xs-4">
                <hh:UserDropDown runat="server" ID="mentee" Text="Select a Mentee:" Type="Mentee" />
                <%--<hh:UserSelect runat="server" ID="mentee" Text="Select a mentee:" Type="Mentee" />--%>
                <div>
                    <ul id="mentors" runat="server"></ul>
                </div>
            </div>
            <div class="col-xs-4">
                <input type="button" class="btn btn-default" id="match" runat="server" value="Match" />
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var <%=ClientID%>_ctrl = new hh.Matching({
            'mentor': <%=mentor.ClientID%>_ctrl,
            'mentee': <%=mentee.ClientID%>_ctrl,
            '$match': $('#<%=match.ClientID%>'),
            '$mentors': $('#<%=mentors.ClientID%>'),
            '$mentees': $('#<%=mentees.ClientID%>')
        });
    </script>
    <script type="text/x-jsrender" id="usersTemplate">
        <li>
            <a href="#" data-id="{{:MentorAssignmentId}}" class="remove" title="delete"><span class="octicon octicon-x" style="color: red;"></span></a>
            {{:Name}}: {{:EmailAddress}}
        </li>
    </script>
    <hh:MatchingSelector runat="server" ID="MatchingSelector" />

</asp:Content>

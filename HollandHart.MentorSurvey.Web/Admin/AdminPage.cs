﻿using HollandHart.MentorSurvey.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HollandHart.MentorSurvey.Web.Admin
{
    public class AdminPage : System.Web.UI.Page
    {

        protected HHMentorSurveyEntities db = new HHMentorSurveyEntities();

        protected bool IAmAdmin()
        {
            string adlogin = GetADLoginName();
            var qry = from a in db.Admins
                      join u in db.Users on a.AdminId equals u.UserId
                      where u.ADLogin == adlogin
                      select a;
            return (qry.Count() > 0);
        }

        protected string GetADLoginName()
        {
            return Request.LogonUserIdentity.Name.Split('\\')[1];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IAmAdmin())
            {
                Response.Redirect("/");
            }
        }
    }
}
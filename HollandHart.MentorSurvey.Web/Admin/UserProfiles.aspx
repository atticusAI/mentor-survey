﻿<%@ page title="Manage Surveys" language="C#" masterpagefile="~/Site.Master" autoeventwireup="true" codebehind="UserProfiles.aspx.cs" inherits="HollandHart.MentorSurvey.Web.Admin.Surveys" %>

<%@ register src="~/Controls/UserSelect.ascx" tagprefix="hh" tagname="UserSelect" %>
<%@ register src="~/Controls/Survey.ascx" tagprefix="hh" tagname="Survey" %>
<%@ register src="~/Controls/UserDropDown.ascx" tagprefix="hh" tagname="UserDropDown" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrapper">
        <div class="wrapper">
            <input type="button" class="btn btn-default" id="mentor" runat="server" onclick="window.open('/api/Export/Profiles/Type/Mentor', '_blank')" value="Export Mentor Profiles" />
            <input type="button" class="btn btn-default" id="mentee" runat="server" onclick="window.open('/api/Export/Profiles/Type/Mentee', '_blank')" value="Export Mentee Profiles" />
        </div>
        <div>
            <hh:UserDropDown runat="server" ID="user" />
            <div>
                <div>
                    <ul id="uSrv" runat="server" class="list-group"></ul>
                </div>
                <div id="dSrv" runat="server"></div>
                <div id="dEdt" runat="server">
                    <hh:Survey runat="server" ID="srv" />
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        <%=ClientID%>_ctrl = new hh.UserSurveys( <%=user.ClientID%>_ctrl, <%=srv.ClientID%>_ctrl, $('#<%=uSrv.ClientID%>'), $('#<%=dSrv.ClientID%>'));
    </script>

    <script type="text/x-jsrender" id="LockSurveyView">
        <li class="list-group-item">
            <div class="btn-group">
                <input type="button" class="btn btn-default select" data-survey-id="{{:SurveyId}}" value="{{:Name}}: {{:Description}}" />
                {{if Complete}}
                <input type="button" class="btn btn-default lock" data-survey-id="{{:SurveyId}}" data-locked="true" value="UnLock" />
                {{else}}
                <input type="button" class="btn btn-default lock" data-survey-id="{{:SurveyId}}" data-locked="false" value="Lock" />
                {{/if}}
            </div>
        </li>
    </script>

</asp:Content>

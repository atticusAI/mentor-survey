﻿There is a method in the UserAdminController class that is not wired to anything in the app.  It was put in place to 
update the user offices when that field was added to the model.

curl -v --ntlm --user hollandhart\bl_mcgee -d "" -X POST http://mentor.hollandhart.com/api/UserAdmin/UpdateAllOffices
﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HollandHart.MentorSurvey.SolverApi
{
    class MMSolver
    {
        public MMSolverResponse GetMM()
        {
            var cli = new RestClient("https://atticus.hollandhart.com/flask");
            cli.AddHandler("application/json", null); //  new SolverResponseDeserializer());
            var rqst = new RestRequest("mm-interface", Method.POST);
            rqst.JsonSerializer = null; // new SolverRequestSerializer();
            rqst.AddHeader("Content-Type", "application/json");
            rqst.AddJsonBody(null); // GetSolverRequest());
            IRestResponse<MMSolverResponse> rsp = cli.Execute<MMSolverResponse>(rqst);
            return rsp.Data;
        }
    }
}

﻿using Newtonsoft.Json;
using RestSharp;

namespace HollandHart.MentorSurvey.SolverApi
{
    class Deserializer
    {
        private string _rootElement = "";
        private string _namespace = "";
        private string _dateFormat = "";

        public string RootElement
        {
            get => _rootElement;
            set => _rootElement = value;
        }
        public string Namespace
        {
            get => _namespace;
            set => _namespace = value;
        }
        public string DateFormat
        {
            get => _dateFormat;
            set => _dateFormat = value;
        }

        public T Deserialize<T>(IRestResponse response)
        {
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}

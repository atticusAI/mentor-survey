﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HollandHart.MentorSurvey.SolverApi
{
    class Serializer
    {
        private string _rootElement = "";
        private string _namespace = "";
        private string _dateFormat = "";
        private string _contentType = "application/json";

        public string RootElement
        {
            get => _rootElement;
            set => _rootElement = value;
        }
        public string Namespace
        {
            get => _namespace;
            set => _namespace = value;
        }
        public string DateFormat
        {
            get => _dateFormat;
            set => _dateFormat = value;
        }
        public string ContentType
        {
            get => _contentType;
            set => _contentType = value;
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HollandHart.MentorSurvey.SolverApi
{
    public class MMSolverResponse
    {
        [JsonProperty("assignments")]
        public List<Dictionary<string, List<string>>> Assignments { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("status_desc")]
        public string StatusDesc { get; set; }
    }
}

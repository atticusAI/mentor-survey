﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HollandHart.MentorSurvey.SolverApi
{
    class MMSolverRequest
    {
        public MMSolverRequest()
        {
            Scores = new Dictionary<string, Dictionary<string, decimal>>();
            Slots = new Dictionary<string, int>();
            Forbidden = new List<List<string>>();
        }

        [JsonProperty("scores")]
        public Dictionary<string, Dictionary<string, decimal>> Scores { get; set; }

        [JsonProperty("slots")]
        public Dictionary<string, int> Slots { get; set; }

        [JsonProperty("forbidden")]
        public List<List<string>> Forbidden { get; set; }
    }
}

﻿namespace HollandHart.SolverApi.Models
{
    public class MentorMatchingScore
    {
        public string Mentor { get; set; }

        public string Mentee { get; set; }

        public decimal Score { get; set; }
    }
}
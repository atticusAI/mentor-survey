﻿namespace HollandHart.SolverApi.Models
{
    public class MentorMatchingSlot
    {
        public string User { get; set; }

        public int Requested { get; set; }
    }
}
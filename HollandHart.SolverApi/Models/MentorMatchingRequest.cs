﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HollandHart.SolverApi.Models
{
    public class MentorMatchingRequest
    {
        public MentorMatchingRequest()
        {
            Scores = new Dictionary<string, Dictionary<string, decimal>>();
            Slots = new Dictionary<string, int>();
            Forbidden = new List<List<string>>();
        }

        public MentorMatchingRequest(IEnumerable<MentorMatchingScore> scores, IEnumerable<MentorMatchingSlot> slots): this()
        {
            foreach (var s in scores)
            {
                if (s.Mentor.Replace("M", "") == s.Mentee.Replace("E", ""))
                {
                    Forbidden.Add(new List<string>() { s.Mentor, s.Mentee });
                }

                if (Scores.ContainsKey(s.Mentor))
                {
                    Scores[s.Mentor].Add(s.Mentee, s.Score);
                }
                else
                {
                    Scores.Add(s.Mentor, new Dictionary<string, decimal>());
                    Scores[s.Mentor].Add(s.Mentee, s.Score);
                }
            }

            foreach (var a in slots)
            {
                Slots.Add(a.User, a.Requested);
            }
        }


        [JsonProperty("scores")]
        public Dictionary<string, Dictionary<string, decimal>> Scores { get; set; }

        [JsonProperty("slots")]
        public Dictionary<string, int> Slots { get; set; }

        [JsonProperty("forbidden")]
        public List<List<string>> Forbidden { get; set; }
    }
}

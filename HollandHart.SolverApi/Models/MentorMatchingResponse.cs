﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HollandHart.SolverApi.Models
{
    public class MentorMatchingResponse
    {
        [JsonProperty("assignments")]
        public List<Dictionary<string, List<string>>> Assignments { get; set; }

        [JsonProperty("remarks")]
        public string Remarks { get; set; }

        [JsonProperty("status_desc")]
        public string StatusDescription { get; set; }
    }
}

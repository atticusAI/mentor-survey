﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HollandHart.SolverApi
{
    public class SolverRequest
    {
        public SolverRequest (string baseUrl)
        {
            _baserUrl = baseUrl;
        }

        private string _baserUrl;

        public string BaserUrl { get => _baserUrl; set => _baserUrl = value; }

        public T PostRequest<T>(string resource, object body)
        {
            Deserializer des = new Deserializer();
            var cli = new RestClient(_baserUrl);
            var rqst = new RestRequest(resource, Method.POST);
            rqst.JsonSerializer = new Serializer();
            rqst.AddJsonBody(body);
            IRestResponse rsp = cli.Execute(rqst);
            return JsonConvert.DeserializeObject<T>(rsp.Content);
        }
    }
}

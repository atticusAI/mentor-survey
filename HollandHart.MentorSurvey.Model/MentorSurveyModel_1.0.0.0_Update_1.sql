﻿USE [HHMentorSurvey]

ALTER TABLE dbo.Questions ADD
	Description nvarchar(MAX) NULL
GO

ALTER TABLE dbo.Questions
	DROP COLUMN Importance
GO

UPDATE dbo.Questions SET [Description] = 'Check and UnCheck areas  to indicate your interest.  Drag and Drop to reorder them.'
WHERE QuestionId IN (1014, 1021);
GO


USE [HHMentorSurvey];
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Admins_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Admins] DROP CONSTRAINT [FK_Admins_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_Mentees_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Mentees] DROP CONSTRAINT [FK_Mentees_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_MentorAssignments_Mentees]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MentorAssignments] DROP CONSTRAINT [FK_MentorAssignments_Mentees];
GO
IF OBJECT_ID(N'[dbo].[FK_MentorAssignments_Mentors]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MentorAssignments] DROP CONSTRAINT [FK_MentorAssignments_Mentors];
GO
IF OBJECT_ID(N'[dbo].[FK_Mentors_Users]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Mentors] DROP CONSTRAINT [FK_Mentors_Users];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answers] DROP CONSTRAINT [FK_QuestionAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionChoice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Choices] DROP CONSTRAINT [FK_QuestionChoice];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionRules_Questions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuestionRules] DROP CONSTRAINT [FK_QuestionRules_Questions];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionRules_Rules]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuestionRules] DROP CONSTRAINT [FK_QuestionRules_Rules];
GO
IF OBJECT_ID(N'[dbo].[FK_Questions_QuestionTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Questions] DROP CONSTRAINT [FK_Questions_QuestionTypes];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionSurveyQuestion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyQuestions] DROP CONSTRAINT [FK_QuestionSurveyQuestion];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionTypeRules_QuestionTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuestionTypeRules] DROP CONSTRAINT [FK_QuestionTypeRules_QuestionTypes];
GO
IF OBJECT_ID(N'[dbo].[FK_QuestionTypeRules_Rules]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[QuestionTypeRules] DROP CONSTRAINT [FK_QuestionTypeRules_Rules];
GO
IF OBJECT_ID(N'[dbo].[FK_RelatedQuestions_Questions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RelatedQuestions] DROP CONSTRAINT [FK_RelatedQuestions_Questions];
GO
IF OBJECT_ID(N'[dbo].[FK_RelatedQuestions_Questions1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RelatedQuestions] DROP CONSTRAINT [FK_RelatedQuestions_Questions1];
GO
IF OBJECT_ID(N'[dbo].[FK_Rules_RuleType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Rules] DROP CONSTRAINT [FK_Rules_RuleType];
GO
IF OBJECT_ID(N'[dbo].[FK_SurveyAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answers] DROP CONSTRAINT [FK_SurveyAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_SurveySurveyAudience]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyAudiences] DROP CONSTRAINT [FK_SurveySurveyAudience];
GO
IF OBJECT_ID(N'[dbo].[FK_SurveySurveyQuestion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyQuestions] DROP CONSTRAINT [FK_SurveySurveyQuestion];
GO
IF OBJECT_ID(N'[dbo].[FK_UserAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Answers] DROP CONSTRAINT [FK_UserAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_UserSurveyAudience]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SurveyAudiences] DROP CONSTRAINT [FK_UserSurveyAudience];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Admins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Admins];
GO
IF OBJECT_ID(N'[dbo].[Answers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Answers];
GO
IF OBJECT_ID(N'[dbo].[AppSettings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AppSettings];
GO
IF OBJECT_ID(N'[dbo].[Choices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Choices];
GO
IF OBJECT_ID(N'[dbo].[Mentees]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Mentees];
GO
IF OBJECT_ID(N'[dbo].[MentorAssignments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MentorAssignments];
GO
IF OBJECT_ID(N'[dbo].[Mentors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Mentors];
GO
IF OBJECT_ID(N'[dbo].[QuestionRules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[QuestionRules];
GO
IF OBJECT_ID(N'[dbo].[Questions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Questions];
GO
IF OBJECT_ID(N'[dbo].[QuestionTypeRules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[QuestionTypeRules];
GO
IF OBJECT_ID(N'[dbo].[QuestionTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[QuestionTypes];
GO
IF OBJECT_ID(N'[dbo].[RelatedQuestions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RelatedQuestions];
GO
IF OBJECT_ID(N'[dbo].[Rules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rules];
GO
IF OBJECT_ID(N'[dbo].[RuleTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RuleTypes];
GO
IF OBJECT_ID(N'[dbo].[SurveyAudiences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SurveyAudiences];
GO
IF OBJECT_ID(N'[dbo].[SurveyQuestions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SurveyQuestions];
GO
IF OBJECT_ID(N'[dbo].[Surveys]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Surveys];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Admins'
CREATE TABLE [dbo].[Admins] (
    [AdminId] int  NOT NULL
);
GO

-- Creating table 'Choices'
CREATE TABLE [dbo].[Choices] (
    [ChoiceId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NULL,
    [Description] nvarchar(max)  NOT NULL,
    [QuestionId] int  NOT NULL,
    [SortOrder] int  NULL,
    [IsDefault] bit  NULL
);
GO

-- Creating table 'Mentees'
CREATE TABLE [dbo].[Mentees] (
    [MenteeId] int  NOT NULL
);
GO

-- Creating table 'MentorAssignments'
CREATE TABLE [dbo].[MentorAssignments] (
    [MentorAssignmentId] int IDENTITY(1,1) NOT NULL,
    [MentorId] int  NOT NULL,
    [MenteeId] int  NOT NULL
);
GO

-- Creating table 'Mentors'
CREATE TABLE [dbo].[Mentors] (
    [MentorId] int  NOT NULL
);
GO

-- Creating table 'Questions'
CREATE TABLE [dbo].[Questions] (
    [QuestionId] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(max)  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [QuestionTypeId] int  NOT NULL,
    [Title] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'SurveyAudiences'
CREATE TABLE [dbo].[SurveyAudiences] (
    [SurveyAudienceId] int IDENTITY(1,1) NOT NULL,
    [SurveyId] int  NOT NULL,
    [UserId] int  NOT NULL,
    [Complete] bit  NOT NULL
);
GO

-- Creating table 'SurveyQuestions'
CREATE TABLE [dbo].[SurveyQuestions] (
    [SurveyQuestionId] int IDENTITY(1,1) NOT NULL,
    [QuestionId] int  NOT NULL,
    [SurveyId] int  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(250)  NOT NULL,
    [ADLogin] nvarchar(250)  NULL,
    [PracticeGroup] nvarchar(250)  NULL,
    [EmailAddress] nvarchar(250)  NULL,
    [EmployeeId] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'QuestionTypeRules'
CREATE TABLE [dbo].[QuestionTypeRules] (
    [QuestionTypeRuleId] int IDENTITY(1,1) NOT NULL,
    [QuestionTypeId] int  NOT NULL,
    [RuleId] int  NOT NULL
);
GO

-- Creating table 'QuestionTypes'
CREATE TABLE [dbo].[QuestionTypes] (
    [QuestionTypeId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'Rules'
CREATE TABLE [dbo].[Rules] (
    [RuleId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Definition] nvarchar(max)  NULL,
    [RuleTypeId] int  NOT NULL
);
GO

-- Creating table 'Answers'
CREATE TABLE [dbo].[Answers] (
    [AnswerId] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(250)  NULL,
    [Text] nvarchar(max)  NULL,
    [SurveyId] int  NOT NULL,
    [UserId] int  NOT NULL,
    [QuestionId] int  NOT NULL
);
GO

-- Creating table 'QuestionRules'
CREATE TABLE [dbo].[QuestionRules] (
    [QuestionRuleId] int IDENTITY(1,1) NOT NULL,
    [QuestionId] int  NOT NULL,
    [RuleId] int  NOT NULL
);
GO

-- Creating table 'Surveys'
CREATE TABLE [dbo].[Surveys] (
    [SurveyId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Closed] bit  NOT NULL,
    [Title] nvarchar(256)  NULL,
    [Description2] nvarchar(max)  NULL,
    [SurveyStyle] nvarchar(max)  NULL
);
GO

-- Creating table 'AppSettings'
CREATE TABLE [dbo].[AppSettings] (
    [SettingId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(256)  NULL,
    [Value] nvarchar(50)  NULL,
    [Text] nvarchar(max)  NULL
);
GO

-- Creating table 'RuleTypes'
CREATE TABLE [dbo].[RuleTypes] (
    [RuleTypeId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'RelatedQuestions'
CREATE TABLE [dbo].[RelatedQuestions] (
    [RelatedQuestionId] int IDENTITY(1,1) NOT NULL,
    [QuestionOneId] int  NOT NULL,
    [QuestionTwoId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AdminId] in table 'Admins'
ALTER TABLE [dbo].[Admins]
ADD CONSTRAINT [PK_Admins]
    PRIMARY KEY CLUSTERED ([AdminId] ASC);
GO

-- Creating primary key on [ChoiceId] in table 'Choices'
ALTER TABLE [dbo].[Choices]
ADD CONSTRAINT [PK_Choices]
    PRIMARY KEY CLUSTERED ([ChoiceId] ASC);
GO

-- Creating primary key on [MenteeId] in table 'Mentees'
ALTER TABLE [dbo].[Mentees]
ADD CONSTRAINT [PK_Mentees]
    PRIMARY KEY CLUSTERED ([MenteeId] ASC);
GO

-- Creating primary key on [MentorAssignmentId] in table 'MentorAssignments'
ALTER TABLE [dbo].[MentorAssignments]
ADD CONSTRAINT [PK_MentorAssignments]
    PRIMARY KEY CLUSTERED ([MentorAssignmentId] ASC);
GO

-- Creating primary key on [MentorId] in table 'Mentors'
ALTER TABLE [dbo].[Mentors]
ADD CONSTRAINT [PK_Mentors]
    PRIMARY KEY CLUSTERED ([MentorId] ASC);
GO

-- Creating primary key on [QuestionId] in table 'Questions'
ALTER TABLE [dbo].[Questions]
ADD CONSTRAINT [PK_Questions]
    PRIMARY KEY CLUSTERED ([QuestionId] ASC);
GO

-- Creating primary key on [SurveyAudienceId] in table 'SurveyAudiences'
ALTER TABLE [dbo].[SurveyAudiences]
ADD CONSTRAINT [PK_SurveyAudiences]
    PRIMARY KEY CLUSTERED ([SurveyAudienceId] ASC);
GO

-- Creating primary key on [SurveyQuestionId] in table 'SurveyQuestions'
ALTER TABLE [dbo].[SurveyQuestions]
ADD CONSTRAINT [PK_SurveyQuestions]
    PRIMARY KEY CLUSTERED ([SurveyQuestionId] ASC);
GO

-- Creating primary key on [UserId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [QuestionTypeRuleId] in table 'QuestionTypeRules'
ALTER TABLE [dbo].[QuestionTypeRules]
ADD CONSTRAINT [PK_QuestionTypeRules]
    PRIMARY KEY CLUSTERED ([QuestionTypeRuleId] ASC);
GO

-- Creating primary key on [QuestionTypeId] in table 'QuestionTypes'
ALTER TABLE [dbo].[QuestionTypes]
ADD CONSTRAINT [PK_QuestionTypes]
    PRIMARY KEY CLUSTERED ([QuestionTypeId] ASC);
GO

-- Creating primary key on [RuleId] in table 'Rules'
ALTER TABLE [dbo].[Rules]
ADD CONSTRAINT [PK_Rules]
    PRIMARY KEY CLUSTERED ([RuleId] ASC);
GO

-- Creating primary key on [AnswerId] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [PK_Answers]
    PRIMARY KEY CLUSTERED ([AnswerId] ASC);
GO

-- Creating primary key on [QuestionRuleId] in table 'QuestionRules'
ALTER TABLE [dbo].[QuestionRules]
ADD CONSTRAINT [PK_QuestionRules]
    PRIMARY KEY CLUSTERED ([QuestionRuleId] ASC);
GO

-- Creating primary key on [SurveyId] in table 'Surveys'
ALTER TABLE [dbo].[Surveys]
ADD CONSTRAINT [PK_Surveys]
    PRIMARY KEY CLUSTERED ([SurveyId] ASC);
GO

-- Creating primary key on [SettingId] in table 'AppSettings'
ALTER TABLE [dbo].[AppSettings]
ADD CONSTRAINT [PK_AppSettings]
    PRIMARY KEY CLUSTERED ([SettingId] ASC);
GO

-- Creating primary key on [RuleTypeId] in table 'RuleTypes'
ALTER TABLE [dbo].[RuleTypes]
ADD CONSTRAINT [PK_RuleTypes]
    PRIMARY KEY CLUSTERED ([RuleTypeId] ASC);
GO

-- Creating primary key on [RelatedQuestionId] in table 'RelatedQuestions'
ALTER TABLE [dbo].[RelatedQuestions]
ADD CONSTRAINT [PK_RelatedQuestions]
    PRIMARY KEY CLUSTERED ([RelatedQuestionId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [AdminId] in table 'Admins'
ALTER TABLE [dbo].[Admins]
ADD CONSTRAINT [FK_Admins_Users]
    FOREIGN KEY ([AdminId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [QuestionId] in table 'Choices'
ALTER TABLE [dbo].[Choices]
ADD CONSTRAINT [FK_QuestionChoice]
    FOREIGN KEY ([QuestionId])
    REFERENCES [dbo].[Questions]
        ([QuestionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionChoice'
CREATE INDEX [IX_FK_QuestionChoice]
ON [dbo].[Choices]
    ([QuestionId]);
GO

-- Creating foreign key on [MenteeId] in table 'Mentees'
ALTER TABLE [dbo].[Mentees]
ADD CONSTRAINT [FK_Mentees_Users]
    FOREIGN KEY ([MenteeId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [MentorId] in table 'Mentors'
ALTER TABLE [dbo].[Mentors]
ADD CONSTRAINT [FK_Mentors_Users]
    FOREIGN KEY ([MentorId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [QuestionId] in table 'SurveyQuestions'
ALTER TABLE [dbo].[SurveyQuestions]
ADD CONSTRAINT [FK_QuestionSurveyQuestion]
    FOREIGN KEY ([QuestionId])
    REFERENCES [dbo].[Questions]
        ([QuestionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionSurveyQuestion'
CREATE INDEX [IX_FK_QuestionSurveyQuestion]
ON [dbo].[SurveyQuestions]
    ([QuestionId]);
GO

-- Creating foreign key on [UserId] in table 'SurveyAudiences'
ALTER TABLE [dbo].[SurveyAudiences]
ADD CONSTRAINT [FK_UserSurveyAudience]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserSurveyAudience'
CREATE INDEX [IX_FK_UserSurveyAudience]
ON [dbo].[SurveyAudiences]
    ([UserId]);
GO

-- Creating foreign key on [QuestionTypeId] in table 'Questions'
ALTER TABLE [dbo].[Questions]
ADD CONSTRAINT [FK_Questions_QuestionTypes]
    FOREIGN KEY ([QuestionTypeId])
    REFERENCES [dbo].[QuestionTypes]
        ([QuestionTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Questions_QuestionTypes'
CREATE INDEX [IX_FK_Questions_QuestionTypes]
ON [dbo].[Questions]
    ([QuestionTypeId]);
GO

-- Creating foreign key on [QuestionTypeId] in table 'QuestionTypeRules'
ALTER TABLE [dbo].[QuestionTypeRules]
ADD CONSTRAINT [FK_QuestionTypeRules_QuestionTypes]
    FOREIGN KEY ([QuestionTypeId])
    REFERENCES [dbo].[QuestionTypes]
        ([QuestionTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionTypeRules_QuestionTypes'
CREATE INDEX [IX_FK_QuestionTypeRules_QuestionTypes]
ON [dbo].[QuestionTypeRules]
    ([QuestionTypeId]);
GO

-- Creating foreign key on [RuleId] in table 'QuestionTypeRules'
ALTER TABLE [dbo].[QuestionTypeRules]
ADD CONSTRAINT [FK_QuestionTypeRules_Rules]
    FOREIGN KEY ([RuleId])
    REFERENCES [dbo].[Rules]
        ([RuleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionTypeRules_Rules'
CREATE INDEX [IX_FK_QuestionTypeRules_Rules]
ON [dbo].[QuestionTypeRules]
    ([RuleId]);
GO

-- Creating foreign key on [QuestionId] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [FK_QuestionAnswer]
    FOREIGN KEY ([QuestionId])
    REFERENCES [dbo].[Questions]
        ([QuestionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionAnswer'
CREATE INDEX [IX_FK_QuestionAnswer]
ON [dbo].[Answers]
    ([QuestionId]);
GO

-- Creating foreign key on [UserId] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [FK_UserAnswer]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserAnswer'
CREATE INDEX [IX_FK_UserAnswer]
ON [dbo].[Answers]
    ([UserId]);
GO

-- Creating foreign key on [QuestionId] in table 'QuestionRules'
ALTER TABLE [dbo].[QuestionRules]
ADD CONSTRAINT [FK_QuestionRules_Questions]
    FOREIGN KEY ([QuestionId])
    REFERENCES [dbo].[Questions]
        ([QuestionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionRules_Questions'
CREATE INDEX [IX_FK_QuestionRules_Questions]
ON [dbo].[QuestionRules]
    ([QuestionId]);
GO

-- Creating foreign key on [RuleId] in table 'QuestionRules'
ALTER TABLE [dbo].[QuestionRules]
ADD CONSTRAINT [FK_QuestionRules_Rules]
    FOREIGN KEY ([RuleId])
    REFERENCES [dbo].[Rules]
        ([RuleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_QuestionRules_Rules'
CREATE INDEX [IX_FK_QuestionRules_Rules]
ON [dbo].[QuestionRules]
    ([RuleId]);
GO

-- Creating foreign key on [SurveyId] in table 'Answers'
ALTER TABLE [dbo].[Answers]
ADD CONSTRAINT [FK_SurveyAnswer]
    FOREIGN KEY ([SurveyId])
    REFERENCES [dbo].[Surveys]
        ([SurveyId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SurveyAnswer'
CREATE INDEX [IX_FK_SurveyAnswer]
ON [dbo].[Answers]
    ([SurveyId]);
GO

-- Creating foreign key on [SurveyId] in table 'SurveyAudiences'
ALTER TABLE [dbo].[SurveyAudiences]
ADD CONSTRAINT [FK_SurveySurveyAudience]
    FOREIGN KEY ([SurveyId])
    REFERENCES [dbo].[Surveys]
        ([SurveyId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SurveySurveyAudience'
CREATE INDEX [IX_FK_SurveySurveyAudience]
ON [dbo].[SurveyAudiences]
    ([SurveyId]);
GO

-- Creating foreign key on [SurveyId] in table 'SurveyQuestions'
ALTER TABLE [dbo].[SurveyQuestions]
ADD CONSTRAINT [FK_SurveySurveyQuestion]
    FOREIGN KEY ([SurveyId])
    REFERENCES [dbo].[Surveys]
        ([SurveyId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SurveySurveyQuestion'
CREATE INDEX [IX_FK_SurveySurveyQuestion]
ON [dbo].[SurveyQuestions]
    ([SurveyId]);
GO

-- Creating foreign key on [RuleTypeId] in table 'Rules'
ALTER TABLE [dbo].[Rules]
ADD CONSTRAINT [FK_Rules_RuleType]
    FOREIGN KEY ([RuleTypeId])
    REFERENCES [dbo].[RuleTypes]
        ([RuleTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Rules_RuleType'
CREATE INDEX [IX_FK_Rules_RuleType]
ON [dbo].[Rules]
    ([RuleTypeId]);
GO

-- Creating foreign key on [QuestionOneId] in table 'RelatedQuestions'
ALTER TABLE [dbo].[RelatedQuestions]
ADD CONSTRAINT [FK_RelatedQuestions_Questions]
    FOREIGN KEY ([QuestionOneId])
    REFERENCES [dbo].[Questions]
        ([QuestionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RelatedQuestions_Questions'
CREATE INDEX [IX_FK_RelatedQuestions_Questions]
ON [dbo].[RelatedQuestions]
    ([QuestionOneId]);
GO

-- Creating foreign key on [QuestionTwoId] in table 'RelatedQuestions'
ALTER TABLE [dbo].[RelatedQuestions]
ADD CONSTRAINT [FK_RelatedQuestions_Questions1]
    FOREIGN KEY ([QuestionTwoId])
    REFERENCES [dbo].[Questions]
        ([QuestionId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RelatedQuestions_Questions1'
CREATE INDEX [IX_FK_RelatedQuestions_Questions1]
ON [dbo].[RelatedQuestions]
    ([QuestionTwoId]);
GO

-- Creating foreign key on [MenteeId] in table 'MentorAssignments'
ALTER TABLE [dbo].[MentorAssignments]
ADD CONSTRAINT [FK_MentorAssignments_Mentees]
    FOREIGN KEY ([MenteeId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MentorAssignments_Mentees'
CREATE INDEX [IX_FK_MentorAssignments_Mentees]
ON [dbo].[MentorAssignments]
    ([MenteeId]);
GO

-- Creating foreign key on [MentorId] in table 'MentorAssignments'
ALTER TABLE [dbo].[MentorAssignments]
ADD CONSTRAINT [FK_MentorAssignments_Mentors]
    FOREIGN KEY ([MentorId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MentorAssignments_Mentors'
CREATE INDEX [IX_FK_MentorAssignments_Mentors]
ON [dbo].[MentorAssignments]
    ([MentorId]);
GO


SET IDENTITY_INSERT [dbo].[RuleTypes] ON 
GO
INSERT [dbo].[RuleTypes] ([RuleTypeId], [Name]) VALUES (1, N'Validation')
GO
INSERT [dbo].[RuleTypes] ([RuleTypeId], [Name]) VALUES (2, N'Option')
GO
SET IDENTITY_INSERT [dbo].[RuleTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Rules] ON 
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (1, N'required', N'required', 1)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (2, N'minlength_5', N'minlength_5', 1)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (3, N'maxlength_500', N'maxlength_500', 1)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (4, N'integer', N'integer', 1)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (5, N'atleast_1', N'atleast_1', 1)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (6, N'atmost_2', N'atmost_2', 1)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (7, N'maxlength_1000', N'maxlength_1000', 1)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (8, N'other', N'other', 2)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (9, N'selected_5', N'selected_5', 2)
GO
INSERT [dbo].[Rules] ([RuleId], [Name], [Definition], [RuleTypeId]) VALUES (11, N'checked', N'checked', 1)
GO
SET IDENTITY_INSERT [dbo].[Rules] OFF
GO
SET IDENTITY_INSERT [dbo].[QuestionTypes] ON 
GO
INSERT [dbo].[QuestionTypes] ([QuestionTypeId], [Name], [Description]) VALUES (1, N'Comment', N'a long entry with a rich text editor')
GO
INSERT [dbo].[QuestionTypes] ([QuestionTypeId], [Name], [Description]) VALUES (2, N'Text', N'as single line text entry')
GO
INSERT [dbo].[QuestionTypes] ([QuestionTypeId], [Name], [Description]) VALUES (3, N'SingleChoice', N'choose a single item from a list of many items')
GO
INSERT [dbo].[QuestionTypes] ([QuestionTypeId], [Name], [Description]) VALUES (4, N'MultipleChoiceRanked', N'choose and order several items form a list of many items')
GO
INSERT [dbo].[QuestionTypes] ([QuestionTypeId], [Name], [Description]) VALUES (6, N'DropDownList', N'choose a single item from a drop down list')
GO
INSERT [dbo].[QuestionTypes] ([QuestionTypeId], [Name], [Description]) VALUES (7, N'MultipleChoice', N'choose multiple items from a list')
GO
INSERT [dbo].[QuestionTypes] ([QuestionTypeId], [Name], [Description]) VALUES (8, N'RankedList', N'put list items in order')
GO
SET IDENTITY_INSERT [dbo].[QuestionTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Questions] ON 
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1010, N'Select your Level:', NULL, NULL, 1000, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1011, N'Would you prefer a mentor in your same office or a different office?', NULL, NULL, 2000, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1012, N'How many mentors would you like? You may be matched with up to 2 mentors.', NULL, NULL, 3000, 6)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1013, N'Would you prefer a mentor in the same practice group or a different practice group?', NULL, NULL, 2500, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1014, N'Please select up to 5 areas in which you would like to be mentored.', N'* Please select at least one area.', NULL, 4000, 7)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1015, N'In a few sentences please describe what you are looking for in a mentor. Please also use this space to provide any additional information including whether you would prefer a diverse mentor or if you already have a mentor in mind who you would like to be paired with.', N'* An answer is required.', NULL, 5000, 1)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1016, N'Select your Level:', NULL, NULL, 1000, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1017, N'Would you prefer a mentee in your same office or a different office?', NULL, NULL, 2000, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1018, N'Would you prefer a mentee in your same practice group or a different practice group?', NULL, NULL, 3000, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1019, N'Would you prefer a mentee of your same gender?', NULL, NULL, 4000, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1020, N'How many mentees are you willing and able to mentor?', N'* An answer is required and it must be a number greater than 0.', NULL, 5000, 6)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1021, N'Please select up to 5 areas in which you would like to provide mentorship.', N'* Please select at least one area.', NULL, 6000, 7)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1022, N'In a few sentences please describe what makes you a unique mentor and what you want to share with your mentee. Please also use this space to provide any additional information, including whether you would prefer a diverse mentee, or if you already have a mentee in mind that you would like to be paired with.', N'* An answer is required.', NULL, 7000, 1)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1023, N'Would you prefer a mentor of your same gender?', NULL, NULL, 2500, 3)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1024, N'Reorder your answers from most important to least.', NULL, NULL, 6500, 8)
GO
INSERT [dbo].[Questions] ([QuestionId], [Text], [Title], [Description], [DisplayOrder], [QuestionTypeId]) VALUES (1025, N'Reorder your answers from most important to least.', NULL, NULL, 4500, 8)
GO
SET IDENTITY_INSERT [dbo].[Questions] OFF
GO
SET IDENTITY_INSERT [dbo].[QuestionRules] ON 
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (4, 1020, 1)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (5, 1020, 4)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (6, 1020, 5)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (12, 1021, 8)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (13, 1021, 9)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (16, 1021, 11)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (17, 1014, 9)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (18, 1022, 1)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (19, 1015, 1)
GO
INSERT [dbo].[QuestionRules] ([QuestionRuleId], [QuestionId], [RuleId]) VALUES (21, 1014, 11)
GO
SET IDENTITY_INSERT [dbo].[QuestionRules] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([UserId], [EmployeeId], [Name], [ADLogin], [PracticeGroup], [EmailAddress]) VALUES (246, N'2201', N'Ben McGee', N'BL_McGee', N'Information Technology', N'BLMcGee@hollandhart.com')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
INSERT [dbo].[Admins] ([AdminId]) VALUES (246)
GO
SET IDENTITY_INSERT [dbo].[Choices] ON 
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (22, N'1', N'Level 1', 100, 1010, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (23, N'2', N'Level 2', 200, 1010, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (24, N'3', N'Level 3', 300, 1010, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (25, N'Same', N'Same Office', 100, 1011, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (26, N'Different', N'Different Office', 200, 1011, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (27, N'None', N'No Preference', 300, 1011, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (28, NULL, N'1', 100, 1012, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (29, NULL, N'2', 200, 1012, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (32, N'Same', N'Same PG', 100, 1013, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (33, N'Different', N'Different PG', 200, 1013, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (34, N'None', N'No Preference', 300, 1013, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (36, N'Balance', N'Balancing Career & Home Life', 100, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (37, N'Diversity', N'Diversity & Inclusion Issues', 200, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (38, N'Profession', N'Professional Skills', 300, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (39, N'Industry', N'Involvement in Industry Groups', 400, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (40, N'Law', N'Law Practice Management & The Business of Law', 500, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (41, N'Lead', N'Leadership Development', 600, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (42, N'Promotion', N'Promotion, Expectancies, & Competencies', 700, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (43, N'Research', N'Research', 800, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (44, N'Transactional Writing', N'Writing for Transactional Attorneys', 900, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (45, N'Litigation Writing', N'Writing for Litigation Attorneys', 1000, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (46, N'Trial', N'Trial Advocacy', 1100, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (47, N'Networking', N'Networking', 1200, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (48, N'Alt Career', N'Alternative Career path & Transition', 1300, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (49, N'Pro Bono', N'Pro Bono Legal Opportunities', 1400, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (50, N'Financial Planning', N'Financial Planning', 1500, 1014, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (51, NULL, N'Level 1', 100, 1016, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (52, NULL, N'Level 2', 200, 1016, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (54, NULL, N'Level 3', 300, 1016, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (55, NULL, N'Partner', 400, 1016, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (56, NULL, N'Same Office', 100, 1017, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (57, NULL, N'Different Office', 200, 1017, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (58, NULL, N'No Preference', 300, 1017, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (59, NULL, N'Same PG', 100, 1018, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (60, NULL, N'Different PG', 200, 1018, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (61, NULL, N'No Preference ', 300, 1018, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (62, NULL, N'Yes', 100, 1019, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (63, NULL, N'No', 200, 1019, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (64, NULL, N'No Preference', 300, 1019, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (65, NULL, N'Balancing Career & Home Life', 100, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (66, NULL, N'Diversity & Inclusion Issues', 200, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (67, NULL, N'Professional Skills', 300, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (68, NULL, N'Involvement in Industry Groups', 400, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (69, NULL, N'Law Practice Management & The Business of Law', 500, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (70, NULL, N'Leadership Development', 600, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (71, NULL, N'Promotion, Expectancies, & Competencies', 700, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (72, NULL, N'Research', 800, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (73, NULL, N'Writing for Transactional Attorneys', 900, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (74, NULL, N'Writing for Litigation Attorneys', 1000, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (75, NULL, N'Trial Advocacy', 1100, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (76, NULL, N'Networking', 1200, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (77, NULL, N'Alternative Career Path & Transition', 1300, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (78, NULL, N'Pro Bono Legal Opportunities', 1400, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (79, NULL, N'Financial Planning', 1500, 1021, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (82, NULL, N'1', 100, 1020, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (83, NULL, N'2', 200, 1020, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (84, NULL, N'3', 300, 1020, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (86, NULL, N'Yes', 100, 1023, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (87, NULL, N'No', 200, 1023, NULL)
GO
INSERT [dbo].[Choices] ([ChoiceId], [Name], [Description], [SortOrder], [QuestionId], [IsDefault]) VALUES (88, NULL, N'No Preference', 300, 1023, NULL)
GO
SET IDENTITY_INSERT [dbo].[Choices] OFF
GO
SET IDENTITY_INSERT [dbo].[Surveys] ON 
GO
INSERT [dbo].[Surveys] ([SurveyId], [Name], [Title], [Closed], [Description], [Description2], [SurveyStyle]) VALUES (4, N'Mentor', N'Mentor Profile Form', 0, N'Mentor Profile 2018', N'Please answer these 8 questions.', N'background-color:#d0d6f2')
GO
INSERT [dbo].[Surveys] ([SurveyId], [Name], [Title], [Closed], [Description], [Description2], [SurveyStyle]) VALUES (5, N'Mentee', N'Mentee Profile Form', 0, N'Mentee Profile 2018', N'Please answer these 8 questions.', N'background-color:#bbe8bf')
GO
SET IDENTITY_INSERT [dbo].[Surveys] OFF
GO
SET IDENTITY_INSERT [dbo].[SurveyQuestions] ON 
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1009, 1010, 5)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1010, 1011, 5)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1011, 1012, 5)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1012, 1013, 5)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1013, 1014, 5)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1014, 1015, 5)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1016, 1016, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1017, 1017, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1018, 1018, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1019, 1019, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1021, 1020, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1022, 1021, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1023, 1022, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1025, 1023, 5)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1026, 1024, 4)
GO
INSERT [dbo].[SurveyQuestions] ([SurveyQuestionId], [QuestionId], [SurveyId]) VALUES (1027, 1025, 5)
GO
SET IDENTITY_INSERT [dbo].[SurveyQuestions] OFF
GO
SET IDENTITY_INSERT [dbo].[RelatedQuestions] ON 
GO
INSERT [dbo].[RelatedQuestions] ([RelatedQuestionId], [QuestionOneId], [QuestionTwoId]) VALUES (1, 1024, 1021)
GO
INSERT [dbo].[RelatedQuestions] ([RelatedQuestionId], [QuestionOneId], [QuestionTwoId]) VALUES (2, 1025, 1014)
GO
SET IDENTITY_INSERT [dbo].[RelatedQuestions] OFF
GO
SET IDENTITY_INSERT [dbo].[AppSettings] ON 
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (1, N'MentorSurvey', N'The ID of the default mentor survey that new mentors must take.', N'4', NULL)
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (2, N'MenteeSurvey', N'The ID of the default mentee survey that new mentees must take.', N'5', NULL)
GO
SET IDENTITY_INSERT [dbo].[AppSettings] OFF
GO


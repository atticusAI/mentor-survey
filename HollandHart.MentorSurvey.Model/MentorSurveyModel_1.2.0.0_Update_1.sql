﻿USE [HHMentorSurvey]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_MultipleChoiceAnswers] AS 
SELECT a.[AnswerId]
		,a.[Value]
		,a.[Text]
		,a.[SurveyId]
		,a.[UserId]
		,a.[QuestionId]
		,CAST(ca.[Value] AS INT) AS [ChoiceId]
		,CASE WHEN ca.[Value] = 0 THEN 'Other: ' + a.[Other]
		ELSE ch.[Description] END AS [ChoiceDescription]
FROM (
	SELECT a.[AnswerId]
		,a.[Value]
		,a.[Text]
		,a.[SurveyId]
		,a.[UserId]
		,a.[QuestionId]
		,JSON_VALUE(a.[Text], '$.other') AS Other
		,JSON_QUERY(a.[Text], '$.selected') AS Choices
	FROM [dbo].[Answers] a
	INNER JOIN [dbo].[Questions] q ON q.[QuestionId] = a.[QuestionId]
	INNER JOIN [dbo].[QuestionTypes] qt ON qt.[QuestionTypeId] = q.[QuestionTypeId]
	WHERE qt.[Name] = 'MultipleChoice'
) AS a
CROSS APPLY OPENJSON(a.[Choices]) AS ca
LEFT OUTER JOIN  [dbo].[Choices] ch ON ch.[ChoiceId] = ca.[Value]
GO

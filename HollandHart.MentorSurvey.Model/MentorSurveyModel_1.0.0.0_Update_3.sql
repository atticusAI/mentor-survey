﻿USE [HHMentorSurvey]

ALTER TABLE dbo.Surveys ADD
	Description2 NVARCHAR(MAX) NULL
GO

UPDATE [dbo].[Surveys]
   SET [Description2] = 'Please answer the next 7 questions.'
 WHERE [SurveyId] IN (4, 5);
GO 


UPDATE [dbo].[Questions]
   SET [Description] = 'Drag and drop the desired topics in a desired place.'
 WHERE [QuestionId] IN (1014, 1021)
GO

UPDATE [dbo].[Questions]
   SET [Text] = 'How many mentors would you like? You may be matched with up to 2 mentors.'
 WHERE [QuestionId] = 1012
GO



﻿USE [HHMentorSurvey]

ALTER TABLE dbo.Surveys ADD
	SurveyStyle NVARCHAR(MAX) NULL
GO

UPDATE [dbo].[Surveys]
   SET [SurveyStyle] = 'background-color:#ffdbdb'
 WHERE [SurveyId] = 5;
GO 

UPDATE [dbo].[Surveys]
   SET [SurveyStyle] = 'background-color:#d0d6f2'
 WHERE [SurveyId] = 4;
GO 

UPDATE [dbo].[Questions]
   SET [Text] = 'Would you prefer a mentor of your same gender?'
 WHERE [QuestionId] = 1023;
GO

DELETE FROM [dbo].[QuestionRules]
      WHERE [QuestionRuleId] IN (8,9, 10,11);
GO

INSERT INTO [dbo].[QuestionRules]
           ([QuestionId]
           ,[RuleId])
     VALUES
           (1022, 1);
GO

INSERT INTO [dbo].[QuestionRules]
           ([QuestionId]
           ,[RuleId])
     VALUES
           (1015, 1);
GO 

UPDATE [dbo].[Questions]
   SET [Title] = '* An answer is required.'
 WHERE [QuestionId] IN (1015, 1022);
GO

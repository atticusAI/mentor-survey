﻿USE [HHMentorSurvey]


GO
PRINT N'Altering [dbo].[Choices]...';


GO
ALTER TABLE [dbo].[Choices]
    ADD [Value] NVARCHAR (50) NULL;

GO 
UPDATE [dbo].[Choices] set [Value] = [Description]
where value is null and ChoiceId in (28,
	29,
	82,
	83,
	84);


GO
PRINT N'Altering [dbo].[SurveyQuestions]...';


GO
ALTER TABLE [dbo].[SurveyQuestions]
    ADD [QuestionWeight] NUMERIC (18, 2) NULL;

GO
  UPDATE [dbo].[SurveyQuestions] 
  SET [QuestionWeight] = 1.25;

GO
PRINT N'Creating [dbo].[ScoreMatrix]...';


GO
CREATE TABLE [dbo].[ScoreMatrix] (
    [ScoreMatrixId]  INT IDENTITY (1, 1) NOT NULL,
    [MentorSurveyId] INT NOT NULL,
    [MenteeSurveyId] INT NOT NULL,
    [MentorChoiceId] INT NOT NULL,
    [MenteeChoiceId] INT NOT NULL,
    [Score]          INT NOT NULL,
    CONSTRAINT [PK_ScoreMatrix] PRIMARY KEY CLUSTERED ([ScoreMatrixId] ASC)
);


GO
SET IDENTITY_INSERT [dbo].[ScoreMatrix] ON 
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (1, 4, 5, 51, 22, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (2, 4, 5, 51, 23, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (3, 4, 5, 51, 24, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (4, 4, 5, 52, 22, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (5, 4, 5, 52, 23, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (6, 4, 5, 52, 24, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (7, 4, 5, 54, 22, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (8, 4, 5, 54, 23, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (9, 4, 5, 54, 24, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (10, 4, 5, 55, 22, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (11, 4, 5, 55, 23, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (12, 4, 5, 55, 24, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (13, 4, 5, 56, 25, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (14, 4, 5, 56, 26, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (15, 4, 5, 56, 27, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (16, 4, 5, 57, 25, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (17, 4, 5, 57, 26, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (18, 4, 5, 57, 27, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (19, 4, 5, 58, 25, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (20, 4, 5, 58, 26, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (21, 4, 5, 58, 27, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (22, 4, 5, 59, 32, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (23, 4, 5, 59, 33, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (24, 4, 5, 59, 34, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (25, 4, 5, 60, 32, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (26, 4, 5, 60, 33, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (27, 4, 5, 60, 34, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (28, 4, 5, 61, 32, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (29, 4, 5, 61, 33, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (30, 4, 5, 61, 34, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (31, 4, 5, 62, 86, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (32, 4, 5, 62, 87, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (33, 4, 5, 62, 88, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (34, 4, 5, 63, 86, -1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (35, 4, 5, 63, 87, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (36, 4, 5, 63, 88, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (37, 4, 5, 64, 86, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (38, 4, 5, 64, 87, 0)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (39, 4, 5, 64, 88, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (40, 4, 5, 65, 36, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (41, 4, 5, 66, 37, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (42, 4, 5, 67, 38, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (43, 4, 5, 68, 39, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (44, 4, 5, 69, 40, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (45, 4, 5, 70, 41, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (46, 4, 5, 71, 42, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (47, 4, 5, 72, 43, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (48, 4, 5, 73, 44, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (49, 4, 5, 74, 45, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (50, 4, 5, 75, 46, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (51, 4, 5, 76, 47, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (52, 4, 5, 77, 48, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (53, 4, 5, 78, 49, 1)
GO
INSERT [dbo].[ScoreMatrix] ([ScoreMatrixId], [MentorSurveyId], [MenteeSurveyId], [MentorChoiceId], [MenteeChoiceId], [Score]) VALUES (54, 4, 5, 79, 50, 1)
GO
SET IDENTITY_INSERT [dbo].[ScoreMatrix] OFF
GO



GO
PRINT N'Creating [dbo].[AppSettings].[UK_AppSettings_Name]...';


GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AppSettings_Name]
    ON [dbo].[AppSettings]([Name] ASC);

GO
SET IDENTITY_INSERT [dbo].[AppSettings] ON 
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (12, N'MentorAssignmentCountQuestion', N'The question whose answer will determine how many assignments the mentor is open for.', N'1020', NULL)
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (13, N'MenteeAssignmentCountQuestion', N'The question whose answer will determine how many assignments the mentee is open for.', N'1012', NULL)
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (14, N'ScoreFloor', N'The minimum possible score.', N'-8', NULL)
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (15, N'ScoreCeiling', N'The maximum possible score.', N'15', NULL)
GO
SET IDENTITY_INSERT [dbo].[AppSettings] OFF

GO
PRINT N'Creating [dbo].[FK_ScoreMatrix_MentorSurveys]...';


GO
ALTER TABLE [dbo].[ScoreMatrix] WITH NOCHECK
    ADD CONSTRAINT [FK_ScoreMatrix_MentorSurveys] FOREIGN KEY ([MentorSurveyId]) REFERENCES [dbo].[Surveys] ([SurveyId]);


GO
PRINT N'Creating [dbo].[FK_ScoreMatrix_MenteeSurveys]...';


GO
ALTER TABLE [dbo].[ScoreMatrix] WITH NOCHECK
    ADD CONSTRAINT [FK_ScoreMatrix_MenteeSurveys] FOREIGN KEY ([MenteeSurveyId]) REFERENCES [dbo].[Surveys] ([SurveyId]);


GO
PRINT N'Creating [dbo].[FK_ScoreMatrix_MentorChoices]...';


GO
ALTER TABLE [dbo].[ScoreMatrix] WITH NOCHECK
    ADD CONSTRAINT [FK_ScoreMatrix_MentorChoices] FOREIGN KEY ([MentorChoiceId]) REFERENCES [dbo].[Choices] ([ChoiceId]);


GO
PRINT N'Creating [dbo].[FK_ScoreMatrix_MenteeChoices]...';


GO
ALTER TABLE [dbo].[ScoreMatrix] WITH NOCHECK
    ADD CONSTRAINT [FK_ScoreMatrix_MenteeChoices] FOREIGN KEY ([MenteeChoiceId]) REFERENCES [dbo].[Choices] ([ChoiceId]);


GO
PRINT N'Altering [dbo].[VW_MultipleChoiceAnswers]...';


GO


ALTER VIEW [dbo].[VW_MultipleChoiceAnswers] AS 
SELECT a.[AnswerId]
		,a.[Value]
		,a.[Text]
		,a.[SurveyId]
		,a.[UserId]
		,a.[QuestionId]
		,CAST(ca.[Value] AS INT) AS [ChoiceId]
		,CASE WHEN ca.[Value] = 0 THEN 'Other: ' + a.[Other]
		ELSE ch.[Description] END AS [ChoiceDescription]
FROM (
	SELECT a.[AnswerId]
		,a.[Value]
		,a.[Text]
		,a.[SurveyId]
		,a.[UserId]
		,a.[QuestionId]
		,JSON_VALUE(a.[Text], '$.other') AS Other
		,JSON_QUERY(a.[Text], '$.selected') AS Choices
	FROM [dbo].[Answers] a
	INNER JOIN [dbo].[Questions] q ON q.[QuestionId] = a.[QuestionId]
	INNER JOIN [dbo].[QuestionTypes] qt ON qt.[QuestionTypeId] = q.[QuestionTypeId]
	WHERE qt.[Name] = 'MultipleChoice'
		AND a.[Text] IS NOT NULL 
		AND a.[Text] != ''
) AS a
CROSS APPLY OPENJSON(a.[Choices]) AS ca
LEFT OUTER JOIN  [dbo].[Choices] ch ON ch.[ChoiceId] = ca.[Value]
GO
PRINT N'Creating [dbo].[VW_AssignmentCounts]...';


GO


CREATE view [dbo].[VW_AssignmentCounts] as

select 'M' + CONVERT(VARCHAR(5), a.UserId) AS [User],
	CONVERT(int, c.Value) as Requested, 
	a.UserId as MentorId,
	-1 as MenteeId, 
	count(ma.MentorAssignmentId) as Assigned
from Answers a 
inner join Choices c on c.ChoiceId = a.Value
inner join Users u on u.UserId = a.UserId
left outer join MentorAssignments ma on ma.MentorId = a.UserId
where a.QuestionId = (
select aps.Value 
from AppSettings aps
where aps.Name = 'MentorAssignmentCountQuestion')
group by c.Value, 
	a.UserId

union all 

select'E' + CONVERT(VARCHAR(5), a.UserId) as [User],
	CONVERT(int, c.Value) as Requested, 
	-1 as MentorId, 
	a.UserId as MenteeId,
		count(ma.MentorAssignmentId) as Assigned
from Answers a 
inner join Choices c on c.ChoiceId = a.Value
inner join Users u on u.UserId = a.UserId
left outer join MentorAssignments ma on ma.MenteeId = a.UserId
where a.QuestionId = (
select aps.Value 
from AppSettings aps
where aps.Name = 'MenteeAssignmentCountQuestion')
group by c.Value, 
	a.UserId
GO
PRINT N'Creating [dbo].[VW_MultipleChoiceScores]...';


GO
CREATE VIEW [dbo].[VW_MultipleChoiceScores] as
select q1.MentorId, 
	q2.MenteeId, 
	sum(isnull(sm.Score, 0.00)) as Score,
	sum(isnull(sm.Score, 0.00) * ((q1.QuestionWeight + q2.QuestionWeight)/2)) as WeightedScore
from ScoreMatrix sm
left outer join (
select a.UserId as MentorId,
	c.ChoiceId as MentorChoiceId, 
	q.QuestionId as MentorQuestionId,
	sq.QuestionWeight
from answers a
inner join VW_MultipleChoiceAnswers va on va.AnswerId = a.AnswerId
inner join choices c on c.QuestionId = a.QuestionId and va.ChoiceId= c.ChoiceId
inner join SurveyQuestions sq on sq.QuestionId = a.QuestionId and sq.SurveyId = a.SurveyId
inner join questions q on q.QuestionId = a.QuestionId
inner join QuestionTypes qt on qt.QuestionTypeId = q.QuestionTypeId and qt.Name in('MultipleChoice')
inner join Mentors m on m.MentorId = a.UserId
where a.SurveyId = ( 
	select Value from 
	AppSettings where name = 'MentorSurvey'
) ) q1 on q1.MentorChoiceId = sm.MentorChoiceId

left outer join (
select a.UserId as MenteeId,
	c.ChoiceId as MenteeChoiceId, 
	q.QuestionId as MenteeQuestionId,
	sq.QuestionWeight
from answers a
inner join VW_MultipleChoiceAnswers va on va.AnswerId = a.AnswerId
inner join choices c on c.QuestionId = a.QuestionId and va.ChoiceId = c.ChoiceId
inner join SurveyQuestions sq on sq.QuestionId = a.QuestionId and sq.SurveyId = a.SurveyId
inner join questions q on q.QuestionId = a.QuestionId
inner join QuestionTypes qt on qt.QuestionTypeId = q.QuestionTypeId and qt.Name in('MultipleChoice')
inner join Mentees e on e.MenteeId = a.UserId
where a.SurveyId = ( 
	select Value from 
	AppSettings where name = 'MenteeSurvey'
) ) q2 on q2.MenteeChoiceId = sm.MenteeChoiceId
where q1.MentorId is not null and 
	q2.MenteeId is not null
group by q1.MentorId, 
	q2.MenteeId, 
	(q1.QuestionWeight + q2.QuestionWeight) / 2
GO








GO
PRINT N'Creating [dbo].[VW_SingleChoiceScores]...';


GO



GO
CREATE VIEW [dbo].[VW_SingleChoiceScores] as
select q1.MentorId, 
	q2.MenteeId, 
	sum(isnull(sm.Score, 0.00)) as Score,
	sum (isnull(sm.Score, 0.00) * ((q1.QuestionWeight + q2.QuestionWeight)/2)) as WeightedScore
from ScoreMatrix sm
left outer join (
select a.UserId as MentorId,
	c.ChoiceId as MentorChoiceId, 
	q.QuestionId as MentorQuestionId,
	sq.QuestionWeight
from answers a
inner join choices c on c.QuestionId = a.QuestionId and a.Value = c.ChoiceId
inner join SurveyQuestions sq on sq.QuestionId = a.QuestionId and sq.SurveyId = a.SurveyId
inner join questions q on q.QuestionId = a.QuestionId
inner join QuestionTypes qt on qt.QuestionTypeId = q.QuestionTypeId and qt.Name in('SingleChoice', 'DropDownList')
inner join Mentors m on m.MentorId = a.UserId
where a.SurveyId = ( 
	select Value from 
	AppSettings where name = 'MentorSurvey'
) ) q1 on q1.MentorChoiceId = sm.MentorChoiceId

left outer join (
select a.UserId as MenteeId,
	c.ChoiceId as MenteeChoiceId, 
	q.QuestionId as MenteeQuestionId,
	sq.QuestionWeight
from answers a
inner join choices c on c.QuestionId = a.QuestionId and a.Value = c.ChoiceId
inner join SurveyQuestions sq on sq.QuestionId = a.QuestionId and sq.SurveyId = a.SurveyId
inner join questions q on q.QuestionId = a.QuestionId
inner join QuestionTypes qt on qt.QuestionTypeId = q.QuestionTypeId and qt.Name in('SingleChoice', 'DropDownList')
inner join Mentees e on e.MenteeId = a.UserId
where a.SurveyId = ( 
	select Value from 
	AppSettings where name = 'MenteeSurvey'
) ) q2 on q2.MenteeChoiceId = sm.MenteeChoiceId
where q1.MentorId is not null and
	q2.MenteeId is not null
group by q1.MentorId, 
	q2.MenteeId, 
	(q1.QuestionWeight + q2.QuestionWeight) / 2
GO



/*
* blmcgee@hollandhart.com 2018-07-31
* calculate the percentage score given a weighted score
* another slope following function
* y = mx + b
*/
CREATE FUNCTION [dbo].[FN_GetPercentageScore]
(
	@WeightedScore numeric(38, 6)
)
RETURNS numeric(38, 6)
AS
BEGIN
	DECLARE @PercentageScore numeric(38, 6);
	DECLARE @Slope numeric(38, 6);
	DECLARE @Floor numeric (38, 6);
	DECLARE @Ceil numeric(38, 6);

	select @Floor = Convert(numeric(38, 6), Value)
		from AppSettings
		where Name = 'ScoreFloor'; -- the minimum possible score
		                           -- sum of all the possible negative values in the score matrix

	select @Ceil = Convert(numeric(38, 6), Value)
			from AppSettings
			where name = 'ScoreCeiling'; -- the maximum possible score
			                             -- sum of all the possible positive values in the score matrix

	select @Slope = (@Floor - @Ceil)  /  (0.00 - 100.00); -- m = (y1 - y2) / (x1 - x2);

	select @PercentageScore = (@WeightedScore - @Floor) / @Slope; -- x = (n - b) / m; 
		
	RETURN @PercentageScore

END

GO
PRINT N'Creating [dbo].[VW_Scores]...';


GO
CREATE view [dbo].[VW_Scores] as
select distinct
	'M' + Convert(nvarchar(50), s.MentorId) as Mentor, 
	'E' + Convert(nvarchar(50), s.MenteeId) as Mentee,
	s.MentorId, 
	um.Name as MentorName,
	s.MenteeId,
	ue.Name as MenteeName,
	s.Score + isnull(m.Score, 0.00) as Score,
	s.WeightedScore,
	[dbo].[FN_GetPercentageScore] (s.WeightedScore + isnull(m.WeightedScore, 0.00)) as PercentageScore
from VW_SingleChoiceScores s 
inner join Users um on um.UserId = s.MentorId 
inner join Users ue on ue.UserId = s.MenteeId

left outer join (
	select 'M' + Convert(nvarchar(50), m.MentorId) as Mentor, 
		'E' + Convert(nvarchar(50), m.MenteeId) as Mentee,
		m.MentorId, 
		m.MenteeId,
		m.Score + m.Score as Score,
		m.WeightedScore,
		[dbo].[FN_GetPercentageScore] (m.WeightedScore) as PercentageScore
	from VW_MultipleChoiceScores m
) m on m.MenteeId = s.MenteeId and m.MentorId = s.MentorId
GO
PRINT N'Creating [dbo].[FN_GetPercentageScore]...';


GO
PRINT N'Checking existing data against newly created constraints';


GO
ALTER TABLE [dbo].[ScoreMatrix] WITH CHECK CHECK CONSTRAINT [FK_ScoreMatrix_MentorSurveys];

ALTER TABLE [dbo].[ScoreMatrix] WITH CHECK CHECK CONSTRAINT [FK_ScoreMatrix_MenteeSurveys];

ALTER TABLE [dbo].[ScoreMatrix] WITH CHECK CHECK CONSTRAINT [FK_ScoreMatrix_MentorChoices];

ALTER TABLE [dbo].[ScoreMatrix] WITH CHECK CHECK CONSTRAINT [FK_ScoreMatrix_MenteeChoices];


GO
PRINT N'Update complete.';


GO

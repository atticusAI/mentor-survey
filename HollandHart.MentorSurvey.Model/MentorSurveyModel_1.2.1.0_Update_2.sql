USE [HHMentorSurvey]
GO
SET IDENTITY_INSERT [dbo].[AppSettings] ON 
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (3, N'MentorProfile_EmailBody', N'The email that gets sent to a mentor when they complete the profile.', NULL, N'<p>
Thank you for volunteering to become a mentor. Your profile is being reviewed by the Mentoring Program Coordinator and you will be notified by email as soon as your “match” has been made. 
</p>
<p>
For more information on the H&H Attorney Mentoring Program, click here: <a href="http://hhportal2.hollandhart.com/Departments/RD/Mentoring%20Program/Pages/default.aspx">H&H Mentoring Program Portal Page</a>
</p>
<p>
If you have any questions, please contact <a href="mailto:KLMeyer@hollandhart.com">Katie Meyer</a>.
</p>
')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (4, N'MenteeProfile_EmailBody', N'The email that gets sent to a mentee when they complete the profile.', NULL, N'<p>
Thank you for signing up as a mentee. Your profile is being reviewed by the Mentoring Program Coordinator and you will be notified by email as soon as your “match” has been made. 
</p>
<p>
For more information on the H&H Attorney Mentoring Program, click here: <a href="http://hhportal2.hollandhart.com/Departments/RD/Mentoring%20Program/Pages/default.aspx">H&H Mentoring Program Portal Page</a>
</p>
<p>
If you have any questions, please contact <a href="KLMeyer@hollandhart.com">Katie Meyer</a>
</p>
')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (5, N'MentorProfile_EmailSubject', N'The subject for the email that gets sent to a mentor when they complete the profile.', NULL, N'Thank you for volunteering.')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (6, N'MenteeProfile_EmailSubject', N'The subject for the email that gets sent to a mentee when they complete the profile.', NULL, N'Thank you for signing up.')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (7, N'EmailSenderAddress', N'The email address that email notifications will come from.', NULL, N'MentorMenteeMatching@hollandhart.com')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (8, N'EmailSenderDisplayName', N'The name that email notifications will come from.', NULL, N'Mentor/Mentee Matching')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (9, N'AdminBccAddresses', N'Semicolon seperated list of email addresses that will get a blind carbon copy of all notifications.', NULL, N'MNishikura@hollandhart.com;KLMeyer@hollandhart.com')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (10, N'AdminToAddresses', N'Semicolon seperated list of email addresses that will get admin notification emails.', NULL, N'MNishikura@hollandhart.com;KLMeyer@hollandhart.com')
GO
INSERT [dbo].[AppSettings] ([SettingId], [Name], [Description], [Value], [Text]) VALUES (11, N'AdminCcAddresses', N'Semicolon separated list of email addresses that will get a carbon copy of all notifications.', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[AppSettings] OFF
GO
